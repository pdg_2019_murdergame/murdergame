const CABALEVICTORY = "cabal victory"
const VILLAGERVICTORY = "villager victory"
async function getApiUrl() {
  let apiFile = "ressource/urlApi.txt";
  return fetch(apiFile)
    .then(response => response.text())
    .then(text => {
      return `https://${text}`;
    })
}

/**
 * This function call the api for return the stats for room size betwen now and now -time 
 * @param {int} time (ms)
 */
async function DoGetRoomSizeStat(time) {
  let apiUrl = await getApiUrl();
  let request = new Request(`${apiUrl}/rest/statistics/roomSize/${time}`);

  let init = {
    method: "GET",
  }
  data = fetch(request, init)
    .then(response => response.json())
    .then(data => {
      return data.RoomsSizeStatistique.map(x => {
        return {
          "group": x.players,
          "cabale": x.cabalVictory,
          "villager": x.parties - x.cabalVictory,
        }
      });
    })
  return data;
}

async function DoGetAverageDeathStat(time) {
  let apiUrl = await getApiUrl();
  let request = new Request(`${apiUrl}/rest/statistics/survavibility/${time}`);

  let init = {
    method: "GET",
  }
  data = fetch(request, init)
    .then(response => response.json())
    .then(data => {
      let result = new Array();
      let playerSize = 3;
      data.forEach(partiesSize => {
        playerSize++;
        let sommeCabal = 0;
        let playerNumberCabal = 0;
        let sommeVillage = 0;
        let playerNumberVillage = 0;
        for (const partie of partiesSize) {
          for (const player of partie.players) {
            if (player.deathTurn != null) {
              if (player.team == "citizen") {
                playerNumberVillage++;
                sommeVillage += player.deathTurn;
              } else {
                playerNumberCabal++;
                sommeCabal += player.deathTurn;
              }

            }
          }
        }
        let cabalAverage = 0;
        let villageAverage = 0;
        if (playerNumberCabal != 0) {
          cabalAverage = sommeCabal / playerNumberCabal;
        }
        if (playerNumberVillage != 0) {
          villageAverage = sommeVillage / playerNumberVillage;
        }
        result.push({ cabale: cabalAverage, players: playerSize, village: villageAverage })
      })
      return result;
    })
  return data;

}

class createPartiesGraph {
  constructor() {

    //le tuto le plus utilise que j'ai trouver ce trouve a l'adresse https://www.datavis.fr/index.php#d3js 
    //dimension et marge du graphique
    this.margin = { top: 20, right: 20, bottom: 90, left: 120 },
      this.width = 800 - this.margin.left - this.margin.right,
      this.height = 400 - this.margin.top - this.margin.bottom;

    //va chercher les data
    //const data = await DoGetRoomSizeStat()
    //creation des sous-groupes
    this.subgroups = ["cabale", "villager"]

    // définition de la fonction permettant le remplissage de l'axe des abcisses et celui des ordonnées.
    this.x = d3.scaleBand()
      .range([0, this.width])
      .padding(0.1);

    // échelle linéaire// range égal à la hauteur de notre graphique
    this.y = d3.scaleLinear()
      .range([this.height, 0]);

  }


  /**
   * this fonction update the histogram with only the betwen now and now -time
   * @param {int} time (ms)
   */
  async update(time) {
    // fetch data to back 
    let data = await DoGetRoomSizeStat(time);

    //simplest solution is to remove this svg
    d3.select("#my_histogram").select("svg").remove();

    this.svg = d3.select("#my_histogram").append("svg")
      .attr("id", "svg")
      .attr("width", this.width + this.margin.left + this.margin.right)
      .attr("height", this.height + this.margin.top + this.margin.bottom)
      .append("g")
      .attr("transform", "translate(" + this.margin.left + "," + this.margin.top + ")");

    // text label for the x axis
    this.svg.append("text")
      .attr("transform",
        "translate(" + (this.width / 2) + " ," +
        (this.height + this.margin.top + 35) + ")")
      .style("text-anchor", "middle")
      .text("number of players");


    // text label for the y axis
    this.svg.append("text")
      .attr("transform", "rotate(-90)")
      .attr("y", 0 - this.margin.left / 2)
      .attr("x", 0 - (this.height / 2))
      .attr("dy", "1em")
      .style("text-anchor", "middle")
      .text("number of games");

    // Handmade legend
    this.svg.append("circle").attr("cx", 0).attr("cy", this.height + this.margin.bottom / 2 + 30).attr("r", 6).style("fill", "#ED4C67")
    this.svg.append("circle").attr("cx", 0).attr("cy", this.height + this.margin.bottom / 2).attr("r", 6).style("fill", "#12CBC4")
    this.svg.append("text").attr("x", 20).attr("y", this.height + this.margin.bottom / 2 + 30).text(CABALEVICTORY).style("font-size", "15px").attr("alignment-baseline", "middle")
    this.svg.append("text").attr("x", 20).attr("y", this.height + this.margin.bottom / 2).text(VILLAGERVICTORY).style("font-size", "15px").attr("alignment-baseline", "middle")
    // on a besoin que axe x soit une letiable dans ce scoop pour appeler la fonction x généré par D3.js
    const x = this.x;
    // on a besoin que axe y soit un const ici (D3.js)
    const y = this.y;
    //definition de la range de nos axes 
    x.domain(data.map(function (d) { return d.group; }));

    y.domain([0, d3.max(data, function (d) { return d.villager + d.cabale; })]);

    //couleur par subgroup
    let color = d3.scaleOrdinal()
      .domain(this.subgroups)
      .range(['#ED4C67', '#12CBC4'])
    //creation des groups (valleur de x)
    let groups = d3.map(data, function (d) { return d.group }).keys()
    //stack les data avec les subgroups
    let stackedData = d3.stack()
      .keys(this.subgroups)
      (data)
    // Ajout de l'axe X au this.svg
    // Déplacement de l'axe horizontal et du futur texte (via la fonction translate) au bas du this.svg
    // Selection des noeuds text, positionnement puis rotation
    this.svg.append("g")
      .attr("transform", "translate(0," + this.height + ")")
      .call(d3.axisBottom(x).tickSize(0))
      .selectAll("text")
      .style("text-anchor", "middle")
      .attr("dy", "1em")
      .attr("transform", "rotate(0)");

    // Ajout de l'axe Y au this.svg avec 6 éléments de légende en utilisant la fonction ticks (sinon D3JS en place autant qu'il peut).
    this.svg.append("g")
      .call(d3.axisLeft(y).ticks(6));

    // Define the div for the tooltip we need to define here not in constructor cause of svg remove
    let div = d3.select("#my_histogram").append("div")
      .attr("class", "tooltip")
      .style("opacity", 0);


    this.svg.append("g")
      .selectAll("g")
      // Enter in the stack data = loop key per key = group per group
      .data(stackedData)
      .enter().append("g")
      .attr("fill", function (d) { return color(d.key); })
      .selectAll("rect")
      // enter a second time = loop subgroup per subgroup to add all rectangles
      .data(function (d) { return d; })
      .enter().append("rect")
      .attr("x", function (d) { return x(d.data.group); })
      .attr("y", function (d) { return y(d[1]); })
      .attr("height", function (d) { return y(d[0]) - y(d[1]); })
      .attr("width", x.bandwidth())
      .on("mouseover", function (d) {
        div.transition()
          .duration(200)
          .style("opacity", .9);
        div.html('parties of ' + d.data.group + " players : " + (d.data.cabale + d.data.villager) + "</br> " + VILLAGERVICTORY + " :" + d.data.villager + "</br> " + CABALEVICTORY + " :" + d.data.cabale)
          .style("left", (d3.event.pageX + 10) + "px")
          .style("top", (d3.event.pageY - 50) + "px");

      })
      .on("mouseout", function (d) {
        div.transition()
          .duration(500)
          .style("opacity", 0);
      });
  }
}

histrogram = new createPartiesGraph();
histrogram.update(0);

class createMultiLineGraph {
  constructor() {
    this.margin = { top: 20, right: 20, bottom: 90, left: 120 },
      this.width = 800 - this.margin.left - this.margin.right,
      this.height = 400 - this.margin.top - this.margin.bottom;



    // set the ranges
    let x = d3.scaleLinear().range([0, this.width]);
    let y = d3.scaleLinear().range([this.height, 0]);
    this.x = x;
    this.y = y;


    // define the 1st line
    this.valueline = d3.line()
      .x(function (d) { return x(d.players); })
      .y(function (d) { return y(d.cabale); });

    // define the 2nd line
    this.valueline2 = d3.line()
      .x(function (d) { return x(d.players); })
      .y(function (d) { return y(d.village); });



  }

  async update(time) {
    //simplest solution is to remove this svg
    d3.select("#my_multi_graph").select("svg").remove();

    // append the svg obgect to my multi graph div of the page
    // appends a 'group' element to 'svg'
    // moves the 'group' element to the top left margin
    this.svg = d3.select("#my_multi_graph").append("svg")
      .attr("width", this.width + this.margin.left + this.margin.right)
      .attr("height", this.height + this.margin.top + this.margin.bottom)
      .append("g")
      .attr("transform",
        "translate(" + this.margin.left + "," + this.margin.top + ")");

    const x = this.x;
    const y = this.y;

    let data = await DoGetAverageDeathStat(time);

    data.forEach(function (d) {
      d.players = +d.players;
      d.cabale = +d.cabale;
      d.village = +d.village;
    });

    // Scale the range of the data
    x.domain(d3.extent(data, function (d) { return d.players; }));
    y.domain([0, d3.max(data, function (d) {
      return Math.max(d.cabale, d.village);
    })]);

    // Add the valueline path.
    this.svg.append("path")
      .data([data])
      .attr("class", "line")
      .attr("stroke", "#ED4C67")
      .attr("d", this.valueline);

    // Add the valueline2 path.
    this.svg.append("path")
      .data([data])
      .attr("class", "line")
      .style("stroke", "#12CBC4")
      .attr("d", this.valueline2);

    // Add the X Axis
    this.svg.append("g")
      .attr("transform", "translate(0," + this.height + ")")
      .call(d3.axisBottom(x));

    // Add the Y Axis
    this.svg.append("g")
      .call(d3.axisLeft(y));



    // text label for the y axis
    this.svg.append("text")
      .attr("transform", "rotate(-90)")
      .attr("y", 0 - this.margin.left / 2)
      .attr("x", 0 - (this.height / 2))
      .attr("dy", "1em")
      .style("text-anchor", "middle")
      .text("avg tour survavibility");
    // text label for the x axis
    this.svg.append("text")
      .attr("transform",
        "translate(" + (this.width / 2) + " ," +
        (this.height + this.margin.top + 35) + ")")
      .style("text-anchor", "middle")
      .text("number of players per party");


    // Handmade legend
    this.svg.append("circle").attr("cx", 0).attr("cy", this.height + this.margin.bottom / 2 + 30).attr("r", 6).style("fill", "#ED4C67")
    this.svg.append("circle").attr("cx", 0).attr("cy", this.height + this.margin.bottom / 2).attr("r", 6).style("fill", "#12CBC4")
    this.svg.append("text").attr("x", 20).attr("y", this.height + this.margin.bottom / 2 + 30).text("cabale").style("font-size", "15px").attr("alignment-baseline", "middle")
    this.svg.append("text").attr("x", 20).attr("y", this.height + this.margin.bottom / 2).text("villager").style("font-size", "15px").attr("alignment-baseline", "middle")
  }
}

multiLineGraph = new createMultiLineGraph();
multiLineGraph.update(0);