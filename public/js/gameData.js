async function DoGet() {
    var apiUrl = await getApiUrl();
    var init = {
        method: "GET",
    }
    var request = new Request(`${apiUrl}/rest/statistics/data`);
    fetch(request, init)
        .then(response => response.json())
        .then(data => {
            document.getElementById('numberOfPlayer').innerHTML = `<p><font color="red">${data.playerCount}</font> players tried to survive</p>`;
            document.getElementById('numberOfVillage').innerHTML = `<p>Hiverdun suffered <font color="red">${data.partyCount}</font> attacks</p>`;
            document.getElementById('numberOfDeath').innerHTML = `<p><font color="red">${data.deathCount}</font> players have been executed</p>`;
        })

}