async function DoGit() {
    Team = [
        { idGitLab: '2869802', idGitHub: '33123187', name: 'Alexandre Gabrielli', role: 'devOps', personalAdd: 'heroku haters' },
        { idGitLab: '2869793', idGitHub: '25949694', name: 'Christoph Rouff soit Rueff', role: 'front end', personalAdd: 'react boy' },
        { idGitLab: '3576112', idGitHub: '5020490', name: 'David Jaquet', role: 'back end', personalAdd: 'with pleasure' },
        { idGitLab: '2869744', idGitHub: '47737071', name: 'Tiago Póvoa', role: 'UI/UX', personalAdd: 'the expert' }
    ]

    for (member of Team) {
        var gitHubRequest = new Request(`https://api.github.com/user/${member.idGitHub}`);
        var init = {
            method: "GET",
        }
        var userGitHub = await fetch(gitHubRequest, init)
            .then(response => response.json())
            .then(data => { return data });

        var gitLabRequest = new Request(`https://gitlab.com/api/v4/users/${member.idGitLab}`);

        await fetch(gitLabRequest, init)
            .then(response => response.json())
            .then(data => {

                userGitLab = data;
                document.getElementById(`us_Team`).innerHTML += `
                <div class="text-center text-black mx-auto gitlab">
                    <h2>${member.name}</h2>
                    <p> role : ${member.role}</p>
                    <p> ${member.personalAdd}</p>
                    <img src="${userGitLab.avatar_url}" alt="${userGitLab.name} gitlab avatar" style="width:150px;height:150px;"/>
                    <p><a href="${userGitLab.web_url}">follow me on gitLab</a></p>
                    <p><a href="${userGitHub.html_url}">or follow me on gitHub</a></p>
                </div>`;
            })
    }
}