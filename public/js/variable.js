var file = "ressource/urlWebApp.txt"

fetch(file)
    .then(response => response.text())
    .then(text => {
        document.getElementById("webapplink").removeAttribute("href");
        document.getElementById("webapplink").setAttribute("href", `https://${text}`);
        document.getElementById("webapplink2").removeAttribute("href");
        document.getElementById("webapplink2").setAttribute("href", `https://${text}`);
    })

async function getApiUrl() {
    var apiFile = "ressource/urlApi.txt";
    return fetch(apiFile)
        .then(response => response.text())
        .then(text => {
            return `https://${text}`;
        })
}