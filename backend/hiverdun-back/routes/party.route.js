const router = require('express').Router();
const httpStatus = require('http-status');

const services = require('../service/services');
const { sign } = require('../service/jwt.service');

const service = services.parties;

/**
 * Get all the parties
 */
router.get('/', async (req, res) => {
  const parties = await service.getAll();

  res.status(httpStatus.OK).send(parties);
});

/**
 * Get a party by code
 */
router.get('/:code', async (req, res) => {
  const { code } = req.params;

  const party = await service.getOneByCode(code);

  if (party) {
    res.status(httpStatus.OK).send(party);
  } else {
    res.status(httpStatus.NOT_FOUND).send();
  }
});

/**
 * Create a party, sign the header with the JWT
 */
router.post('/', async (req, res) => {
  try {
    const party = await service.create();
    const token = sign({ code: party.code });

    res.status(httpStatus.OK).send({ party, token: `Bearer ${token}` });
  } catch (err) {
    res.status(httpStatus.INTERNAL_SERVER_ERROR).send('something went wrong');
  }
});

/**
 * Delete a party
 */
router.delete('/:id', async (req, res) => {
  const { id } = req.params;

  if (service.remove(id)) {
    res.status(httpStatus.NO_CONTENT).send();
  } else {
    res.status(httpStatus.CONFLICT).send();
  }
});

module.exports = router;
