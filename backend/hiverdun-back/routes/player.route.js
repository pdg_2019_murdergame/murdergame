const router = require('express').Router();
const httpStatus = require('http-status');

const { sign } = require('../service/jwt.service');
const services = require('../service/services');

const playerService = services.players;
const partyService = services.parties;

/**
 * Create a player and add it to the wanted party
 * Header with signed jwt
 */
router.post('/', async (req, res) => {
  const { code } = req.query;
  const { nickname } = req.body;

  if (nickname === undefined || nickname.length === 0 || code === undefined) {
    res.status(httpStatus.BAD_REQUEST).send('Something is missing');
    return;
  }

  let player = null;
  try {
    player = await playerService.create(`${nickname}-${code}`);
  } catch (error) {
    res.status(httpStatus.BAD_REQUEST).send('Duplicate player');
    return;
  }

  try {
    const party = await partyService.addPlayer(code, player);
    const token = sign({ nickname: player.pseudo, code: party.code });
    res.send({ player, token: `Bearer ${token}` });
  } catch (error) {
    res.status(httpStatus.BAD_REQUEST).send(error.message);
  }
});

// TODO : Delete route if unused
router.get('/', async (req, res) => {
  const players = await playerService.getAll();

  // res.status(httpStatus.OK).send(players[0].pseudo);
  res.status(httpStatus.OK).send(players);
});

// UNCOMMENT FOR TESTING PURPOSES :)
/*
router.post('/save', async (req, res) => {
  const { victory, turn, player, team } = req.body;
  const a = await services.players.save(player, turn, team, victory);
  res.status(httpStatus.OK).send(a);
});
*/

module.exports = router;
