const router = require('express').Router();
const httpStatus = require('http-status');

const services = require('../service/services');

const service = services.statistics;
/**
 * Get the number of parties
 */
router.get('/partiesNumber', async (req, res) => {
  const numberOfPartiesLaunch = await service.partyCount();
  res.status(httpStatus.OK).send({ numberOfPartiesLaunch });
});

router.get('/playerNumber', async (req, res) => {
  const numberOfPlayer = await service.playerCount();
  res.status(httpStatus.OK).send({ numberOfPlayer });
});

router.get('/deathCount', async (req, res) => {
  const deathCount = await service.deathCountVanilla();
  res.status(httpStatus.OK).send({ deathCount });
});

router.get('/data', async (req, res) => {
  const allData = await service.getAllData();
  res.status(httpStatus.OK).send(allData);
});

router.get('/roomSize/:time', async (req, res) => {
  const { time } = req.params;
  const statistics = await service.getRoomsSizeStatistique(time);
  res.status(httpStatus.OK).send(statistics);
});

router.get('/survavibility/:time', async (req, res) => {
  const { time } = req.params;
  const statistics = await service.getSurvavibilityStatistique(time);
  res.status(httpStatus.OK).send(statistics);
});

module.exports = router;
