const express = require('express');

const router = express.Router();

const http = require('http').createServer(express);

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
  console.log("I'm not a teapot")
});

module.exports = router;
