const express = require('express');
const Games = require('../models/party.model');

const router = express.Router();

router.get('/', (req, res) => {
  console.log('test');
  Games.find().then(games => res.status(201).json({ games }));
});

router.post('/create', (req, res) => {
  console.log('Create games');

  Games({
    nbPlayers: 10,
    name: 'Game 78',
    nbDead: 9,
    createDate: new Date(),
  })
    .save()
    .then(savedGame => res.status(201).json({ savedGame }))
    .catch(err => res.status(400).json(`Error ${err}`));
});

module.exports = router;
