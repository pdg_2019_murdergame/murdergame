/**
 * Add your routes and middlewares here
 */
const express = require('express');
const parties = require('./party.route');
const players = require('./player.route');
const statistics = require('./statistics.route');

const { checkJWT } = require('../middleware/auth.middleware');
const alive = require('./alive.route');

const router = express.Router();

router.get('/p*', checkJWT);

router.use('/parties', parties);
router.use('/players', players);
router.use('/statistics', statistics);

router.use('/alive', alive);

module.exports = router;
