const { env } = process;

const config = {
  // If you don't want mongodb+srv, setup it in a env var
  URL_BEGIN: env.URL_BEGIN || 'mongodb+srv',
  DB_USER: env.DB_USER || `root`,
  DB_PWD: env.DB_PWD || `example`,
  DB_URI: env.DB_URI || `localhost:27017`,
  JWT_SECRET: env.JWT_SECRET || 'default_secret_do_not_use_this_outside_testing_purposes',
  NODE_ENV: env.NODE_ENV, // don't exist by default
};

module.exports = { config };
