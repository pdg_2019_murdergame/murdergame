const { assert } = require('chai');
const sinon = require('sinon');
const PartyModel = require('../models/party.model');

const { PartyService } = require('../service/party.service');

const obj = { players: [], _id: '5dcabb30ebadb53ac3c2c146', code: 'XPH2KJ', __v: 0 };

describe('Party', () => {
  let stub;

  afterEach(() => {
    stub.restore();
  });

  describe('CRUD operations on Party Service', () => {
    describe('Create', () => {
      it('should be possible to create a party', async () => {
        stub = sinon.stub(PartyModel.prototype, 'save').returns(obj);

        const partyService = new PartyService(PartyModel);

        partyService.create();

        sinon.assert.calledOnce(stub);
      });
    });

    it('should be possible to get all parties', async () => {
      stub = sinon.stub(PartyModel, 'find').returns([obj, obj]);

      const partyService = new PartyService(PartyModel);

      assert.isArray(await partyService.getAll());

      sinon.assert.calledOnce(stub);
    });

    describe('FindOneByCode', () => {
      it('should be possible to get one party by code', async () => {
        stub = sinon.stub(PartyModel, 'findOne').returns({
          players: [],
          _id: '5dcd5987fd3be322ea735a40',
          code: 'IT9M76',
          __v: 0,
        });

        const partyService = new PartyService(PartyModel);

        assert.isNotNull(await partyService.getOneByCode());

        sinon.assert.calledOnce(stub);
      });

      it('should return null if nothing got found', async () => {
        stub = sinon.stub(PartyModel, 'findOne').returns(null);

        const partyService = new PartyService(PartyModel);

        assert.isNull(await partyService.getOneByCode());

        sinon.assert.calledOnce(stub);
      });
    });

    describe('Delete', () => {
      it('should be possible to delete a party', async () => {
        stub = sinon.stub(PartyModel, 'deleteOne').returns({ n: 1, ok: 1, deletedCount: 1 });

        const partyService = new PartyService(PartyModel);

        assert.isTrue(await partyService.remove(1));

        sinon.assert.calledOnce(stub);
      });

      it("should return false, on delete, when object doesn't exist", async () => {
        stub = sinon.stub(PartyModel, 'deleteOne').returns({ n: 0, ok: 1, deletedCount: 0 });

        const partyService = new PartyService(PartyModel);

        assert.isFalse(await partyService.remove(1));

        sinon.assert.calledOnce(stub);
      });
    });
  });
});
