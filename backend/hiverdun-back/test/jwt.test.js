const { assert } = require('chai');

const { sign, verify } = require('../service/jwt.service');

describe('JWT', () => {
  let payload;

  beforeEach(() => {
    payload = { val: 'something something' };
  });

  it('should be possible to sign a jwt', () => {
    sign(payload);
  });

  it('should be possible to verify a jwt', () => {
    const token = sign(payload);

    const decoded = verify(token);

    assert.equal(decoded.val, 'something something');
  });
});
