const { assert } = require('chai');
const sinon = require('sinon');

const { PartyContext } = require('../io/manager/party.context');

const { Player, TeamsEnum } = require('../io/manager/player');
const { CardsEnum } = require('../io/manager/cards/enum.cards');

describe('Player IO', () => {
  let player;

  beforeEach(() => {
    player = new Player('payara8er');
    player.cards = [CardsEnum.nothing, CardsEnum.prophecy, CardsEnum.ankh];
  });

  describe('Team', () => {
    it('should be in citizen team by default', () => {
      assert.equal(player.team, TeamsEnum.citizen);
    });

    it('should be possible to change team', () => {
      player.setTeam(TeamsEnum.cabal);

      assert.equal(player.team, TeamsEnum.cabal);
    });
  });

  describe('Card manipulation', () => {
    describe('Add', () => {
      it('should be possible to add a card', () => {
        player.addCard(CardsEnum.nothing);

        assert.equal(player.newcard, CardsEnum.nothing);
      });
    });

    describe('Reject', () => {
      // Reject a card in hand
      it('should be possible to reject a card in the hand', () => {
        player.removeCard(CardsEnum.ankh);

        assert.equal(player.cards.length, 2);
        assert.equal(player.cards[0], CardsEnum.nothing);
        assert.equal(player.cards[1], CardsEnum.prophecy);
        assert.equal(player.newcard, null);
      });

      // Reject the new card
      it('should be possible to reject the new card', () => {
        player.addCard(CardsEnum.curse);
        player.removeCard(CardsEnum.curse);

        assert.equal(player.cards.length, 3);
        assert.equal(player.newcard, null);
      });

      // Reject an inexistant card
      it('should not be possible to reject an inexistant card, the new card will be rejected', () => {
        player.addCard(CardsEnum.curse);
        player.removeCard(CardsEnum.clue);

        assert.equal(player.cards[0], CardsEnum.nothing);
        assert.equal(player.cards[1], CardsEnum.prophecy);
        assert.equal(player.cards[2], CardsEnum.ankh);
        assert.equal(player.cards.newcard, null);
      });

      // Reject nothing ( = reject -1)
      it('should be possible to keep all cards', () => {
        player.cards = [CardsEnum.nothing, CardsEnum.prophecy];
        player.addCard(CardsEnum.ankh);
        player.removeCard(-1);

        assert.equal(player.cards[0], CardsEnum.nothing);
        assert.equal(player.cards[1], CardsEnum.prophecy);
        assert.equal(player.cards[2], CardsEnum.ankh);
        assert.equal(player.newcard, null);
      });

      // Card duplicated, reject one of them
      it('should not remove all the duplicated cards', () => {
        player.cards = [CardsEnum.nothing, CardsEnum.nothing, CardsEnum.prophecy];
        player.addCard(CardsEnum.ankh);
        player.removeCard(CardsEnum.nothing);

        assert.equal(player.cards[0], CardsEnum.nothing);
        assert.equal(player.cards[1], CardsEnum.prophecy);
        assert.equal(player.cards[2], CardsEnum.ankh);
        assert.equal(player.newcard, null);
      });

      // Card duplicated, reject one of them who is the new one
      it('should not remove all the duplicated cards, even if one of them is the new one', () => {
        player.addCard(CardsEnum.ankh);
        player.removeCard(CardsEnum.ankh);

        assert.equal(player.cards[0], CardsEnum.nothing);
        assert.equal(player.cards[1], CardsEnum.prophecy);
        assert.equal(player.cards[2], CardsEnum.ankh);
        assert.equal(player.newcard, null);
      });

      it('should not be possible to reject a persistent card', () => {
        player.cards = [CardsEnum.sacrifice, CardsEnum.sacrifice, CardsEnum.sacrifice];
        player.addCard(CardsEnum.nothing);
        player.removeCard(CardsEnum.sacrifice);

        assert.equal(player.cards[0], CardsEnum.sacrifice);
        assert.equal(player.cards[1], CardsEnum.sacrifice);
        assert.equal(player.cards[2], CardsEnum.sacrifice);
        assert.equal(player.newcard, null);
      });

      it('should be possible to reject a persistent card if it is the new one and the player has already 3 persistent cards', () => {
        player.cards = [CardsEnum.sacrifice, CardsEnum.sacrifice, CardsEnum.sacrifice];
        player.addCard(CardsEnum.ankh);
        player.removeCard(CardsEnum.sacrifice);

        assert.equal(player.cards[0], CardsEnum.sacrifice);
        assert.equal(player.cards[1], CardsEnum.sacrifice);
        assert.equal(player.cards[2], CardsEnum.sacrifice);
        assert.equal(player.newcard, null);
      });
    });

    describe('Passive', () => {
      it('should not be possible to kill a user with the hard skin card', () => {
        const context = sinon.createStubInstance(PartyContext);

        player.cards[1] = CardsEnum.hardskin;
        player.setAlive(context, false);

        assert.isTrue(player.alive);
      });
    });

    describe('Hard Skin', () => {
      let context;
      beforeEach(() => {
        context = sinon.createStubInstance(PartyContext);
      });
      it('should remove the hard skin card if the players try to kill him', () => {
        player.cards[1] = CardsEnum.hardskin;

        player.setAlive(context, false);

        assert.equal(player.cards.length, 2);
        assert.equal(player.cards[0], CardsEnum.nothing);
        assert.equal(player.cards[1], CardsEnum.ankh);
      });

      it('should be possible to resurrect a player with the hard skin card', () => {
        // We kill the player
        player.setAlive(context, false);

        player.cards[1] = CardsEnum.hardskin;
        player.setAlive(context, true);

        assert.isTrue(player.alive);
      });

      it('should not remove the hard skin if we want to resurrect the player', () => {
        // We kill the player
        player.setAlive(context, false);

        player.cards[1] = CardsEnum.hardskin;
        player.setAlive(context, true);

        assert.equal(player.cards.length, 3);
        assert.equal(player.cards[0], CardsEnum.nothing);
        assert.equal(player.cards[1], CardsEnum.hardskin);
        assert.equal(player.cards[2], CardsEnum.ankh);
      });
    });

    describe('Solo Card', () => {
      it('should be possible to play a solo card', () => {
        const context = new PartyContext();

        context.addPlayer(player.nickname);

        const playingPlayer = context.getPlayer(player.nickname);
        player.cards.forEach(card => {
          playingPlayer.addCard(card);
          playingPlayer.removeCard(-1);
        });

        const cardPlayed = context.playCard(CardsEnum.nothing, player.nickname, null);

        assert.equal(playingPlayer.cards.length, 2);
        assert.equal(playingPlayer.cards[0], CardsEnum.prophecy);
        assert.equal(playingPlayer.cards[1], CardsEnum.ankh);
        assert.equal(cardPlayed, CardsEnum.nothing);
      });
    });

    describe('Pair Card', () => {
      it('should be possible to play a pair card if we the player has the good cards', () => {
        const context = new PartyContext();
        const target = 'WildFlyForFun';

        context.addPlayer(player.nickname);
        context.addPlayer(target);

        const playingPlayer = context.getPlayer(player.nickname);
        playingPlayer.cards = [CardsEnum.nothing, CardsEnum.clue, CardsEnum.clue];

        const cardPlayed = context.playCard(CardsEnum.clue, player.nickname, target);

        assert.equal(playingPlayer.cards.length, 1);
        assert.equal(playingPlayer.cards[0], CardsEnum.nothing);
        assert.equal(cardPlayed, CardsEnum.clue);
      });

      it('should not be possible to play a pair card if the user has not the good one', () => {
        const context = new PartyContext();
        const target = 'WildFlyForFun';

        context.addPlayer(player.nickname);
        context.addPlayer(target);

        const playingPlayer = context.getPlayer(player.nickname);
        playingPlayer.cards = [CardsEnum.nothing, CardsEnum.ankh, CardsEnum.clue];

        const cardPlayed = context.playCard(CardsEnum.clue, player.nickname, target);

        assert.equal(playingPlayer.cards.length, 3);
        assert.equal(playingPlayer.cards[0], CardsEnum.nothing);
        assert.equal(playingPlayer.cards[1], CardsEnum.ankh);
        assert.equal(playingPlayer.cards[2], CardsEnum.clue);
        assert.equal(cardPlayed, -1);
      });
    });
  });

  describe('Personnal information', () => {
    it('sould be possible to get the player personnal information', () => {
      player.addCard(3);
      player.setTeam(TeamsEnum.cabal);

      const info = player.getMe();

      assert.equal(info.nickname, 'payara8er');
      assert.isTrue(info.alive);
      assert.equal(info.cards.length, 3);
      assert.equal(info.team, TeamsEnum.cabal);
    });

    it('should be possible to get the min info of every player', () => {
      const info = player.getInfo();

      assert.equal(info.nickname, 'payara8er');
      assert.isTrue(info.alive);
      assert.isTrue(info.online);
    });

    it('should return the minimum info of a player if he is not in the information set', () => {
      const context = new PartyContext();
      const target = 'WildFlyForFun';

      context.addPlayer(player.nickname);
      context.addPlayer(target);

      const info = player.getOtherPlayerInfo(context, target);

      assert.equal(info.nickname, 'WildFlyForFun');
      assert.isTrue(info.alive);
      assert.isTrue(info.online);
      assert.notOwnInclude(info, { cards: [] });
    });

    it('should return all infos of a player if he is in the information set', () => {
      const context = new PartyContext();
      const target = 'WildFlyForFun';

      context.addPlayer(player.nickname);
      context.addPlayer(target);

      player.addPlayerInfo(target);

      const info = player.getOtherPlayerInfo(context, target);

      assert.equal(info.nickname, target);
      assert.isTrue(info.alive);
      assert.isTrue(info.online);
      assert.isArray(info.cards);
    });
  });
});
