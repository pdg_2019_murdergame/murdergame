const { assert } = require('chai');

const { PartyContext } = require('../io/manager/party.context');
const { TeamsEnum } = require('../io/manager/player');
const { SacrificeCardReq } = require('../io/manager/cards/sacrifice.cardreq');
const {
  AbstractState,
  LobbyState,
  StoryState,
  FirstNightState,
  FirstDawnState,
  FirstDayState,
  NightState,
  DawnState,
  DayState,
  EndState,
} = require('../io/manager/states/states');

describe('PartyContext', () => {
  const nickname = 'payara8ter';
  let pc;

  beforeEach(() => {
    pc = new PartyContext();
    pc.addPlayer(nickname);
  });

  it('should be possible to add players', () => {
    assert.isNotEmpty(pc.players);
    assert.lengthOf(pc.players, 1);
  });

  it('should be possible to know room size', () => {
    assert.equal(pc.getRoomSize(), 1);
  });

  it('should be possible to pass a player online and offline', () => {
    pc.setPlayerOnline(nickname, true);
    assert.isTrue(pc.players.get(nickname).online);
    pc.setPlayerOnline(nickname, false);
    assert.isFalse(pc.players.get(nickname).online);
  });

  it('shoud be possible to set that story has ended', () => {
    pc.skipStory();
    assert.isTrue(pc.storyEnd);
  });

  describe('Game over', () => {
    it('should be able to know if the game is over', () => {
      pc.addPlayer(`${nickname}12`);
      assert.isTrue(pc.isGameOver());
    });

    it('should be possible to win as cabal', () => {
      pc.addPlayer(`${nickname}12`);

      pc.getPlayer(nickname).team = TeamsEnum.cabal;
      pc.getPlayer(`${nickname}12`).team = TeamsEnum.cabal;

      assert.isTrue(pc.isGameOver());
      assert.isTrue(pc.allCitizenDead);
    });
  });

  describe('Play Card', () => {
    it('should be possible to play a card', () => {
      pc.addPlayer(`${nickname}12`);
      pc.getPlayer(`${nickname}12`).cards.push(6);
      pc.playCard(6, `${nickname}12`, nickname);
      assert.isNotEmpty(pc.nightendqueue, 'nightendqueue should have a new req');
      assert.instanceOf(pc.nightendqueue[0], SacrificeCardReq);
    });

    it('should not be possible to play a card you do not possess', () => {
      pc.addPlayer(`${nickname}12`);
      pc.playCard(6, `${nickname}12`, nickname);
      assert.isEmpty(pc.nightendqueue, 'nightendqueue should have a new req');
    });
  });

  describe('Resolve vote', () => {
    beforeEach(() => {
      pc.getPlayer(nickname).vote = `${nickname}3`;

      pc.addPlayer(`${nickname}1`);
      pc.getPlayer(`${nickname}1`).vote = nickname;
      pc.addPlayer(`${nickname}2`);
      pc.getPlayer(`${nickname}2`).vote = `${nickname}1`;
      pc.addPlayer(`${nickname}3`);
      pc.getPlayer(`${nickname}3`).vote = `${nickname}1`;
      pc.addPlayer(`${nickname}4`);
      pc.getPlayer(`${nickname}4`).vote = nickname;
    });

    it('should count votes', () => {
      pc.addPlayer(`${nickname}5`);
      pc.getPlayer(`${nickname}5`).vote = nickname;

      const result = pc.findGuilty();

      assert.deepEqual(result.guilty, nickname);
      assert.deepEqual(result.count, 3);
    });

    it('should handle even votes / draw', () => {
      const result = pc.findGuilty();

      assert.isTrue(result.equal);
    });

    it('should kill the one who was selected', () => {
      pc.addPlayer(`${nickname}5`);
      pc.getPlayer(`${nickname}5`).vote = nickname;

      const result = pc.resolveVote();

      assert.isFalse(pc.getPlayer(result).alive);
    });
  });

  describe('Is Vote open', () => {
    beforeEach(() => {
      pc.state = new DayState(pc);
    });
    it('should be possible to know if votes are allowed', () => {
      assert.isTrue(pc.isVoteOpen());
      pc.state = new FirstDayState(pc);
      assert.isTrue(pc.isVoteOpen());
    });

    it('should allow players to vote during day', () => {
      pc.addPlayer(`${nickname}5`);
      assert.isTrue(pc.getPlayer(`${nickname}5`).setVote(pc, nickname));
    });

    it('should not allow players to vote during night or dawn', () => {
      pc.state = new NightState(pc);
      pc.addPlayer(`${nickname}5`);
      assert.isFalse(pc.getPlayer(`${nickname}5`).setVote(pc, nickname));
    });

    it('should allow cabal to vote during night', () => {
      pc.state = new NightState(pc);
      pc.addPlayer(`${nickname}5`);
      pc.getPlayer(`${nickname}5`).team = TeamsEnum.cabal;
      assert.isTrue(pc.getPlayer(`${nickname}5`).setVote(pc, nickname));
    });
  });

  describe('Dead players list', () => {
    let p;
    beforeEach(() => {
      pc.addPlayer(`${nickname}5`);
      pc.addDeadPlayer(pc.getPlayer(`${nickname}5`));
      p = pc.getPlayer(`${nickname}5`);
    });

    it('should be possible to add dead players', () => {
      assert.deepInclude([...pc.lastdeads], p);
    });

    it('should be possible to remove dead players', () => {
      assert.deepInclude([...pc.lastdeads], p);

      pc.deleteDeadPlayer(p);

      assert.notInclude([...pc.lastdeads], p);
    });

    it('should be possible to reset dead players', () => {
      for (let i = 0; i < 10; i += 1) {
        pc.addPlayer(`${nickname}${i}`);
        pc.addDeadPlayer(pc.getPlayer(`${nickname}${i}`));
      }
      pc.resetDeadPlayers();
      assert.isEmpty(pc.lastdeads);
    });
  });
});
