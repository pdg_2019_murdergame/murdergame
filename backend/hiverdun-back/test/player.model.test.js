const { assert } = require('chai');
const sinon = require('sinon');
const PlayerModel = require('../models/player.model');

const { PlayerService } = require('../service/player.service');

const player = {
  _id: '5dcf005a9cb5c30006d8b185',
  pseudo: 'NaughtyPayara',
  team: 'village',
  cards: [],
  information: [],
};

describe('Player', () => {
  let stub;

  afterEach(() => {
    stub.restore();
  });

  describe('CRUD operations on Player Service', () => {
    describe('Create', () => {
      it('should be possible to create a player', async () => {
        stub = sinon.stub(PlayerModel.prototype, 'save').returns(player);

        const playerService = new PlayerService(PlayerModel);

        playerService.create('Funny_WildFly');

        sinon.assert.calledOnce(stub);
      });

      it('should not be possible to create a player with a too small pseudo', async () => {
        stub = sinon.stub(PlayerModel.prototype, 'save').returns(player);

        const playerService = new PlayerService(PlayerModel);

        playerService.create('aa');

        sinon.assert.notCalled(stub);
      });

      it('should not be possible to create a player with a too large pseudo', async () => {
        stub = sinon.stub(PlayerModel.prototype, 'save').returns(player);

        const playerService = new PlayerService(PlayerModel);

        playerService.create('Je_ne_suis_pas_de_la_cabale_ne_me_tuez_pas');

        sinon.assert.notCalled(stub);
      });
    });

    describe('Read', () => {
      it('should be possible to get all players', async () => {
        stub = sinon.stub(PlayerModel, 'find').returns([player, player]);
        const playerService = new PlayerService(PlayerModel);
        const players = await playerService.getAll();

        assert.isArray(players);
        assert.equal(players.length, 2);

        sinon.assert.calledOnce(stub);
      });

      it('should be possible to get one player by his pseudo', async () => {
        stub = sinon.stub(PlayerModel, 'findOne').returns(player);

        const playerService = new PlayerService(PlayerModel);

        assert.isNotNull(await playerService.getOneByPseudo('NaughtyPayara'));
        sinon.assert.calledOnce(stub);
      });

      it('should return null if there is no player with the wanted pseudo', async () => {
        stub = sinon.stub(PlayerModel, 'findOne').returns(null);

        const playerService = new PlayerService(PlayerModel);

        assert.isNull(await playerService.getOneByPseudo('FunnyWildFly'));
        sinon.assert.calledOnce(stub);
      });
    });

    describe('Delete', () => {
      it('should be return true when a player is deleted', async () => {
        stub = sinon.stub(PlayerModel, 'deleteOne').returns({ n: 1, ok: 1, deletedCount: 1 });

        const playerService = new PlayerService(PlayerModel);

        // eslint-disable-next-line no-underscore-dangle
        assert.isTrue(await playerService.remove(player._id));
        sinon.assert.calledOnce(stub);
      });

      it('should be return false when we trying to remove an inexistent player', async () => {
        stub = sinon.stub(PlayerModel, 'deleteOne').returns({ n: 0, ok: 1, deletedCount: 0 });

        const playerService = new PlayerService(PlayerModel);

        assert.isFalse(await playerService.remove('not_a_id'));
        sinon.assert.calledOnce(stub);
      });
    });
  });
});
