const { assert } = require('chai');
const sinon = require('sinon');

const { PartyContext } = require('../io/manager/party.context');
const { Deck } = require('../io/manager/deck');
const {
  AbstractState,
  LobbyState,
  StoryState,
  FirstNightState,
  FirstDawnState,
  FirstDayState,
  NightState,
  DawnState,
  DayState,
  EndState,
} = require('../io/manager/states/states');

const { Player, TeamsEnum } = require('../io/manager/player');

describe('PartyContext States', () => {
  let pc;

  beforeEach(() => {
    pc = new PartyContext();
  });
  describe('Transition between lobby and story', () => {
    it('should start with a lobby state', () => {
      assert.instanceOf(pc.state, LobbyState);
    });

    it('should change state if ready and player min', () => {
      // Pushing players
      for (let i = 0; i < 8; i += 1) {
        pc.players.set(`nickname${i}`, null);
      }
      pc.state.doTransition();

      assert.instanceOf(pc.state, StoryState);
    });

    it("should not change state if isn't ready and player min", () => {
      pc.state.doTransition();

      assert.instanceOf(pc.state, LobbyState);
    });
  });

  describe('Transition between story and first night', () => {
    it('should change state if story ended', () => {
      pc.state = new StoryState(pc);

      for (let i = 0; i < 8; i += 1) {
        pc.players.set(`nickname${i}`, new Player(`nickname${i}`));
      }

      pc.storyEnd = true;
      pc.state.doTransition();

      assert.instanceOf(pc.state, FirstNightState);
    });

    it('should not change state if story has not ended or was not skipped', () => {
      pc.state = new StoryState(pc);
      pc.state.doTransition();

      assert.instanceOf(pc.state, StoryState);
    });
  });

  describe('Transition between first night and first dawn', () => {
    beforeEach(() => {
      pc.state = new FirstNightState(pc);

      pc.addPlayer('Vincent G.');
      pc.getPlayer('Vincent G.').pass = true;
    });

    it('should change state if all players passed', () => {
      pc.state.doTransition();

      assert.instanceOf(pc.state, FirstDawnState);
    });

    it('should not change state if some players have not passed', () => {
      pc.getPlayer('Vincent G.').pass = false;

      pc.state.doTransition();

      assert.instanceOf(pc.state, FirstNightState);
    });
  });

  describe('Transition between first dawn and first day', () => {
    beforeEach(() => {
      pc.state = new FirstDawnState(pc);

      pc.addPlayer('Vincent G.');
      pc.getPlayer('Vincent G.').pass = true;
    });

    it('should change if all players passed', () => {
      pc.state.doTransition();

      assert.instanceOf(pc.state, FirstDayState);
    });

    it('should not change state if witch did nothing', () => {
      pc.getPlayer('Vincent G.').pass = false;

      pc.state.doTransition();

      assert.instanceOf(pc.state, FirstDawnState);
    });
  });

  describe('Transition between first day and night', () => {
    beforeEach(() => {
      pc.state = new FirstDayState(pc);

      // Creating a stub with no class logic
      const deck = sinon.createStubInstance(Deck);

      pc.deck = deck;
    });
    it('should change state if everyone has voted', () => {
      pc.setEveryoneHasVoted(true);
      pc.state.doTransition();

      assert.instanceOf(pc.state, NightState);
    });

    it('should not change state until everyone has voted', () => {
      pc.state.doTransition();

      assert.instanceOf(pc.state, FirstDayState);
    });
  });

  describe('Transition between night to dawn', () => {
    beforeEach(() => {
      pc.state = new NightState(pc);

      pc.addPlayer('Vincent G.');
      pc.getPlayer('Vincent G.').pass = true;
      pc.getPlayer('Vincent G.').team = TeamsEnum.cabal;
      pc.getPlayer('Vincent G.').pass = true;
      pc.addPlayer('Vincent G2.');
      pc.getPlayer('Vincent G2.').pass = true;
    });

    it('should change state if all players passed', () => {
      pc.state.doTransition();

      assert.instanceOf(pc.state, DawnState);
    });

    it('should not change state if not all players passed', () => {
      pc.getPlayer('Vincent G.').pass = false;

      pc.state.doTransition();

      assert.instanceOf(pc.state, NightState);
    });

    it('should change state if one team has fallen', () => {
      pc.getPlayer('Vincent G.').alive = false;

      pc.state.doTransition();

      assert.instanceOf(pc.state, EndState);
    });
  });

  describe('Transition between dawn and day or end', () => {
    beforeEach(() => {
      pc.state = new DawnState(pc);

      pc.addPlayer('bob');
      pc.getPlayer('bob').pass = true;
      pc.addPlayer('john');
      pc.getPlayer('john').pass = true;
      pc.addPlayer('ringo');
      pc.getPlayer('ringo').pass = true;

      pc.getPlayer('bob').team = TeamsEnum.cabal;
      pc.getPlayer('ringo').team = TeamsEnum.cabal;
    });

    it('should change state if all players passed', () => {
      pc.state.doTransition();

      assert.instanceOf(pc.state, DayState);
    });

    it('should not change state if some players have not passed', () => {
      pc.getPlayer('ringo').pass = false;
      pc.state.doTransition();

      assert.instanceOf(pc.state, DawnState);
    });

    it('should end the game if all cabalists are dead', () => {
      pc.getPlayer('bob').alive = false;
      pc.getPlayer('ringo').alive = false;

      pc.state.doTransition();

      assert.instanceOf(pc.state, EndState);
    });
  });

  describe('Transition between day and night or end', () => {
    beforeEach(() => {
      pc.state = new DayState(pc);

      pc.addPlayer('bob');
      pc.addPlayer('john');
      pc.addPlayer('ringo');

      // Creating a stub with no class logic
      const deck = sinon.createStubInstance(Deck);

      pc.deck = deck;

      pc.getPlayer('bob').team = TeamsEnum.cabal;
      pc.getPlayer('john').team = TeamsEnum.citizen;
      pc.getPlayer('ringo').team = TeamsEnum.cabal;
    });

    it('should change state if everyone has voted', () => {
      pc.setEveryoneHasVoted(true);

      pc.state.doTransition();

      assert.instanceOf(pc.state, NightState);
    });

    it('should not change state until everyone has voted, or someone is dead', () => {
      pc.state.doTransition();

      assert.instanceOf(pc.state, DayState);
    });

    it('should end the game if all cabalists are dead', () => {
      pc.getPlayer('bob').alive = false;
      pc.getPlayer('ringo').alive = false;
      pc.setEveryoneHasVoted(true);

      pc.state.doTransition();

      assert.instanceOf(pc.state, EndState);
    });

    it('should end the game if all citizens are dead', () => {
      pc.getPlayer('john').alive = false;
      pc.setEveryoneHasVoted(true);

      pc.state.doTransition();

      assert.instanceOf(pc.state, EndState);
    });
  });

  describe('The game has ended', () => {
    it('should do nothing for now I guess', () => {
      pc.state = new EndState(pc);
      pc.state.doTransition();
    });
  });
  describe('Abstract state', () => {
    it('shoudld throw an error when calling doTransition', () => {
      pc.state = new AbstractState(pc);
      assert.throw(pc.state.doTransition);
    });
  });
});
