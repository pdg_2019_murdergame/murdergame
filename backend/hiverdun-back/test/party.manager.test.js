const { assert } = require('chai');

const { PartyManager } = require('../io/manager/party.manager');

describe('PartyManager', () => {
  let pm;
  const client = { id: 1, party: 'DSGX5Z', nickname: 'bob' };

  beforeEach(() => {
    pm = new PartyManager();
    pm.addClientById(client.id, client.party, client.nickname);
  });

  describe('Party: clients', () => {
    it('should be possible to add a client', () => {
      assert.isNotEmpty(pm.clients);
      assert.lengthOf(pm.clients, 1);
    });

    it('should be possible to recover info of a client', () => {
      const { nickname, party } = pm.getInfosById(client.id);

      assert.equal(nickname, client.nickname);
      assert.equal(party, client.party);
    });

    it('should send undefined if has not found key', () => {
      pm = new PartyManager();
      const info = pm.getInfosById(client.id);

      assert.isUndefined(info);
    });

    it('should be possible to remove a client', () => {
      pm.removeClientById(client.id);

      assert.isEmpty(pm.clients);
    });
  });

  describe('Parties registered', () => {
    it('should be possible to add a party', () => {
      assert.isNotEmpty(pm.parties);
      assert.lengthOf(pm.parties, 1);
    });

    it('should not create the same party two times', () => {
      pm.addClientById(client.id, client.party, client.nickname);

      assert.isNotEmpty(pm.parties);
      assert.lengthOf(pm.parties, 1);
    });

    it('should be possible to retrieve a party', () => {
      assert.isNotNull(pm.getParty(client.party));
    });

    it('should be possible to know the room size', () => {
      assert.equal(pm.getRoomSize(client.party), 1);
    });

    it('should return size 0 is room doesnt exist', () => {
      assert.equal(pm.getRoomSize('bliblu'), 0);
    });
  });

  describe('Parties: players', () => {
    it('should be possible to add a player to a party', () => {
      assert.isNotEmpty(pm.parties.get(client.party).players);
      assert.lengthOf(pm.parties.get(client.party).players, 1);
    });
  });
});
