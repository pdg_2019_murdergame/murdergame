const chai = require('chai');

const { assert } = chai;

describe('Hello, World!', () => {
  describe('This is a test to check if Chai is OK', () => {
    it('should return -1 when the value is not present', () => {
      assert.equal([1, 2, 3].indexOf(4), -1);
      assert.equal(false, false);
    });
  });
});
