const { assert } = require('chai');

const { FactoryCardReq } = require('../io/manager/cards/factory.cardreq');
const { SacrificeCardReq } = require('../io/manager/cards/sacrifice.cardreq');
const { PartyContext } = require('../io/manager/party.context');
const { Player } = require('../io/manager/player');

describe('Factory Cards Requests', () => {
  it('should be able to create a Sacrifice Card', () => {
    const factoryCardReq = new FactoryCardReq(null);
    const card = factoryCardReq.makeCardReq(6, 'javaee', 'spring');

    assert.instanceOf(card, SacrificeCardReq);
  });

  it('should be possible to kill some player', () => {
    const pc = new PartyContext();

    for (let i = 0; i < 2; i += 1) {
      pc.players.set(`helmut${i}`, new Player(`helmut${i}`));
    }

    const card = new SacrificeCardReq(pc, 'helmut0', 'helmut1');

    card.do();

    assert.isFalse(pc.players.get('helmut1').alive, 'should be dead, but is not');
  });
});
