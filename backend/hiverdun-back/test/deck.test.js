const { assert } = require('chai');
const { CardsEnum } = require('../io/manager/cards/enum.cards');
const { Deck } = require('../io/manager/deck');

describe('Deck tests', () => {
  it('should have 2 cabal cards', () => {
    const myDeck = new Deck(8);

    const test = myDeck.deck.reduce((acc, val) => {
      return acc + (val === CardsEnum.sacrifice ? 1 : 0);
    }, 0);

    assert.strictEqual(test, 2);
  });

  it('should be of size N', () => {
    const myDeck = new Deck(8);

    assert.lengthOf(myDeck.deck, 8, 'is not size 8');
  });

  it('should only distribute a unique card once', () => {
    const myDeck = new Deck(100);
    const array = [];

    for (let i = 0; i < 100; i += 1) {
      array.push(myDeck.draw());
    }

    const count = array.filter(c => c === myDeck.config.uniqueCards[0]).reduce(acc => acc + 1, 0);
    assert.deepEqual(count, 1);
  });
});
