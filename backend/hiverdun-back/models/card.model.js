const mongoose = require('mongoose');

const { Schema } = mongoose;

const CardSchema = new Schema({
  name: {
    type: String,
    required: true,
    trim: true,
  },
  description: {
    type: String,
    required: true,
    trim: true,
  },
  isPersistent: {
    type: Boolean,
    default: false,
  },
});

module.exports = mongoose.model('Card', CardSchema);
