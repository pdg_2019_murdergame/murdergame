const mongoose = require('mongoose');

const { Schema } = mongoose;

const playerSchema = new Schema(
  {
    pseudo: {
      type: String,
      trim: true,
      unique: true,
      required: true,
      minlength: 10, // 3 chars min for the nickname + "-<partyCode>", party code has 6 chars
      maxlength: 23, // 16 chars max for the nickname + "-<partyCode>", party code has 6 chars
      get: v => v.substring(0, v.lastIndexOf('-')), // nickname format = <nickname>-<partyCode>, we don't want the '-' and the game code
    },
    team: {
      type: String,
      enumValues: ['citizen', 'cabal'],
      default: 'citizen',
    },
    cards: [
      {
        type: mongoose.Schema.ObjectId,
        ref: 'Card',
      },
    ],
    informations: [
      {
        type: mongoose.Schema.ObjectId,
        ref: 'Information',
      },
    ],
    deathTurn: {
      type: Number,
      min: 0,
      max: 255,
      default: 0,
    },
    victory: {
      type: Boolean,
      default: null,
    },
  },
  { id: false },
);

const Player = mongoose.model('Player', playerSchema);
module.exports = Player;
