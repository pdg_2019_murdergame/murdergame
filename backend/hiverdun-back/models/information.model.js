const mongoose = require('mongoose');

const { Schema } = mongoose;

const InformationSchema = new Schema({
  victim: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Player',
    required: true,
  },
  team: {
    type: String,
    enumValues: ['village', 'cabal'],
  },
  cards: [
    {
      type: mongoose.Schema.ObjectId,
      ref: 'Card',
    },
  ],
});

module.exports = mongoose.model('Information', InformationSchema);
