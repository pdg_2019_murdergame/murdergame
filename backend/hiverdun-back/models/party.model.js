const mongoose = require('mongoose');
const Chance = require('chance');
const { playermax } = require('../io/manager/party.config');

const chance = new Chance();

const { Schema } = mongoose;

const PartySchema = new Schema({
  code: {
    type: String,
    default() {
      return chance.string({
        length: 6,
        casing: 'upper',
        alpha: true,
        numeric: true,
      });
    },
    unique: true,
    minlength: 6,
    maxlength: 6,
  },
  players: {
    type: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Player',
      },
    ],
  },
  beginDate: {
    type: Date,
    default: Date.now,
  },
  endDate: {
    type: Date,
  },
  victory: {
    type: String,
    enumValues: ['citizen', 'cabal'],
    default: null,
  },
  turn: {
    type: Number,
    default: 0,
    min: 0,
    max: 255,
  },
});

PartySchema.pre('findOneAndUpdate', async function () {
  const partyToUpdate = await this.model.findOne(this.getQuery());

  if (partyToUpdate.players.length >= playermax) {
    throw new Error('Too many player in this party');
  }
});

module.exports = mongoose.model('Party', PartySchema);
