class IoClient {
  /**
   * Hides some business logic from controller
   * @param {PartyManager} manager
   * @param {SocketIO-ID} socketid
   */
  constructor(manager, socketid) {
    this.socketid = socketid;
    this.manager = manager;

    const { party, nickname } = this.manager.getInfosById(socketid);

    this.code = party;
    this.nickname = nickname;

    this.context = this.manager.getParty(this.code);
    this.player = this.context.getPlayer(this.nickname);
  }

  setOnline(bool) {
    this.context.setPlayerOnline(this.nickname, bool);
  }

  removeClient() {
    this.manager.removeClientById(this.socketid);
  }

  /**
   * Prevents returning null if this client is MASTER
   * @returns {string} name
   */
  get safename() {
    return this.nickname || 'Anon';
  }

  /**
   * @returns {string} cabal room name
   */
  get codecabal() {
    return `${this.code}-cabal`;
  }

  /**
   * @returns {boolean} true if this client is player
   */
  isPlayer() {
    return this.player && true;
  }

  /**
   * @returns {boolean} true if this client is master
   */
  isMaster() {
    return !this.isPlayer();
  }
}

module.exports = { IoClient };
