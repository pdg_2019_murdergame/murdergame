const io = require('socket.io');
const { verify } = require('../service/jwt.service');
const { PartyManager } = require('./manager/party.manager');
const { EventNames } = require('./io.config');
const { Callback } = require('./io.callback');
const { IoClient } = require('./io.client');

/**
 * Protects the Event-Based API endpoints
 * @param {PartyManager} manager
 */
const tokenmiddleware = manager => (socket, next) => {
  const { token } = socket.handshake.query;
  try {
    const { code, nickname } = verify(token);

    console.log('start middleware', code, nickname);
    manager.addClientById(socket.id, code, nickname);

    console.log('clients', manager.clients);

    return next();
  } catch (err) {
    return next(new Error('authentication error'));
  }
};

const disconnect = reason => {
  console.log('user disconnected: ', reason);
};

/**
 * Handle socket.io stuff
 */
class IoEntry {
  /**
   * Construct the socket.io
   * @param {*} server take the express server
   */
  constructor(server) {
    this.srvio = io(server);
    this.manager = new PartyManager();

    this.init();
  }

  init() {
    // Binding this middleware
    this.srvio.use(tokenmiddleware(this.manager));

    this.srvio.on('connection', socket => {
      console.log('a user connected');

      const client = new IoClient(this.manager, socket.id);
      client.setOnline(true);

      // Joining a room
      socket.join(client.code, Callback.cbJoinRoom(this.srvio, socket, client, client.code));

      // Starting a game
      socket.on(EventNames.start, Callback.cbStartGame(this.srvio, client));

      // Skip event
      socket.on(EventNames.skip, Callback.cbSkipStory(this.srvio, client));

      // Vote
      socket.on(EventNames.vote, Callback.cbVotes(this.srvio, client));

      // Vote Cabal
      socket.on(EventNames.votecabal, Callback.cbVotesCabal(this.srvio, client));

      // Personnal information ME
      socket.on(EventNames.me, Callback.cbMe(this.srvio, socket, client));

      // Card rejection
      socket.on(EventNames.reject, Callback.cbReject(client));

      // Play card
      socket.on(EventNames.play, Callback.cbPlayCard(socket, client));

      // Get informations of an other player
      socket.on(EventNames.getinfo, Callback.cbGetInfo(socket, client));

      // Pass
      socket.on(EventNames.pass, Callback.cbPassToNextState(this.srvio, client));

      // Next
      socket.on(EventNames.verdict, Callback.cbVerdict(this.srvio, client));

      // Ask Winner
      socket.on(EventNames.askwinner, Callback.cbAskWinner(this.srvio, client));

      // Leaving
      socket.on('disconnecting', Callback.cbDisconnecting(this.srvio, socket, client));

      socket.on('disconnect', disconnect);
    });
  }
}

module.exports = { IoEntry };
