/**
 * Use this constants to use the correct event names
 */
const EventNames = Object.freeze({
  cardplayed: 'cardplayed', // ->
  playerslist: 'playerslist', // ->
  caballist: 'cabal-info', // ->
  enter: 'roomenter',
  gamestate: 'gamestate', // ->
  getinfo: 'getinfo', // <-
  info: 'info', // ->
  join: 'roomjoin', // ->
  leave: 'roomleave', // ->
  me: 'me', // <->
  reject: 'reject', // <-
  size: 'gamesize', // ->
  start: 'gamestart', // <-
  skip: 'skip', // <-
  vote: 'vote', // <-
  votecabal: 'cabal-vote', // <-
  votes: 'voteslist', // ->
  votescabal: 'cabal-voteslist', // ->
  play: 'playcard', // <-
  pass: 'pass',
  lastdeads: 'lastdeads', // ->
  verdict: 'verdict', // <-
  winner: 'winner', // ->
  askwinner: 'askwinner', // <-
});

module.exports = { EventNames };
