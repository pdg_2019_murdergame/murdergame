const services = require('../../service/services');

const service = services.players;

const TeamsEnum = Object.freeze({ cabal: 'cabal', citizen: 'citizen', unkown: '' });
const { CardsMap } = require('./cards/enum.cards');
const { CardsEnum } = require('./cards/enum.cards');

class Player {
  constructor(nickname) {
    this.online = true;
    this.nickname = nickname;
    this.alive = true;
    this.vote = null;
    this.team = TeamsEnum.citizen;
    this.cards = [];
    this.newcard = null;
    this.infos = new Set([]);
    this.pass = false;
    this.deathturn = null;
  }

  addCard(card) {
    this.newcard = card;
  }

  setTeam(team) {
    this.team = team;
  }

  setAlive(context, life) {
    // We want to resurrect the player
    if (life) {
      this.alive = true;
      context.deleteDeadPlayer(this);
    } else {
      const index = this.cards.indexOf(CardsEnum.hardskin);
      // The player has the Hard Skin card, he is not dead
      if (index !== -1) {
        this.cards.splice(index, 1);
      } else {
        this.alive = false;
        context.addDeadPlayer(this);
        this.deathturn = context.turn;
      }
    }
  }

  setVote(context, target) {
    if (
      context.getPlayer(target) !== undefined &&
      (context.isVoteOpen() || this.team === TeamsEnum.cabal)
    ) {
      this.vote = target;
      return true;
    }
    this.vote = null;
    return false;
  }

  addPlayerInfo(target) {
    this.infos.add(target);
  }

  getOtherPlayerInfo(context, target) {
    const otherPlayer = context.getPlayer(target);

    if (otherPlayer === undefined) return null;

    if (this.infos.has(otherPlayer.nickname)) {
      return otherPlayer.getMe();
    }

    return otherPlayer.getInfo();
  }

  getInfo() {
    return {
      nickname: this.nickname,
      alive: this.alive,
      online: this.online,
      team: TeamsEnum.unkown,
    };
  }

  getMe() {
    return {
      nickname: this.nickname,
      alive: this.alive,
      cards: this.cards,
      online: this.online,
      team: this.team,
      newcard: this.newcard,
    };
  }

  removeCard(cardId) {
    const card = CardsMap.get(cardId);

    // Try to remove a persistent card
    // The user can remove a persistent card only if it's the new one and he has 3 persistent cards
    if (card !== undefined && card.persistent) {
      if (this.cards.length < 3) return;

      const isAllPersistent = this.cards.reduce((acc, value) => {
        return acc && CardsMap.get(value).persistent;
      }, true);

      if (!isAllPersistent) {
        return;
      }

      // Player has 3 persistent card
      this.newcard = null;
      return;
    }

    const index = this.cards.indexOf(cardId);

    // The rejected card is in the hand
    if (index !== -1) {
      this.cards.splice(index, 1);
    }

    if (this.cards.length < 3 && this.newcard !== null) {
      this.cards.push(this.newcard);
    }

    this.newcard = null;
  }

  isCabal() {
    if (this.team === TeamsEnum.cabal) {
      return true;
    }
    return false;
  }

  setPass(bool) {
    this.pass = bool;
  }

  /**
   * Saves this player in DB
   * @param {PartyContext} context
   */
  save(context) {
    service.save(`${this.nickname}-${context.code}`, this.deathturn, this.team, this.alive);
  }
}

module.exports = { Player, TeamsEnum };
