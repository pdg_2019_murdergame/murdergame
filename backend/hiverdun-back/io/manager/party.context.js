const services = require('../../service/services');

const service = services.parties;

const { LobbyState, FirstDayState, DayState, NightState } = require('./states/states');
const { Player, TeamsEnum } = require('./player');
const { CardsEnum } = require('./cards/enum.cards');
const { FactoryCardReq } = require('./cards/factory.cardreq');
const { EnumQueues } = require('./cards/abstract.cardreq');

/**
 * Represents the context of a running party
 */
class PartyContext {
  constructor(code) {
    this.code = code;
    this.players = new Map();
    this.self = this;
    this.state = new LobbyState(this.self); // We start in lobbystate
    this.deck = null;

    this.nightendqueue = [];
    this.dawnendqueue = [];

    this.storyEnd = false;

    this.turn = 0;

    this.lastdeads = new Set([]);

    this.everyoneHasVoted = false;

    this.allCabalDead = false;
    this.allCitizenDead = false;

    this.factory = new FactoryCardReq(this);
  }

  // State

  /**
   * Sets new state
   * @param {AbstractState} newState
   */
  changeState(newState) {
    this.state = newState;
    return this.state;
  }

  /**
   * Next State in the state graph / pattern
   * may not change if conditions not met yet
   * @returns {boolean} returns true if state changed
   */
  nextState() {
    return this.state.doTransition();
  }

  /**
   * @returns {string} Returns GameState Name as string
   */
  getGameState() {
    return this.state.constructor.name;
  }

  skipStory() {
    this.storyEnd = true;
  }

  isNight() {
    return this.state instanceof NightState;
  }

  // Party Context

  getRoomSize() {
    const list = this.getPlayersList();
    const size = list.reduce((acc, val) => acc + val.online, 0);
    return size;
  }

  getContext() {
    const obj = this.getPlayersList().map(p => p.getInfo());
    return obj;
  }

  // Player

  /**
   *
   * @param {String} nickname
   * @returns {Player} player object
   */
  getPlayer(nickname) {
    return this.players.get(nickname);
  }

  getPlayersList() {
    return [...this.players.values()];
  }

  getCabalList() {
    return this.getPlayersList()
      .filter(p => p.alive)
      .filter(p => p.team === TeamsEnum.cabal);
  }

  addPlayer(nickname) {
    if (!this.players.has(nickname) && nickname) {
      const player = new Player(nickname);
      this.players.set(nickname, player);
    }
  }

  setPlayerOnline(nickname, bool) {
    if (this.players.has(nickname)) {
      this.players.get(nickname).online = bool;
    }
  }

  isVoteOpen() {
    return this.state instanceof FirstDayState || this.state instanceof DayState;
  }

  getPlayersVoteList() {
    return this.getPlayersList()
      .filter(p => p.alive)
      .reduce((acc, p) => {
        const targetnickname = p.vote ? p.vote : '';
        acc.push({ player: p.nickname, target: targetnickname });
        return acc;
      }, []);
  }

  /**
   * Returns true if alive players have passed the round
   */
  isTurnFinished() {
    return this.getPlayersList()
      .filter(p => p.alive)
      .reduce((acc, p) => {
        return acc && (p.pass || !p.alive);
      }, true);
  }

  resetPlayers() {
    this.getPlayersList().forEach(p => {
      p.setPass(false);
      p.setVote(this, null);
    });
  }

  /**
   * Iterates over players and find the one with most votes against him
   */
  findGuilty() {
    const result = this.getPlayersList()
      .filter(p => p.alive)
      .filter(p => p.online)
      .reduce(
        (acc, p) => {
          // If the player doesn't exist, next
          if (!this.getPlayer(p.vote)) return acc;

          let count = acc.map.get(p.vote);

          if (count !== undefined) {
            count += 1;
            acc.map.set(p.vote, count);
          } else {
            count = 1;
            acc.map.set(p.vote, count);
          }

          // Update the max
          if (count > acc.max.count) {
            acc.max.guilty = p.vote;
            acc.max.count = count;
            acc.max.equal = false;
          } else if (count === acc.max.count) {
            acc.max.equal = true;
          }

          return acc;
        },
        { map: new Map([]), max: { guilty: '', count: 0, equal: true } },
      );

    return result.max;
  }

  resolveVote() {
    const result = this.findGuilty();

    if (result.equal === false && result.guilty) {
      // TODO: Maybe add the reason of death, nice to have
      const p = this.getPlayer(result.guilty);
      p.setAlive(this, false);
      return result.guilty;
    }

    return '';
  }

  isGameOver() {
    const result = this.getPlayersList()
      .filter(p => p.alive === true)
      .reduce(
        (acc, p) => {
          if (p.team === TeamsEnum.cabal) {
            acc.cabal += 1;
          } else if (p.alive) {
            acc.citizen += 1;
          }

          return acc;
        },
        { cabal: 0, citizen: 0 },
      );

    this.allCabalDead = result.cabal === 0;
    this.allCitizenDead = result.citizen === 0;

    const bool = this.allCabalDead || this.allCitizenDead;

    if (bool) {
      this.save();
    }

    return bool;
  }

  // Cards

  distribute() {
    this.getPlayersList()
      .filter(p => p.alive)
      .forEach(p => {
        const card = this.deck.draw(p.team === TeamsEnum.cabal);
        p.addCard(card);

        if (card === CardsEnum.sacrifice) {
          p.setTeam(TeamsEnum.cabal);
        }
      });
  }

  playCard(cardid, issuer, target) {
    // The player should exist
    const player = this.getPlayer(issuer);
    if (!player) return -1;

    // The player should have the card in his hand
    const index = player.cards.indexOf(cardid);
    if (index === -1) return -1;

    const cardreq = this.factory.makeCardReq(cardid, issuer, target);

    // The player should have the right amount of card in his hands
    if (player.cards.filter(c => c === cardid).length < cardreq.nbrequire) return -1;

    switch (cardreq.queue) {
      // We have to execute now the req
      case EnumQueues.instant:
        cardreq.do();
        break;
      case EnumQueues.nightend:
        this.nightendqueue.push(cardreq);
        break;
      case EnumQueues.dawnend:
        this.dawnendqueue.push(cardreq);
        break;
      default:
        // Invoking some mystic arts
        console.log('Donnez-nous 6 :D !');
        return -1;
    }

    player.cards = player.cards.filter(card => card !== cardid);

    return cardid;
  }

  execCardsNight() {
    this.nightendqueue.sort((a, b) => a.priority - b.priority).forEach(c => c.do());
    this.nightendqueue = [];
  }

  execCardsDawn() {
    this.dawnendqueue.sort((a, b) => a.priority - b.priority).forEach(c => c.do());
    this.dawnendqueue = [];
  }

  // LAST DEADS

  /**
   * push a player into last deads
   * @param {Player} p
   */
  addDeadPlayer(p) {
    this.lastdeads.add(p);
  }

  /**
   * remove a player from last deads
   * @param {Player} p
   */
  deleteDeadPlayer(p) {
    this.lastdeads.delete(p);
  }

  /**
   * Resets the dead players set
   */
  resetDeadPlayers() {
    this.lastdeads = new Set([]);
  }

  obtainLastDeads() {
    const array = [...this.lastdeads];
    // In case it's day, we have to give full report
    if (this.isVoteOpen() || this.isNight()) {
      return array.map(p => p.getMe());
    }
    return array.map(p => p.getInfo());
  }

  incrementTurn() {
    this.turn += 1;
  }

  save() {
    const winnerTeam = this.allCabalDead ? TeamsEnum.citizen : TeamsEnum.cabal;
    service.save(this.code, winnerTeam, this.turn);
    this.getPlayersList().forEach(p => {
      p.save(this);
    });
  }

  getWinner() {
    if (this.allCabalDead && this.allCitizenDead) {
      return 'equality';
    }

    if (this.allCabalDead) {
      return TeamsEnum.citizen;
    }

    return TeamsEnum.cabal;
  }

  setEveryoneHasVoted(value) {
    this.everyoneHasVoted = value;
  }
}

module.exports = { PartyContext };
