const Chance = require('chance');

const { CardsEnum } = require('./cards/enum.cards');

const chance = new Chance();

const GameSizeConfig = Object.freeze({
  small: {
    nbMin: 8,
    nbCabalists: 2,
    startingCards: [CardsEnum.prophecy],
    uniqueCards: [CardsEnum.ankh, CardsEnum.curse, CardsEnum.prayer, CardsEnum.hardskin],
    uniqueCabalCards: [CardsEnum.heir],
    regularCards: [CardsEnum.nothing, CardsEnum.clue, CardsEnum.burgle],
    cabalCards: [CardsEnum.nothing],
  },
  medium: {
    nbMin: 12,
    nbCabalists: 2,
    startingCards: [CardsEnum.prophecy],
    uniqueCards: [CardsEnum.ankh, CardsEnum.curse, CardsEnum.prayer, CardsEnum.hardskin],
    uniqueCabalCards: [CardsEnum.heir],
    regularCards: [CardsEnum.nothing, CardsEnum.clue, CardsEnum.burgle],
    cabalCards: [CardsEnum.nothing],
  },
  big: {
    nbMin: 18,
    nbCabalists: 4,
    startingCards: [CardsEnum.prophecy],
    uniqueCards: [CardsEnum.ankh, CardsEnum.curse, CardsEnum.prayer, CardsEnum.hardskin],
    uniqueCabalCards: [CardsEnum.heir],
    regularCards: [CardsEnum.nothing, CardsEnum.clue, CardsEnum.burgle],
    cabalCards: [CardsEnum.nothing],
  },
});

class Deck {
  constructor(nbPlayers) {
    this.deck = [];
    this.alreadyDistributed = new Set([]);
    this.alreadyDistributedCabal = new Set([]);

    this.nbPlayers = nbPlayers;
    this.config = GameSizeConfig.small;
    this.calcGameSize(nbPlayers);

    this.init();
  }

  init() {
    const cabalArray = Deck.makeArrayCabal(this.config.nbCabalists);

    const restToFill = this.nbPlayers - this.config.nbCabalists - this.config.startingCards.length;

    let uniques = [];
    if (restToFill > 0) {
      uniques = Array(restToFill)
        .fill()
        .map(() => this.takeUniqueCard());
    }

    this.deck = this.config.startingCards.concat(cabalArray, uniques);
    this.shuffle();
  }

  /**
   * @param {Number} n
   * @returns {Array} array filled with sacrifice cards
   */
  static makeArrayCabal(n) {
    return Array(n).fill(CardsEnum.sacrifice);
  }

  shuffle() {
    for (let i = 0; i < this.deck.length; i += 1) {
      const pos = Deck.randomNumber(i, this.deck.length - 1);
      const val = this.deck[i];
      this.deck[i] = this.deck[pos];
      this.deck[pos] = val;
    }
  }

  fillRest() {
    const rest = this.nbPlayers - this.deck.length;
    if (rest > 0) {
      this.deck = this.deck.concat(this.makeArrayRest(rest));
    }
  }

  /**
   * @param {Number} n
   * @returns {Array} array filled with regular cards
   */
  makeArrayRest(n) {
    return Array(n)
      .fill()
      .map(() => (chance.bool({ likelihood: 20 }) ? this.takeUniqueCard() : this.takeBasicCard()));
  }

  refill() {
    this.fillRest(); // REFACTOR
    this.shuffle(this.deck);
  }

  draw(isCabal) {
    if (this.deck.length === 0) {
      return 99;
    }

    if (isCabal) {
      return chance.bool({ likelihood: 20 }) ? this.takeUniqueCabalCard() : this.takeCabalCard();
    }
    return this.deck.pop();
  }

  calcGameSize(n) {
    if (n >= GameSizeConfig.big.nbMin) {
      this.config = GameSizeConfig.big;
    } else if (this.nbPlayers >= GameSizeConfig.medium.nbMin) {
      this.config = GameSizeConfig.medium;
    }
  }

  /**
   * @returns a regular card from the list
   */
  takeBasicCard() {
    return Deck.takeCard(this.config.regularCards);
  }

  takeCabalCard() {
    return Deck.takeCard(this.config.cabalCards);
  }

  takeUniqueCard() {
    const card = Deck.takeCard(this.config.uniqueCards);
    if (this.alreadyDistributed.has(card)) {
      return this.takeBasicCard();
    }

    this.alreadyDistributed.add(card);
    return card;
  }

  takeUniqueCabalCard() {
    const card = Deck.takeCard(this.config.uniqueCabalCards);
    if (this.alreadyDistributedCabal.has(card)) {
      return this.takeCabalCard();
    }

    this.alreadyDistributedCabal.add(card);
    return card;
  }

  static takeCard(array) {
    const max = array.length - 1;
    const min = 0;
    const pos = Deck.randomNumber(min, max);

    return array[pos];
  }

  /**
   * utility
   * @param {Number} min
   * @param {Number} max
   */
  static randomNumber(min, max) {
    return chance.integer({ min, max });
  }
}

module.exports = { Deck };
