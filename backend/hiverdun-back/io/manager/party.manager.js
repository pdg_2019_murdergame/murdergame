const { PartyContext } = require('./party.context');

/**
 * Keeps track of all running parties and clients
 */
class PartyManager {
  constructor() {
    this.clients = new Map();
    this.parties = new Map();
  }

  /**
   * Register client
   * @param {Number} id id of a socket client
   * @param {String} party code of the party he's in
   * @param {String} nickname nickname of the client
   */
  addClientById(id, party, nickname) {
    this.clients.set(id, { party, nickname });

    this.addParty(party);

    this.parties.get(party).addPlayer(nickname);
  }

  /**
   * Recover the party code and nickname of a client
   * @param {idNumber} id id of socket client
   */
  getInfosById(id) {
    return this.clients.get(id);
  }

  /**
   * Remove client
   * @param {Number} id id of socket client
   */
  removeClientById(id) {
    this.clients.delete(id);
  }

  /**
   * Add a party
   * @param {PartyContext} party to add
   */
  addParty(party) {
    if (!this.parties.has(party)) {
      const context = new PartyContext(party);

      this.parties.set(party, context);
    }
  }

  /**
   * Get a party context
   * @param party to get
   * @returns {PartyContext} party wanted
   */
  getParty(party) {
    return this.parties.get(party);
  }

  /**
   * Get the number of player in the party
   * @param party that we want to know the number of player
   * @returns {number}
   */
  getRoomSize(party) {
    const pc = this.getParty(party);
    if (pc) {
      return pc.getRoomSize();
    }

    return 0;
  }
}

module.exports = { PartyManager };
