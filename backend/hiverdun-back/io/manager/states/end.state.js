const { AbstractState } = require('./abstract.state');

class EndState extends AbstractState {
  constructor(context) {
    super(context);
    this.saved = false;
  }

  doTransition() {
    return this.saved;
  }
}

module.exports = { EndState };
