const { AbstractState } = require('./abstract.state');
const { EndState } = require('./end.state');

/**
 * The state of the game at night
 */
class NightState extends AbstractState {
  doTransition() {
    if (this.context.isTurnFinished()) {
      this.context.resetDeadPlayers();
      this.context.resolveVote();
      this.context.resetPlayers();
      this.context.execCardsNight();

      if (this.context.isGameOver()) {
        this.context.state = new EndState(this.context);
  
        return true;
      }
      
      this.context.incrementTurn();

      const newState = new DawnState(this.context);
      this.context.changeState(newState);

      return true;
    }
    return false;
  }
}

module.exports = { NightState };
const { DawnState } = require('./dawn.state');
