const { FirstNightState } = require('./firstnight.state');
const { Deck } = require('../deck');

class StoryState {
  constructor(context) {
    this.context = context;
  }

  doTransition() {
    if (this.context.storyEnd) {
      const newState = new FirstNightState(this.context);
      this.context.changeState(newState);

      this.context.deck = new Deck(this.context.players.size);

      this.context.distribute();
      return true;
    }
    return false;
  }
}

module.exports = { StoryState };
