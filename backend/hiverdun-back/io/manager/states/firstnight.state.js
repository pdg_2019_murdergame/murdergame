const { AbstractState } = require('./abstract.state');
const { FirstDawnState } = require('./firstdawn.state');

class FirstNightState extends AbstractState {
  doTransition() {
    if (this.context.isTurnFinished()) {
      this.context.resetDeadPlayers();
      this.context.resolveVote();
      this.context.resetPlayers();
      this.context.execCardsNight();
      this.context.incrementTurn();

      const newState = new FirstDawnState(this.context);
      this.context.changeState(newState);
      return true;
    }
    return false;
  }
}

module.exports = { FirstNightState };
