const { AbstractState } = require('./abstract.state');
const { StoryState } = require('./story.state');
const { playermin } = require('../party.config');

class LobbyState extends AbstractState {
  doTransition() {
    if (this.context.players.size >= playermin) {
      const newState = new StoryState(this.context);
      this.context.changeState(newState);
      return true;
    }
    return false;
  }
}

module.exports = { LobbyState };
