const { AbstractState } = require('./abstract.state');
const { DayState } = require('./day.state');
const { EndState } = require('./end.state');

class DawnState extends AbstractState {
  doTransition() {
    if (this.context.isTurnFinished()) {
      this.context.resetPlayers();
      this.context.execCardsDawn();

      if (this.context.isGameOver()) {
        this.context.state = new EndState(this.context);

        return true;
      }

      this.context.state = new DayState(this.context);

      return true;
    }
    return false;
  }
}

module.exports = { DawnState };
