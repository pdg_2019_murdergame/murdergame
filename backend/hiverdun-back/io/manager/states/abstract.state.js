class AbstractState {
  constructor(context) {
    this.context = context;
  }

  // eslint-disable-next-line class-methods-use-this
  doTransition() {
    throw new Error('Not implemented');
  }
}

module.exports = { AbstractState };
