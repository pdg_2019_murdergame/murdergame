const { AbstractState } = require('./abstract.state');
const { FirstDayState } = require('./firstday.state');

class FirstDawnState extends AbstractState {
  doTransition() {
    if (this.context.isTurnFinished()) {
      const newState = new FirstDayState(this.context);
      this.context.resetPlayers();
      this.context.execCardsDawn();
      this.context.changeState(newState);
      return true;
    }
    return false;
  }
}

module.exports = { FirstDawnState };
