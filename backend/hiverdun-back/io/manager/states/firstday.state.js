const { AbstractState } = require('./abstract.state');
const { NightState } = require('./night.state');

class FirstDayState extends AbstractState {
  doTransition() {
    this.context.resolveVote();

    if (this.context.everyoneHasVoted) {
      this.context.resetDeadPlayers();
      this.context.resolveVote();
      this.context.resetPlayers();
      this.context.deck.refill();
      this.context.distribute();
      this.context.state = new NightState(this.context);

      return true;
    }
    return false;
  }
}

module.exports = { FirstDayState };
