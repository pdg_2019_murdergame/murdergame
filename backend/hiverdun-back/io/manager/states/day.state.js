const { AbstractState } = require('./abstract.state');
const { EndState } = require('./end.state');

class DayState extends AbstractState {
  doTransition() {
    if (this.context.everyoneHasVoted) {
      this.context.resetDeadPlayers();
      this.context.resolveVote();

      if (this.context.isGameOver()) {
        this.context.state = new EndState(this.context);

        return true;
      }

      this.context.resetPlayers();
      this.context.deck.refill();
      this.context.distribute();
      this.context.state = new NightState(this.context);

      return true;
    }

    return false;
  }
}

module.exports = { DayState };
const { NightState } = require('./night.state');
