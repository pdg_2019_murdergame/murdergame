const { AbstractCardReq, EnumQueues } = require('./abstract.cardreq');

class SacrificeCardReq extends AbstractCardReq {
  constructor(context, issuer, target) {
    super(context, issuer, target, EnumQueues.nightend, 60, true, 1);
  }

  do() {
    const targetplayer = this.context.getPlayer(this.target);
    targetplayer.setAlive(this.context, false);
  }
}

module.exports = { SacrificeCardReq };
