const { NothingCardReq } = require('./nothing.cardreq');
const { AnkhCardReq } = require('./ankh.cardreq');
const { CurseCardReq } = require('./curse.cardreq');
const { HeirCardReq } = require('./heir.cardreq');
const { PrayerCardReq } = require('./prayer.cardreq');
const { ProphecyCardReq } = require('./prophecy.cardreq');
const { BurgleCardReq } = require('./burgle.cardreq');
const { SacrificeCardReq } = require('./sacrifice.cardreq');
const { ClueCardReq } = require('./clue.cardreq');

const CardsEnum = Object.freeze({
  nothing: 0,
  prophecy: 1,
  ankh: 2,
  curse: 3,
  heir: 4,
  prayer: 5,
  sacrifice: 6,
  burgle: 8,
  hardskin: 9,
  clue: 14,
});

const initializer = [
  [
    CardsEnum.nothing,
    {
      Constructor: NothingCardReq,
      persistent: false,
      nbrequire: 1,
    },
  ],
  [
    CardsEnum.prophecy,
    {
      Constructor: ProphecyCardReq,
      persistent: true,
      nbrequire: 1,
    },
  ],
  [
    CardsEnum.ankh,
    {
      Constructor: AnkhCardReq,
      persistent: false,
      nbrequire: 1,
    },
  ],
  [
    CardsEnum.curse,
    {
      Constructor: CurseCardReq,
      persistent: false,
      nbrequire: 1,
    },
  ],
  [
    CardsEnum.heir,
    {
      Constructor: HeirCardReq,
      persistent: false,
      nbrequire: 1,
    },
  ],
  [
    CardsEnum.sacrifice,
    {
      Constructor: SacrificeCardReq,
      persistent: true,
      nbrequire: 1,
    },
  ],
  [
    CardsEnum.prayer,
    {
      Constructor: PrayerCardReq,
      persistent: true,
      nbrequire: 1,
    },
  ],
  [
    CardsEnum.burgle,
    {
      Constructor: BurgleCardReq,
      persistent: false,
      nbrequire: 1,
    },
  ],
  [
    CardsEnum.hardskin,
    {
      persistent: false,
      nbrequire: 1,
    },
  ],
  [
    CardsEnum.clue,
    {
      Constructor: ClueCardReq,
      persistent: false,
      nbrequire: 1,
    },
  ],
];

const CardsMap = Object.freeze(new Map(initializer));

module.exports = { CardsEnum, CardsMap };
