const EnumQueues = Object.freeze({ instant: 0, nightend: 1, dawnend: 2 });

class AbstractCardReq {
  /**
   * Use this as base class for card requests
   * @param {PartyContext} context
   * @param {String} issuer
   * @param {String} target
   * @param {EnumQueues} queue
   * @param {Number} priority
   */
  constructor(context, issuer, target, queue, priority, persistent, nbrequire) {
    this.context = context;
    this.priority = priority;
    this.queue = queue;
    this.issuer = issuer;
    this.target = target;
    this.persistant = persistent;
    this.nbrequire = nbrequire;
  }

  do() {

  }
}

module.exports = { AbstractCardReq, EnumQueues };
