const { AbstractCardReq, EnumQueues } = require('./abstract.cardreq');

class ProphecyCardReq extends AbstractCardReq {
  constructor(context, issuer, target) {
    super(context, issuer, target, EnumQueues.instant, 0, true, 1);
  }

  do() {
    const player = this.context.getPlayer(this.issuer);
    const otherPlayer = this.context.getPlayer(this.target);

    if (player === undefined || otherPlayer === undefined) return;

    player.addPlayerInfo(this.target);
  }
}

module.exports = { ProphecyCardReq };
