const { AbstractCardReq, EnumQueues } = require('./abstract.cardreq');

class PrayerCardReq extends AbstractCardReq {
  constructor(context, issuer, target) {
    super(context, issuer, target, EnumQueues.nightend, 40, true, 1);
  }

  do() {
    const targetplayer = this.context.getPlayer(this.target);

    if (this.context.lastdeads.has(targetplayer)) {
      targetplayer.setAlive(this.context, true);
    }
  }
}

module.exports = { PrayerCardReq };
