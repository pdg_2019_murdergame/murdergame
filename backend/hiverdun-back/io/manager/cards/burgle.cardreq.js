const { AbstractCardReq, EnumQueues } = require('./abstract.cardreq');

class BurgleCardReq extends AbstractCardReq {
  constructor(context, issuer, target) {
    super(context, issuer, target, EnumQueues.nightend, 500, false, 1);
  }

  do() {
    const targetPlayer = this.context.getPlayer(this.target);
    const issuerPlayer = this.context.getPlayer(this.issuer);

    if (targetPlayer.team === 'citizen') {
      const card = targetPlayer.cards.pop();

      if (card !== undefined) {
        issuerPlayer.cards.push(card);
      }
    } else {
      issuerPlayer.setAlive(this.context, false);
    }
  }
}

module.exports = { BurgleCardReq };
