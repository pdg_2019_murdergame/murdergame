const { AbstractCardReq, EnumQueues } = require('./abstract.cardreq');

class CurseCardReq extends AbstractCardReq {
  constructor(context, issuer, target) {
    super(context, issuer, target, EnumQueues.dawnend, 100, false, 1);
  }

  do() {
    const targetplayer = this.context.getPlayer(this.target);
    targetplayer.setAlive(this.context, false);
  }
}

module.exports = { CurseCardReq };
