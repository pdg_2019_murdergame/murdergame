const { CardsMap } = require('../cards/enum.cards');

class FactoryCardReq {
  constructor(context) {
    this.context = context;
  }

  /**
   *
   * @param {Number} cardid
   */
  makeCardReq(cardid, issuer, target) {
    const card = CardsMap.get(cardid);

    if (card === undefined) {
      return null;
    }

    return new card.Constructor(this.context, issuer, target);
  }
}

module.exports = { FactoryCardReq };
