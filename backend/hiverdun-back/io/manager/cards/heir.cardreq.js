const { AbstractCardReq, EnumQueues } = require('./abstract.cardreq');

class HeirCardReq extends AbstractCardReq {
  constructor(context, issuer, target) {
    super(context, issuer, target, EnumQueues.nightend, 200, false, 1);
  }

  do() {
    const player = this.context.getPlayer(this.issuer);
    const otherPlayer = this.context.getPlayer(this.target);

    if (player === undefined || otherPlayer === undefined) return;

    if (!player.alive) {
      otherPlayer.setTeam('cabal');
      otherPlayer.cards = [6];
      otherPlayer.newcard = null;
    } else if (
      this.context.getGameState() === 'FirstNightState' ||
      this.context.getGameState() === 'NightState'
    ) {
      this.context.dawnendqueue.push(this);
    } else {
      this.context.nightendqueue.push(this);
    }
  }
}

module.exports = { HeirCardReq };
