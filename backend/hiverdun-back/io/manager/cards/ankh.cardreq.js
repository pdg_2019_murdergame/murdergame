const { AbstractCardReq, EnumQueues } = require('./abstract.cardreq');

class AnkhCardReq extends AbstractCardReq {
  constructor(context, issuer, target) {
    super(context, issuer, target, EnumQueues.dawnend, 100, true, 1);
  }

  do() {
    const targetplayer = this.context.getPlayer(this.target);

    if (this.context.obtainLastDeads(targetplayer)) {
      targetplayer.setAlive(this.context, true);
    }
  }
}

module.exports = { AnkhCardReq };
