const { AbstractCardReq, EnumQueues } = require('./abstract.cardreq');

class NothingCardReq extends AbstractCardReq {
  constructor(context, issuer, target) {
    super(context, issuer, target, EnumQueues.instant, 0, false, 1);
  }

  do() {
    console.log('I am a useless card :D');
  }
}

module.exports = { NothingCardReq };
