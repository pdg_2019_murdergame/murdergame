const { EventNames } = require('./io.config');
const { Emit } = require('./io.emit');

/**
 * When Disconnecting
 * @param {io} iosrv
 * @param {SocketIO} socket
 * @param {IoClient} client
 */
const cbDisconnecting = (iosrv, socket, client) => () => {
  const { context, code, safename } = client;

  client.removeClient();
  client.setOnline(false);
  Emit.Context(iosrv, client);
  iosrv.to(code).emit(EventNames.leave, safename);
  iosrv.to(code).emit(EventNames.size, context.getRoomSize());
};

/**
 * Callback when someone joins a room
 * @param {SocketIO} iosrv
 * @param {Socket} socket
 * @param {IoClient} client
 * @param {string} roomcode
 */
const cbJoinRoom = (iosrv, socket, client, roomcode) => () => {
  const { context, safename } = client;

  socket.emit(EventNames.enter, roomcode);
  // Broadcast name of new player
  iosrv.to(roomcode).emit(EventNames.join, safename);

  // Broadcast roomsize
  Emit.Context(iosrv, client);
  iosrv.to(roomcode).emit(EventNames.size, context.getRoomSize());
};

/**
 * Callback on game start
 * @param {SocketIO} iosrv
 * @param {IoClient} client
 */
const cbStartGame = (iosrv, client) => () => {
  const { context } = client;
  if (client.isMaster() && context.nextState()) {
    Emit.Context(iosrv, client);
    Emit.GameState(iosrv, client);
  }
};

/**
 * Callback on skip story
 * @param {SocketIO} iosrv
 * @param {IoClient} client
 */
const cbSkipStory = (iosrv, client) => () => {
  const { context } = client;

  context.skipStory();
  if (client.isMaster() && context.nextState()) {
    Emit.Context(iosrv, client);
    Emit.GameState(iosrv, client);
  }
};

/**
 * Callback on verdict
 * @param {SocketIO} iosrv
 * @param {IoClient} client
 */
const cbVerdict = (iosrv, client) => () => {
  const { context } = client;

  if (client.isMaster()) {
    context.setEveryoneHasVoted(true);
    if (context.nextState()) {
      context.setEveryoneHasVoted(false);
      Emit.Context(iosrv, client);
      Emit.Votes(iosrv, client);
      Emit.CabalVotes(iosrv, client);
      Emit.GameState(iosrv, client);
      Emit.LastDeads(iosrv, client);
    }
  }
};

/**
 * Callback on next state when a player pass
 * @param {SocketIO} iosrv
 * @param {IoClient} client
 */
const cbPassToNextState = (iosrv, client) => () => {
  const { context, player } = client;

  if (client.isPlayer()) {
    player.setPass(true);
    if (context.nextState()) {
      Emit.Context(iosrv, client);
      Emit.Votes(iosrv, client);
      Emit.CabalVotes(iosrv, client);
      Emit.GameState(iosrv, client);
      Emit.LastDeads(iosrv, client);
    }
  }
};

/**
 * Call back to emit vote
 * @param {io} iosrv
 * @param {IoClient} client
 */
const cbVotes = (iosrv, client) => target => {
  const { context, player } = client;

  // player could be null if sent from master
  if (client.isPlayer() && player.alive) {
    player.setVote(context, target);

    // iosrv.to(code).emit(EventNames.votes, context.getPlayersVoteList());
    Emit.Votes(iosrv, client);
    Emit.CabalVotes(iosrv, client);
  }
};

/**
 * Callback on cabal vote
 * When a cabalist votes
 * @param {SocketIO} iosrv
 * @param {IoClient} client
 */
const cbVotesCabal = (iosrv, client) => target => {
  const { context, player } = client;

  if (client.isPlayer() && player.alive) {
    player.setVote(context, target);
    Emit.CabalVotes(iosrv, client);
  }
};

/**
 * Call back when calls me
 * @param {io} iosrv
 * @param {Socket} socket
 * @param {IoClient} client
 */
const cbMe = (iosrv, socket, client) => () => {
  const { context, player } = client;

  if (client.isPlayer()) {
    socket.emit(EventNames.me, player.getMe());

    // Retrieve the rooms of this socket
    const rooms = Object.keys(socket.rooms);

    if (player.isCabal()) {
      if (!rooms.includes(client.codecabal)) {
        socket.join(client.codecabal, cbJoinRoom(iosrv, socket, client, client.codecabal));
      }

      socket.emit(EventNames.caballist, context.getCabalList());
    }
  }
};

/**
 * Callback on card reject
 * @param {IoClient} client
 */
const cbReject = client => card => {
  const { player } = client;

  player.removeCard(parseInt(card, 10));
};

/**
 * Callback on a card played
 * when a player plays a card
 * @param {SocketIO} iosrv
 * @param {IoClient} client
 */
const cbPlayCard = (socket, client) => payload => {
  const { context, nickname } = client;

  const { cardid, target } = payload;
  const playingPlayer = context.getPlayer(nickname);

  // A player cannot play a card if he is dead
  if (!playingPlayer.alive) {
    socket.emit(EventNames.cardplayed, -1);
    return;
  }

  const cardplayed = context.playCard(parseInt(cardid, 10), nickname, target);

  if (context.getPlayer(target)) {
    socket.emit(EventNames.info, playingPlayer.getOtherPlayerInfo(context, target));
  }

  socket.emit(EventNames.cardplayed, cardplayed);
};

/**
 * Callback on info
 * When a player wants info on another player
 * @param {SocketIO} iosrv
 * @param {IoClient} client
 */
const cbGetInfo = (socket, client) => target => {
  const { context } = client;

  const otherPlayer = context.getPlayer(target);

  if (otherPlayer !== undefined) {
    socket.emit(EventNames.info, otherPlayer.getInfo());
  }
};

/**
 * Callback on askwinner
 * When the game is over, we want to know who is the winner team
 * @param {io} iosrv
 * @param {IoClient} client
 */
const cbAskWinner = (iosrv, client) => () => {
  const { context, code } = client;

  // If the game is not over, we emit an empty string
  const winnerTeam = context.isGameOver() ? context.getWinner() : '';

  iosrv.to(code).emit(EventNames.winner, winnerTeam);
};

const Callback = {
  cbDisconnecting,
  cbStartGame,
  cbSkipStory,
  cbVerdict,
  cbPassToNextState,
  cbVotes,
  cbVotesCabal,
  cbJoinRoom,
  cbMe,
  cbReject,
  cbPlayCard,
  cbGetInfo,
  cbAskWinner,
};

module.exports = { Callback };
