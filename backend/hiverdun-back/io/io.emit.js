const { EventNames } = require('./io.config');

/**
 * Emits context (list of players)
 * to party room
 * @param {Socket.io Server} iosrv
 * @param {IoClient} client
 */
const Context = (iosrv, client) => {
  const { context, code } = client;

  iosrv.to(code).emit(EventNames.playerslist, context.getContext());
};

/**
 * Emits game state
 * to party room
 * @param {Socket.io Server} iosrv
 * @param {IoClient} client
 */
const GameState = (iosrv, client) => {
  const { context, code } = client;

  iosrv.to(code).emit(EventNames.gamestate, context.getGameState());
};

/**
 * Emits last dead players
 * to party room
 * @param {Socket.io Server} iosrv
 * @param {IoClient} client
 */
const LastDeads = (iosrv, client) => {
  const { context, code } = client;

  iosrv.to(code).emit(EventNames.lastdeads, context.obtainLastDeads());
};

/**
 * Emits list of votes
 * to party room
 * @param {Socket.io Server} iosrv
 * @param {IoClient} client
 */
const Votes = (iosrv, client) => {
  const { context, code } = client;
  iosrv.to(code).emit(EventNames.votes, context.getPlayersVoteList());
};

/**
 * Emits list of cabal votes
 * to cabal room
 * @param {Socket.io Server} iosrv
 * @param {IoClient} client
 */
const CabalVotes = (iosrv, client) => {
  const { context, codecabal } = client;

  iosrv.to(codecabal).emit(EventNames.votescabal, context.getPlayersVoteList());
};

/**
 * Emits the winner
 * to party room
 * @param {Socket.io Server} iosrv
 * @param {IoClient} client
 */
const Winner = (iosrv, client) => {
  const { context, code } = client;

  iosrv.to(code).emit(EventNames.winner, context.getWinner());
};

const Emit = {
  Context,
  GameState,
  LastDeads,
  Votes,
  CabalVotes,
  Winner,
};

module.exports = { Emit };
