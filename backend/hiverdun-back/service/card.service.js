class CardService {
  constructor(CardModel) {
    this.Model = CardModel;
  }

  async create() {
    const card = new this.Model();

    return card.save();
  }

  async getOneByCode(code) {
    const card = await this.Model.findOne({ code });

    return card;
  }

  async getAll() {
    const cards = await this.Model.find();

    return cards;
  }
}

module.exports = { CardService };
