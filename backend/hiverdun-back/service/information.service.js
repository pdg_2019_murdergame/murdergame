class InformationService {
  constructor(InformationModel) {
    this.Model = InformationModel;
  }

  async create(player) {
    const information = new this.Model();
    information.victim = player;

    return information.save();
  }
}

module.exports = { InformationService };
