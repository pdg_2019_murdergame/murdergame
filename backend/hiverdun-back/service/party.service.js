class PartyService {
  constructor(PartyModel) {
    this.Model = PartyModel;
  }

  /**
   * Create a new party
   */
  async create() {
    const party = new this.Model();
    return party.save();
  }

  async remove(id) {
    const { n, ok, deletedCount } = await this.Model.deleteOne({ _id: id });

    if (n === 1 && deletedCount === 1) {
      return true;
    }

    return false;
  }

  // eslint-disable-next-line class-methods-use-this
  async update() {
    throw new Error('Not Implemented yet. Get back to work');
  }

  /**
   *
   * @param {String} code
   *
   */
  async getOneByCode(code) {
    const party = await this.Model.findOne({ code });

    return party;
  }

  /**
   * get the list of all parties
   */
  async getAll() {
    const parties = await this.Model.find();

    return parties;
  }

  /**
   * get a player by his code
   * @param {string} code
   */
  async getPlayers(code) {
    const party = await this.getOneByCode(code);

    return party.players;
  }

  /**
   * Add a Player id to the players array in Party
   * @param {String} partyCode
   * @param {MongooseDoc} player
   */
  async addPlayer(partyCode, player) {
    const filter = { code: partyCode }; // players: { $elemMatch: { $ne: player.pseudo } }
    const options = { new: true };
    const update = {
      $addToSet: { players: player },
    };
    const party = await this.Model.findOneAndUpdate(filter, update, options);

    return party;
  }

  /**
   * this party updtade the enddate of party to the actual time and set up the victory parameter
   * @param {String} partyCode
   * @param {String} victory
   */
  async save(partyCode, victory, turn) {
    const filter = { code: partyCode };
    const update = {
      endDate: new Date(),
      victory,
      turn,
    };
    const options = { new: true };

    const party = await this.Model.findOneAndUpdate(filter, update, options);
    return party;
  }
}

module.exports = { PartyService };
