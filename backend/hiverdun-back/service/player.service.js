class PlayerService {
  constructor(PlayerModel) {
    this.Model = PlayerModel;
  }

  async create(playerPseudo) {
    const regExpPseudo = /^.{3,40}$/;

    if (regExpPseudo.test(playerPseudo)) {
      const player = new this.Model({ pseudo: playerPseudo });

      return player.save();
    }

    return undefined;
  }

  async remove(id) {
    const { n, ok, deletedCount } = await this.Model.deleteOne({ _id: id });

    if (n === 1 && deletedCount === 1) {
      return true;
    }

    return false;
  }

  // eslint-disable-next-line class-methods-use-this
  async update() {
    throw new Error('Not Implemented yet. Get back to work');
  }

  async getOneByPseudo(pseudo) {
    const player = await this.Model.findOne({ pseudo });

    return player;
  }

  async getAll() {
    const players = await this.Model.find();

    return players;
  }

  // eslint-disable-next-line class-methods-use-this
  async addCard() {
    throw new Error('Not Implemented yet. Get back to work');
  }

  // eslint-disable-next-line class-methods-use-this
  async dropCard() {
    throw new Error('Not Implemented yet. Get back to work');
  }

  // eslint-disable-next-line class-methods-use-this
  async addInformation() {
    throw new Error('Not Implemented yet. Get back to work');
  }

  /**
   * save the end of parties for a player
   * @param {String} Playerpseudo
   * @param {Number} PlayerDeathTurn
   * @param {Boolean} victory
   */
  async save(Playerpseudo, PlayerDeathTurn, team, victory) {
    const filter = { pseudo: Playerpseudo };
    const update = {
      deathTurn: PlayerDeathTurn,
      victory,
      team,
    };
    const options = { new: true };

    const player = await this.Model.findOneAndUpdate(filter, update, options);
    return player;
  }
}

module.exports = { PlayerService };
