const { PlayerService } = require('./player.service');
const { PartyService } = require('./party.service');
const { StatisticsService } = require('./statistics.service');

const PartyModel = require('../models/party.model');
const PlayerModel = require('../models/player.model');

const partyService = new PartyService(PartyModel);
const statisticsService = new StatisticsService(PartyModel, PlayerModel);
const playerService = new PlayerService(PlayerModel);

const services = Object.freeze({
  parties: partyService,
  players: playerService,
  statistics: statisticsService,
});

module.exports = services;
