const jwt = require('jsonwebtoken');
const { config } = require('../config/config');

const secret = config.JWT_SECRET;

/**
 * Sign a jwt with a payload
 * @param {Object} obj pass your payload like this { something }
 * @returns {String} a jwt token
 * @throws will throw if payload isn't an object
 */
const sign = payload => {
  return jwt.sign(payload, `${secret}`, { expiresIn: 12 * 60 * 60 });
};

/**
 * verify if the token was issued by us
 * @param {String} token
 * @throws will throw an error if token is invalid
 */
const verify = token => {
  const decoded = jwt.verify(token.replace('Bearer ', ''), `${secret}`);

  return decoded;
};

module.exports = { sign, verify };
