const { playermin, playermax } = require('../io/manager/party.config');
class StatisticsService {
  constructor(PartyModel, PlayerModel) {
    this.PartyModel = PartyModel;
    this.PlayerModel = PlayerModel;
  }

  /**
   * return number of parties played
   */
  async partyCount() {
    return this.PartyModel.estimatedDocumentCount();
  }

  /**
   * return number of players that have play the game
   */
  async playerCount() {
    return this.PlayerModel.estimatedDocumentCount();
  }

  /**
   * return the number of victory (one by player that see victory screen)
   */
  async victoryCount() {
    return await this.PlayerModel.find({ victory: true }).countDocuments();
  }

  /**
   * return number of death at hiverdun
   */
  async deathCountVanilla() {
    const playerCount = await this.playerCount();
    const victoryCount = await this.victoryCount();
    return playerCount - victoryCount;
  }

  /**
   * return number of death at hiverdun if you have already the
   * total number of player
   * @param {Integer} totalPlayerNumber the total of player 
   */
  async deathCount(totalPlayerNumber) {
    return totalPlayerNumber - await this.victoryCount();
  }

  /**
   * return the count of players and parties
   */
  async getAllData() {
    const partyCount = await this.partyCount();
    const playerCount = await this.playerCount();
    //we use this fonction for avoid one call to the db
    const deathCount = await this.deathCount(playerCount);
    return { partyCount, playerCount, deathCount };
  }

  /**
   * return number of parties played and number of cabale's victory for each player size game
   * betwen now and Time (if Time is 0 it will return all time data)
   * @param {Int} Time (ms)
   */
  async getRoomsSizeStatistique(Time) {
    const RoomsSizeStatistique = new Array();
    if (Time == 0) {
      Time = new Date();
    }
    const DateRange = { $gte: new Date(+new Date() - Time), $lte: new Date() };
    // parties between 4 and 18 players
    for (let playerSize = 4; playerSize <= 18; playerSize++) {
      const villageVictory = await this.PartyModel.find({
        players: { $size: playerSize },
        endDate: DateRange,
      }).countDocuments();
      const cabalVictory = await this.PartyModel.find({
        players: { $size: playerSize },
        victory: 'cabal',
        endDate: DateRange,
      }).countDocuments();

      const game = { players: playerSize, parties: villageVictory, cabalVictory };
      RoomsSizeStatistique.push(game);
    }
    return { RoomsSizeStatistique };
  }

  /**
    * this function return all the data you need for count the survavibility
    * we dont do the calculation for cause we don't wanna overload the back
    */
  async getSurvavibilityStatistique(Time) {
    const Statistique = [];
    if (Time == 0) {
      Time = new Date();
    }
    const DateRange = { $gte: new Date(+new Date() - Time), $lte: new Date() };

    for (let playerSize = playermin; playerSize <= playermax; playerSize += 1) {
      // We can also do with an agregation but it take to much memory
      const avgByTeam = await this.PartyModel.find({
        players: { $size: playerSize },
        endDate: DateRange,
      })
        .select('players')
        .populate('players', 'deathTurn team');
      Statistique.push(avgByTeam);
    }
    return Statistique;
  }
}

module.exports = { StatisticsService };
