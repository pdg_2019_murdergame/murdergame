const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const cors = require('cors');
const helmet = require('helmet');

require('dotenv').config();

const { config } = require('./config/config');
const router = require('./routes/routes');

const app = express();

require('./db');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
if (config.NODE_ENV !== 'production') {
  app.use(express.static(path.join(__dirname, 'public')));
}
// Secure some headers with helmet
app.use(helmet());

const corsOptions = {
  origin: '*',
  methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
  preflightContinue: false,
  optionsSuccessStatus: 204,
  // uncomment if needed
  // allowedHeaders: ['Content-Type', 'Authorization'],
  // exposedHeaders: ['Authorization'],
};

// enable CORS
app.use(cors(corsOptions));

app.use('/rest', router);

module.exports = app;
