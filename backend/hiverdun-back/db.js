const mongoose = require('mongoose');
const { config } = require('./config/config');

const uri = config.DB_URI;
const dbUser = config.DB_USER;
const dbPwd = config.DB_PWD;

const urlbegin = config.URL_BEGIN;

const url = `${urlbegin}://${dbUser}:${dbPwd}@${uri}`;

mongoose.connect(url, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
  useCreateIndex: true,
});
console.log("mon url mango est " + url)

const db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => {
  console.log('Connection successful to db');
});

module.exports = mongoose;
