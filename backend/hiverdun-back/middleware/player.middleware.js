const { PartyService } = require('../service/party.service').default;
const { PlayerService } = require('../service/player.service');
const { playermax } = require('../io/manager/party.config');

const PartyModel = require('../models/party.model');
const PlayerModel = require('../models/player.model');

const partyService = new PartyService(PartyModel);
const playerService = new PlayerService(PlayerModel);

const checkUser = (req, res, next) => {
  const partyCode = req.headers.values[0];
  const pseudo = req.headers.values[1];

  const party = partyService.getOneByCode(partyCode);

  // Unable to find the party
  if (party === undefined) {
    res.status(403).send();
    return;
  }

  if (party.players.length >= playermax) {
    res.status(403).send();
    return;
  }

  for (const player in party.players) {
    if (player.name == pseudo) {
      res.status(403).send();
      return;
    }
  }

  const player = playerService.create(pseudo);

  if (player === undefined) {
    res.status(403).send();
  } else {
    partyService.addPlayer(party, player);
    next();
  }
};

module.exports = { checkUser };
