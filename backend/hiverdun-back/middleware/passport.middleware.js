const passport = require('passport');
const http = require('passport-http');

const { BasicStrategy } = http;

const { PartyService } = require('../service/party.service');
const PartyModel = require('../models/party.model');

const partyService = new PartyService(PartyModel);

const checkPassport = passport.use(
  new BasicStrategy((username, partyCode, done) => {
    const party = partyService.getOneByCode(partyCode);

    // Party not found
    if (party === undefined) return done(null, false);

    // Player already in the party
    if (party.players.inludes(username)) return done(null, false);
  }),
);

module.exports = { checkPassport };

// dead code
