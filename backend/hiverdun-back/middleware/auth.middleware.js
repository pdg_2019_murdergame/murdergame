const httpStatus = require('http-status');

const { verify } = require('../service/jwt.service');

/**
 * @param {express.Request & {authorization : string}} req
 * @param {express.Response} res
 * @param {express.NextFunction} next
 */
const checkJWT = (req, res, next) => {
  const token = req.headers.authorization;
  try {
    verify(token);
    next();
  } catch (err) {
    // if the token is invalid
    res.status(httpStatus.FORBIDDEN).send();
  }
};

module.exports = { checkJWT };
