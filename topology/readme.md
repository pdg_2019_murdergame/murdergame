# launch production 



```shell
## Fire up the container:
docker-compose -f docker-compose-prod.yml up -d --build
## Spin up the container:
docker run -it -p 80:80 --rm sample:prod
```

lauch dev 

```shell
#  Fire up the container:
docker-compose up -d --build
# Ensure the app is running in the browser and test hot-reloading again. Bring down the container before moving on:
docker-compose stop
```



sometime on windows you can have volumes problems , follow this link for resolve it:

-  [ Mounting Host Directories](https://rominirani.com/docker-on-windows-mounting-host-directories-d96f3f056a2c)
-  [Shared Drives / Volume Mounting with AD](https://blogs.msdn.microsoft.com/stevelasker/2016/06/14/configuring-docker-for-windows-volumes/)

# -it on windows

sometime you can have this message if you're not in docker bash 

```
the input device is not a TTY.  If you are using mintty, try prefixing the command with 'winpty'

```

use winpty in front of you're command

```shell
winpty docker run -it -p 80:80 --rm sample:prod
```



