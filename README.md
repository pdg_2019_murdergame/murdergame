# Shadows of Hiverdun

## Page du jeu

Pour pouvoir accéder directement à la page notre jeu et pouvoir le tester ou simplement voir quelques statistiques sur les parties jouées c'est par ici !

[shadows of hiverdun](https://pdg_2019_murdergame.gitlab.io/murdergame/)

## Contexte

Cette application a été faite dans le contexte d'un projet de 3ème et dernière année de Bachelor au sein de la *[HEIG-VD](https://heig-vd.ch/)*, pour plus de détails veillez vous référer à [notre rapport](documentation/rapport.pdf)

## Introduction

Cette application reprend le principe du célèbre jeu du **Loups-Garous de Thiercelieux**. Chaque utilisateur se voit attribué un rôle aléatoire et peut effectuer différentes actions pendant les phases de *nuit* à l'aide de cartes distribuée à chaque tour. Les utilisateurs effectuent leurs actions sur un écran personnel tandis que les informations communes (morts, votations publiques, ...) sont affichées sur un écran commun.

Le jeu est jouable en français et en anglais ! Les règles du jeu sont aussi disponibles dans les deux langues.

- [voir les règles en français](documentation/regles_du_jeu_fr.pdf)
- [see the game rules in english](documentation/game_rules_en.pdf)

![Image de connection](documentation/img/screensConnection.jpg)

