# Shadows of Hiverdun game rules

## Main Objectives

**Shadows of Hiverdun** is game of the type "Werewolf/Mafia". The game is played in a group of 8 to 18 people divided between **The Cabal** and the **Citizen**.

**The Cabal** conspire secretly to eliminate the **Citizen** : **the night**, they choose a **Citizen** to be sacrificed.

The **Citizen** try to find out the identity of the evildoers: **the day**, all players debate and choose together someone that they think should be executed using what they discover while playing.

The interest of the game lies in the ability of **The Cabal** to lie and make the **Citizen** doubtful.

## Launch the game

To start a game you will need a **Master** (Game Master), but in contrary of the **Werewolf** game you won't need a player to assume this role.

The game is played with a **main screen** that will be used as the **Game Master** and be common to every player (typically a computer screen, a video projector, a TV screen, ...), and **personal screens** (typically a smartphone) for the players.

A game is created on the **main screen** when clicking on the **create** button and the players can then join the game by pressing on the **join** button by entering the **party code** (on the **main screen**) with a **player name** (unique in the party).

## Game flow

The game takes place in cycles of **3 phases**: **The night** falls, followed by the **dawn** and finally the **day**. The cycle repeat itself and the **night** falls again.

During the **night**, every player receives cards on his **personal screen** that gives them some possible game actions. This phase is played in **2 parts**, **a first one** to **take** or **reject** a card and a **second** to eventually **play** a card.

It is also at this time that **The Cabal** votes to eliminate a player. Someone is eliminated when the majority of the votes are against him, if there is an equality or no votes the game continues without anyone dying.

At **dawn**, a small set of cards can be played if a player has one of them.

And finally, **the day**: The player discover if one or more persons died during the **night**. The players must now debate together and take action against the killing by voting against someone they think guilty. If there is and equality or no votes, no one is executed. This information are displayed on the **main screen**.

The game ends when every member of a team is eliminated.

## The Cards

### Cabal Cards

- **Sacrifice**

  ![sacrifice](img/game/cards/en/sacrifice-en.png)

  The sacrifice card is the main card of the cabal, it makes them possible to vote against a player at night when activated.

- **Heir**

  ![heritier](img/game/cards/en/heir-en.png)

  The heir card makes it possible at it's activation to target a citizen that will become a cabal member at the death of the card holder.

### Citizen Cards

- **Broken Card**

  ![carte-cassee](img/game/cards/en/broken-en.png)

  The "broken" card is a card that has no effects. Playing it will just consume it without doing anything.

- **Ankh**

  ![ankh](img/game/cards/en/ankh-en.png)

  The ankh card is playable at dawn. Playing it will permit the card holder to save a player that died during the night. If there is no one to save it can't be activated.

- **Burgle**

  ![larcin](img/game/cards/en/burgle-en.png)

  The burgle card is playable at night and permits to steal a card from a chosen player. But take care ! Stealing the cabal will result in a death of the card owner.

- **Death Poison**

  ![poison-mortel](img/game/cards/en/death_poison-en.png)

  The death poison card is played at night and makes it possible to kill someone.

- **Hard Skin**

  ![peau-dure](img/game/cards/en/hard_skin-en.png)

  The hard skin card is a passive card that can't be played but has as effect to protect the owner from an attack of the cabal. If the card holder is targeted, the card is used and consumed.

- **Hint Fragment**

  ![partie-indice](img/game/cards/en/hint-en.png)

  The hint fragment card can be played during the night enables the player to target someone to see his team. But to played it, it need to be hold in pair. Two of them are consumed when used.

- **Prayer**

  ![priere](img/game/cards/en/prayer-en.png)

  The prayer card is used at night and is used to protect another player during the current night. If protected the player target by the cabal won't die.

- **Prophecy**

  ![prophetie](img/game/cards/en/prophecy-en.png)

  The prophecy card is playable during the night and allows the player to see the team of someone he chooses. It can be used every night.