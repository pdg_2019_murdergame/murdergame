# PDG - PV 24.09.2019

## Définition du scénario du MVP

- 2 personnes ne peuvent pas avoir le même pseudo
  - Le premier utilisateur connecté est prioritaire
- Jeu avec carte
  - Plusieurs carte max (3 ?). Pendant la nuit, on tire une carte. Ensuite, on a le choix de jouer une carte, ne rien faire ou en défausser une. Si le joueur a plus de carte que le maximum, il doit soit en défausser une, soit en jouer une
  - Carte maudite
    - On doit se faire tuer par les loups garou, lors de la mort, on devient loup garou. On ne peut plus gagner avec les villageois

## Déroulement

*Voir le plan en photo*

1) Ouverture de l'application
2) Choisir si on veut rejoindre une partie ou en créer une
3) Une fois que tous les joueurs ont rejoint, l'histoire s'affiche sur l'écran commun
4) Les joueurs voient leur rôle
5) La nuit se lance

La nuit dure un certain temps (1 min) se déroule comme suit pour chacun des rôles :

- **Loups garous :** Ils voient une listes avec tous les joueurs, quand un loup choisi une victime potentiel, les autres voient son choix de vote et un pourcentage et affiché. Les loups peuvent changer leur vote pendant la nuit. Une fois que tous les loups sont d'accord sur la victime, elle est tuée. Cependant, si à la fin de la nuit et si les loups ne se sont pas mis d'accord, la victime ayant le plus gros pourcentage est tuée. Un départage aléatoire sera fait en cas d'égalité.
- **Villageois :** Une carte est distribuée au villageois. Le joueur a le choix entre jouer une carte, de ne rien faire ou de défausser une carte. Si le joueur a plus de carte que le maximum, il doit obligatoirement jouer une carte ou en défausser une.

Une fois que la nuit se termine, la **sorcière** peut choisir de ressusciter ou non une des victimes de la nuit avec sa potion de vie. Les potions de vie ou de mort ne sont utilisable qu'une seule fois.

6) Le jour se lance
7) La votations est faite de même manière que celle des loups garous.
8) Le rôle de la victime de la votation est affichée sur l'écran commun.
9) Les étapes 5 à 8 sont répétées jusqu'à ce qu'un vainqueur est désigné.

## Élément à implémenter optionnel

- Création de compte pour montrer le nombre de partie que le personnage a survécu
- Montrer les cartes perdues (rôles et carte en main)