# Liste d'état

## Transition (avec l'image Telegram de Christoph)

### Night 1 to Dawn 1

(Timer min (1 min) && vote des adeptes unanime) || Timer max (3 min)

Si les adeptes ne sont pas parvenu à une unanimité, la personne ayant eu la majorité se fait attaquer.

### Dawn 1 to Day 1

La sorcière peut choisir de sauver la personne ou non (ou autre carte qui se joue après l'attaque des adaptes).

### Day 1 to Night

Le maire a été élu

Tout le monde a voté, la personne avec le plus de vote se fait exécuter. Les villageois ont la possibilité de voté blanc.

### Night to Dawn

(Timer min (1 min) && vote des adeptes unanime) || Timer max (3 min)

Si les adeptes ne sont pas parvenu à une unanimité, la personne ayant eu la majorité se fait attaquer.

### Dawn to Day

La sorcière peut choisir de sauver la personne ou non (ou autre carte qui se joue après l'attaque des adaptes).

### Day to Night

Tout le monde a voté, la personne avec le plus de vote se fait exécuter. Les villageois ont la possibilité de voté blanc.

### Day to End

Une des conditions de victoire a été remplie :

- Tout les loups ont été tués
- Tout les citoyens ont été tués