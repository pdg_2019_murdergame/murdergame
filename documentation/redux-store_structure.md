# Redux Store

## Store Main States
 - Server State
 - Connection State
 - Game State
 - Player State
 - Master State
 - ...other frontend-only related states

## Store Structure

```javascript
/**
 * Simple state to store if the API server is online
 */
const serverState = {
  loading : false,  // (bool) true while the update is beeing fetched
  alive : false,    // (bool) true if the server is online
  error : ''        // (string) error message from the server
}

/**
 * State to keep connection related infos
 * and the role of the user (player/master)
 */
const connectionState = {
    loading: false,     // (bool) true while the connection is beeing set-up
    socket: null,       // (socketio) websocket connected to the party instance on the API server
    serverMessage: '',  // (string) connection related messages send from the server (connections/disconnections)
    token: '',          // (string) JWT token that allows a client to get a socket connection
    role: '',           // (string) connected as 'player' or 'master'
    error: ''           // (string) error while fetching the token or connecting to a party
}

/**
 * State to store the overall game/party informations
 */
const gameState = {
    partyCode: '',        // (string) code of the party
    numberOfPlayers: 0,   // (int) number of players in the party
    state: 'LobbyState',  // (string) state of the game (LobbyState, StoryState, ... , EndState)
    players: [],          // (Array[player]) array of players with infos
    cabalVotes: [],       // (Array[vote]) array of the cabal votes
    lastDeads: [],        // (Array[player]) array of players dead durring last state
    winner: ''				    // (string) name of the winning team (or 'equality')
}

/**
 * State to store the player informations when connected as player
 */
const playerState = {
  nickname: '',   // (string) players nickname
  cards: [],      // (Array[Card]) player cards (models.Card)
  cardslot: null, // (Card) special card slot to receive new cards and reject them (models.Card)
  team: '',       // (string) player team 'citizen' or 'cabal'
  alive: true,    // (bool) true if the player is alive
}

/**
 * State to sotre master informations when connected as master
 */
const masterState = {
  votes: [],   // (Array[vote]) list of 'vote' used to display vote state on master
  guilty: ''   // (string) currently most voted player (empty on equalities)
}

/**
 * Structure of a player with infos in 'players'
 */
const player = {
  nickname,   // (string) player's name
  alive,      // (bool) true if the player is alive
  online,     // (bool) true if the player is online (connected on the socket)
  team        // (string) the team of the player 'citizen', 'cabal' or 'suspect'
}

/**
 * Structure of a vote
 */
const vote = {
  player,   // (string) vote emiter's nickname
  target    // (string) targeted player's nickname
}
```
