# React

## prerequisites

- Nodejs installed

## Create and start React project

with `npx`

```code
npx create-react-app <project_name>
cd <project_name>
npm start
```

or `npm`

```code
npm install -g create-react-app <project_name>
```

The page will be available on [localhost:3000](http://localhost:3000)

## Tutorials

[ReactJS Tutorial by Vishwas](https://www.youtube.com/watch?v=QFaFIcGhPoM&list=PLC3y8-rFHvwgg3vaYJgHGnModB54rxOk3)

