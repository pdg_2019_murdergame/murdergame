# liens 



- [GitLab CI/CD Pipeline Configuration Reference](https://gitlab.com/help/ci/yaml/README)
- [repo utiliser pour faire les tests](https://gitlab.com/alexandre.gabrielli/test-integration)

## work only with path in racine

```yml
image: node:latest

cache:
  paths:
  - node_modules/

test:
  script:
   - npm install
   - npm run test

```

les submodules semblent poser problème nécessite plus d'investigation.

-> solution trouver commencer les script par cd subdossier

## integration multiple 

se projet permet de voir ma marche a suivre sur le push b665fab280ff7f886ef5b9f16a1768b095e08dd1, ici on a fait run les tests et ensuite nous avons deploy sur la page (de gitlab), les stages sont réaliser les uns après les autres. 

```yml
stages:
  - test
  - deploy


image: node:latest
cache:
  paths:
  - ./front/node_modules/
before_script:
  - git submodule sync --recursive
  - git submodule update --init --recursive
test:
  script:
   - cd front
   - npm install
   - npm run test


pages:
  stage: deploy
  script:
  - mkdir .public
  - cp -r * .public
  - mv .public public
  artifacts:
    paths:
    - public
  only:
  - master

```







![test+deploy](.\test+deploy.PNG)

https://gitlab.com/alexandre.gabrielli/test-integration/pipelines/88005524 



