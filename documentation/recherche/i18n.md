# I18n in React

## Node modules

- `i18next`
  - i18n for node applications
- `react-i18next`  
  - i18n for React
- `i18next-xhr-backend`
  - load translations
- `i18next-browser-languagedetector`
  - detect user language

## Github example on i18next repository

[i18next react example](https://github.com/i18next/react-i18next/tree/master/example/react)

## Folder structure

- `/src`
  - `i18n.js`
- `/public`
  - `/locales`
    - `/en`
      - `translation.json`
    - `/fr`
      - `translation.json`