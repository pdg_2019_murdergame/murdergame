# heroku

## environement variable

for set up the environnement variables first you need install heroku CLI, you can use npm if you have node and npm install. 

```bash
npm install -g heroku
```

after you can log with your're browser with the commande

```bash
heroku login
```

or in command line 

```bash
heroku login -i
```

You can set environnement variable if you have access to the project with.

```bash
heroku config:set GITHUB_USERNAME=joesmith -a yourappname
```

and erase with 

```bash
heroku config:unset GITHUB_USERNAME
```



we know that isn't a good method but we cannot do better with heroku.