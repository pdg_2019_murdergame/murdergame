# Vision du Projet

# Idée de base du projet

Faire un jeu basé sur le "loup-garou" où chaque joueur utiliserait son téléphone mobile (ou autre appareil : pc, ...) pour jouer. Un autre appareil (pc, tablette, mobile, etc...) serait utilisé pour donner les informations visibles par tous et faire office de maître du jeu.

# Extensions du projet

Une fois la base mise en place, le but serait d'étendre les fonctionnalités de notre application en ajoutant plus d'éléments à notre jeu pour le rendre plus intéressant et profité du support informatique pour en faire quelque chose qui ne serait pas possible d'avoir dans le jeu original.

Pour pouvoir être le plus accessible possible nous avons choisi de partir sur des applications web mais nous souhaiterions pouvoir faire une application mobile native si nous avons le temps afin de pouvoir utiliser des fonctionnalités comme la caméra (rejoindre une partie avec un QR code).

# Ma vision sur le projet

Je suis content de notre idée de projet car notre but est de faire quelque chose qui pourra être utilisé de manière pratique et interactive, et non pas un projet à but seulement éducatif.

Nous nous somme mis d'accord de faire notre(nos) application(s) en utilisant des technologies web qui m'intéressent beaucoup et que je voudrais apprendre. (reactjs, react-native, websocket, etc...)
