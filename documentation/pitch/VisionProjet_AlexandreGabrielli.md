# Vision du projet 

## Idée général

Le but est de faire un loup-garou like comme sur [le site officiel](https://www.loups-garous-en-ligne.com/) mais en utilisant les téléphones portable des différents joueurs. 

## support utilisé

- écran commun avec plateau et information commune
- chaque joueur ce connecte et joue sur son smartphone

j'aime beaucoup le fait qu'il y ai un écran partagée et que chaque joueurs jouent sur son propre écran. Car cela permet de gardé la convivialité du jeu de société (les gens sont obligés de se réunir dans la même salle) et aussi apprécie la mécanique d'avoir des infos partagé et des infos individuelle pour ce jeu.

Problème possible => batterie du smartphone mal adapté à plusieurs heures de jeu.

## application smatphone

très bonne idée permet d'utilisé directement le QR code, mais le problème est qu'on ne peut pas être sur que l'apple store accepte notre application et donc nous devons en 1er faire une application web. L'application mobile sera une feature. 

## QR code

très bonne idée, dans une étape secondaire du projet au début juste faire un code sera suffisant, le QR code doit être complémentaire au code car un ordi ne lira pas le QR, voir un smartphone avec caméra cassé. 



Auteur: Gabrielli Alexandre