# Vision du projet

Auteur : David Jaquet

## Projet

Nous avons eu l'idée de créer une application reprenant le concept du loup-garou. L'application sera utilisée sur un navigateur web et, dans un second temps, l'application sera aussi disponible en application mobile.

## Mes attentes

Ce projet me motive car je connais peu les technologies que nous avons choisies (`React`, `Express`, `websocket`, ...). Je suis habituellement assigné à la partie `UI` du projet. Cependant, pour ce projet, je m'occupe de la partie du `backend`.

Je suis impatient d'apprendre de nouvelles technologies, de travailler sur une autre partie d'un projet (ici le `backend`) et, surtout, de le faire dans un cadre d'un jeu de plateau (qui, pour moi, est ludique et attrayant).