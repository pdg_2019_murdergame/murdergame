# Vision du Projet

Auteur: Tiago P. 

## Idée de base

Créer un jeu sur la base du "Loup garou de Thiercelieux". La distribution des rôles ainsi que tous les autres aspects du jeu normalement gérés par un Maître de jeu seraient ainsi facilités. Notre difficulté sera d'intégrer des mécanismes supplémentaires intéressants qui mettront à profit les plateformes mobiles / informatiques au lieu du support traditionnel. 

## Support

* Écran moyen à grand utilisé comme plateau (tablette, portable, tv ?)

* Écran personnel comme "Carte de rôle" et tous les autres aspects de gameplay individuels (smartphone de préférence)

## Vision

De mon point de vue, les technologies discutés et l'envergure du projet ne sont pas trop expérimentales. Et pour cette raison je pense que c'est l'opportunité de faire quelque chose de propre, déployable et utilisable. Aussi, une fois la base du projet mis en place, que j'estime assez rapide, on aura l'opportunité d'expérimenter des idées de game design. Le tout sans s'embêter avec des difficultés techniques et des inconnus trop grands comme ça aurait été le cas avec un projet unity. 

