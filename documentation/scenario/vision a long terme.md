# Vision à long terme

Dans une version future nous imaginons les implémentations suivantes.

- Thématisation : plusieurs histoire avec des thèmes différents pourraient être sélectionner au lancement de la partie (mafia , loup-garou, ...)
- multi-village : en imaginant pouvoir joueur en grand nombre il serait possible implémenter une partie avec plusieurs villages avec des interactions entre eux.
- faire une application native pour mobile
- ajout de carte
- ajout de rôle
- engager un graphiste
- Indicateur du nombre de personnes mortes et de parties jouées sur la landing page
- possibilité de s'inscrire et de voir les stats des différentes parties que l'on a joueur voir généré des personnages
- trouver un nom 

