# Minimal Value Project

## Objectif 
Nous désirons une version informatique du [loup garoux](http://jeuxstrategie.free.fr/Loups_garous_complet.php) tout en gardant l'idée que les gens se réunissent physiquement. Chaque joueurs ont une interface individuelle qui sont les seul a pouvoir voir. Une interface commune permet d'afficher les messages partagés et permet de garder une interaction entre les utilisateurs. 

Les noms propres au jeu du loup garou seront adaptés en fonction de notre thème et des règles issues de notre conception. Par soucis de simplicité, on notera d'une astérisque ce qui peut changer. 

## Règles
Dans notre MVP nous désirerons créer un jeu basé sur le [loup garoux](http://jeuxstrategie.free.fr/Loups_garous_complet.php) dont les règles que nous simplifions un peu sont les suivantes:
- une partie est composé de 4 à 18.
- lors d'une partie un joueur incarne soit un adepte soit un citoyen. 
- les personnages, leurs actions et leurs objectifs sont inspirés sur le [jeu officiel](http://jeuxstrategie.free.fr/Loups_garous_complet.php) 
- le nombre d'adeptes par partie suit le nombre de loups [des régles officiels](http://jeuxstrategie.free.fr/Loups_garous_complet.php) , en dessous de 8 joueurs nous ne mettrons qu'un seul adepte (cela pour pouvoir tester facilement le jeu)
- lors de la phase de nuit les citoyens piochent une carte, un joueur peut avoir au maximum 3* cartes sur eux, choisir de jouer directement ou non leur carte, rejeter la carte (lorsqu'il on déjà deux cartes), certains carte spécial ne peuvent pas être rejeter ni défausser.
- Au levée du soleil des évènements peuvent apparaitre

## Rôle

- Adepte : se réunisse chaque nuits et vote pour la personne qu'ils utiliseront pour leurs cérémonies (au cours de laquelle cette personne mourra).  
- Citoyen : tire une carte durant la nuit 
	- Sorcière* : reçoit deux carte de base potion de vie, potion de mort.
	- Voyante* : reçoit la carte voir une carte qui revient automatiquement dans son deck.
	- voleur : reçoit une carte vol qui ne se défausse pas, il ne reçoit pas d'autre carte.
	- idiot du village: lorsqu'il est lynché par le village, celui ci survit mais ne peu plus voter 
	- cupidon* : en début de partie choisi deux personnes qui tombent amoureux, leurs destin et lier si l'un meurt l'autre meurt, si un et loup et l'autre non leurs but est d'être les deux dernier survivants.  

## Carte

### active

- potion de mort : peut tuer quelqu'un durant la nuit
- vol : peut voler une carte, quand il vole un loup il reçoit une carte aléatoire
### passive

- potion de vie : permet de ressusciter une personne pendant la nuit cette carte peu se jouer juste après le tour des loups.
- maladie : empêche de voter, doit la refiler la carte a quelqu'un pour le jour suivant, ne peut pas être défausser.

### spécial 

- maudite : si cette carte est piochée dans le deck, le joueur ne peut pas la défausser et est obligé de la garder. L'objectif du joueur change il doit se faire mordre par un loup afin d'être transformée en loup et ensuite gagné avec eux. 

## lancement d'une partie
Nous voudrions que la page web d'accueil propose deux boutons, rejoindre une partie et créer une partie. 
- le bouton créer une partie génère un code unique qui s'affiche avec le lobby de la partie, l'utilisateur peut ainsi donner le code aux personnes avec qui il désire jouer
-  Le bouton rejoindre une partie permet de rentrer un code unique et un pseudo, lorsqu'on entre le code on peut rejoindre le lobby de la partie, la personne qui a créé la partie peut sur un autre appareil rejoindre la partie et donc aussi être joueur.
- lorsqu'il le désire l'utilisateur qui a créer la partie peu lancer la partie (via un bouton) et commence par une phase de nuit

## Partie lancée, écran commun
### jour 
Sur l'écran commun le cycle jour nuit s'affiche. Durant la journée si une personne est morte durant la nuit sont nom est affiché, et lors de phase de vote un histogramme en temps réel s'affiche.
### nuit 
Durant la phase de nuit un thème ou du texte signifie qu'on est la nuit.

## partie lancée, écran individuelle
### jour 
le rôle est clairement identifiable ainsi qu'un texte expliquant son objectif et ses éventuelles pouvoir.
lors de la phase de vote, utilisateur peu sélectionner la personne pour qui il vote dans une liste des pseudos des joueurs 

### nuit
les personnes ayant des actions peuvent sélectionner via leurs interface les pseudos des gens sur lequel il désire effectuer son action.
afin d'éviter que les loups soit trop flagrant (seul a jouer la nuit) des questions de feedback sont poser au villageois. 

## fin de partie
### victoire des adeptes
sur l'écran commun le message "Le culte a triomphé, la nouvelle ère peut commencer " apparait et victoire sur l'écran individuelle de chaque loups.
### victoire des citoyens 
Sur l'écran commun le message "ouf le culte a été rayé de notre village " apparait et victoire s'affiche sur l'écran individuelle de chaque villageois. 

## Thème 

### texte de début de partie

Dunwich, Nouvelle-Angleterre

Une méfiance générale règne. Des regards discrets suivis de rideaux tirés. Des individus conspirent dans l'ombre, servant des dessins mystérieux.

Pour sauver la ville, il vous faudra mettre ses actes en lumière, mais saurez-vous les démasquer?

## statistique 

nous implémenterons un moyen de récupérer des statistiques, % de victoire des loup , rôle en fonction du nombre de joueur. nous nous réservons le droit de supprimer, modifier un rôle ou une carte de ce MVP en fonction des statistiques récolté.