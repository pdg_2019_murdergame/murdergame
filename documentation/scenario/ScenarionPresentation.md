# Scénario de présentation final

## Démo

On commence par présenter le concept du jeu, les rôles disponibles, les cartes. Puis, nous afficherons l'écran commun sur le projecteur et demanderons à l'assistance de rejoindre la partie créé. Nous afficherons également un écran personnel afin que les spectateurs ne voulant pas se connecter puissent voir toutes les fonctionnalités de notre application.

Nous commencerons par lancé une partie normal pour montrer le fonctionnement à tout le monde tout en montrant l'écran personnel. Le but ici n'est pas de montrer une vraie partie, mais de faire connaître une parties des fonctionnalités implémentées.

Après un certains nombres de tours (pour correspondre aux 10 minutes allouées), nous allons simuler un arrêt de partie afin de pouvoir montrer l'écran de victoire.

## Présentation du front-end

Une fois la démo terminée, le responsable front-end présente les technologies utilisées et motive le choix de ces technologies.

Une fois cette partie de présentation terminée, nous aurons déjà effectué 10 minutes de présentation

## Explication de l'intégration continue

Lorsque nous avons terminé de présenter le front-end, la personne en charge du back-end présente le pipeline de l'intégration contenu et comment il est utilisé pour l'intégration des versions sur un cloud.

Cette partie doit durer 10 minutes, ce qui fait que nous aurons 20 minutes de présentation au total.