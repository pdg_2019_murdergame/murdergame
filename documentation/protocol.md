# Messages émis en socket.io



| impl | sens | Message         | Qui    | Quoi                                 |
| ---- | ---- | --------------- | ------ | ------------------------------------ |
| *    | ->   | roomenter       | client | partycode                            |
| *    | ->   | roomjoin        | All    | player nickname who joined           |
| *    | ->   | roomleave       | All    | player nickname who left             |
| *    | ->   | gamesize        | All    | number of players                    |
|      | ->   | gamestate       | All    | state of the game                    |
| *    | <-   | gamestart       | srv    | start the game                       |
| *    | <-   | skip            | srv    |                                      |
| *    | ->   | ~~gamecontext~~ | All    | state, playerlist with online status |
|      | ->   | gamestate       | All    | state                                |
|      | ->   | playerlist      | All    | { nickname, alive }                  |
|      | <-   | playercards     | client |                                      |
| *    | <-   | playcard        | srv    | { card, target }                     |
| *    | <-   | vote            | srv    | your vote choice                     |
| *    | ->   | votelist        | All    | nickname to vote against             |
|      | ->   | votecabal       | Cabal  | nickname to vote against             |

Vote -> tous les votes d'un coup 