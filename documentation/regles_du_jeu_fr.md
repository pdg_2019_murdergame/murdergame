# Règle de Shadows of Hiverdun

## Prince du jeu

**Shadows of Hiverdun** est un jeu de type “Loup-garou / Mafia”. Le jeu est joué avec un groupe de 8 à 18 joueurs répartis entre **La cabale** et les **Citoyens**.</br>
**Les Cabalistes** conspirent secrètement pour éliminer les **Citoyens**: **la nuit**, ils choisissent ensemble un **Citoyen** à éliminer.<br>
Les **Citoyens** tentent de découvrir l’identité des malfaiteurs: **le jour**, tous les joueurs débattent et choisissent un coupable à éliminer en fonction des indices découverts. 

L’intérêt du jeu réside dans la capacité de **La Cabale** à mentir et créer le trouble parmi les **Citoyens**.

## Lancer une partie

Pour faire une partie il est nécessaire d’avoir un **Master** (Maître du jeu), mais contrairement à une partie de **Loup-Garou** il n’y a pas besoin de quelqu’un pour jouer ce rôle.

Le jeu se joue avec un **écran principal** qui va faire office de **Maître du jeu** et être commun (typiquement un écran d’ordinateur, un projecteur, une télévision, ...), et des **écrans personnels** (typiquement des smartphones) pour chacun des joueurs.

Une partie est créée sur **l’écran principal** en appuyant sur le bouton **créer** et les joueurs peuvent ensuite rejoindre cette partie en appuyant sur le bouton **rejoindre** en entrant le **code de partie** (sur **l’écran principal**) avec un **pseudonyme** (unique à la partie).

## Déroulement

Le jeu se déroule en cycles de **3 phases** : **La nuit** tombe, suit **l’aube** et finalement **le jour** se lève. Le cycle recommence à la tombée de **la nuit**. 

Durant **la nuit**, tous les joueurs reçoivent des cartes sur leur écran personnel qui leur donne des possibilités de jeu. Cette phase se fait en **2 parties**, **une première** pour **prendre** ou **rejeter** une carte et **une deuxième** pour éventuellement **jouer** une carte.</br>
C’est aussi à ce moment là que **la Cabale** vote pour éliminer un joueur. Le vote se fait à la majorité, s’il n’y a pas de votes ou une égalité personne n’est éliminé.

À **l’aube**, certains effets de carte peuvent encore se déclencher par un les joueurs qui possèdent des cartes jouables à ce moment.

Et finalement, **le jour**: On découvre si un ou plusieurs joueurs ont péri pendant **la nuit**. La suspicion est de mise. Les joueurs doivent alors débattre ensemble sur qui ils pensent responsable et voter sur la cible à éliminer. Dans le cas où il n’y a pas de votes ou une égalité, personne n’est exécuté. Ces informations seront présentés sur **l’écran principal**.

La partie se termine quand tous les membres d’une équipe est éliminée.

## Les Cartes

### Carte Cabale

- **Sacrifice**
  
  ![sacrifice](img/game/cards/fr/sacrifice-fr.png)
  
  La carte sacrifice est la carte principale de la cabale, elle leur permet de voter pour éliminer un joueur pendant la nuit à son activation.

- **Héritier**

  ![heritier](img/game/cards/fr/heir-fr.png)

  La carte Héritier permet à son activation de choisir un villageois qui va devenir un Cabale à la mort du joueur de cette carte.

### Carte Citoyen

- **Carte Cassée**

  ![carte-cassee](img/game/cards/fr/broken-fr.png)

  la carte "Cassée" est, comme son nom le suppose, une carte qui n'a plus/pas d'effet. L'utiliser n'a aucun effet. Elle est jouable pendant la nuit.

- **Ankh**

  ![ankh](img/game/cards/fr/ankh-fr.png)

  La carte Ankh est jouable pendant l'aube, la jouer permet de choisir un joueur qui est mort pendant la nuit et le sauver. S'il n'y a personne à sauver la carte ne se joue pas à l'activation.

- **Larcin**

  ![larcin](img/game/cards/fr/burgle-fr.png)

  La carte Larcin est jouable de nuit, elle permet à l'activation de choisir un autre joueur et lui voler une carte au hazard. Mais attention ! Si le joueur choisi est un membre de la Cabale, cela tue le joueur qui a activer la carte.

- **Poison Mortel**

  ![poison-mortel](img/game/cards/fr/death_poison-fr.png)

  La carte poison mortel, jouable la nuit permet, à l'activation, de choisir un joueur qu'on veut éliminer.

- **Peau dure**

  ![peau-dure](img/game/cards/fr/hard_skin-fr.png)

  La carte peau dure est une carte passive qui ne peut pas être jouée, elle permet de survivre à un sacrifice de la Cabale. À son utilisation, elle est consumée.

- **Partie d'Indice**

  ![partie-indice](img/game/cards/fr/hint-fr.png)

  La carte "Partie d'Indice" est une carte qui se joue pendant la nuit et permet de choisir un joueur pour savoir son rôle. Mais pour pouvoir l'utiliser, il faut être en possession de 2 d'entre elles. Deux de ces cartes sont consumée à l'utilisation.

- **Prière**

  ![priere](img/game/cards/fr/prayer-fr.png)

  La carte prière, utilisable pendant la nuit, permet de choisir un joueur (autre que soi) pour le protéger pendant la nuit en cours. S'il est attaqué par la Cabale, il ne se fera pas tuer.
  
- **Prophétie**

  ![prophetie](img/game/cards/fr/prophecy-fr.png)

  La carte prophétie est jouable la nuit et permet de voir le rôle d'un autre joueur. Cette carte peut être utilisée autant de fois qu'on veut, jusqu'à la mort du joueur qui la détient.