import playerStateReducer from '../../../src/redux/playerState/playerStateReducer'
import * as types from '../../../src/redux/playerState/playerStateTypes'

describe('playerState REDUCER', () => {

  const initialState = {
    nickname: '',
    cards: [],
    cardslot: null,
    team: '',
    alive: true
  }

  it('should return the initial state', () => {
    expect(playerStateReducer(undefined, {})).toEqual(initialState)
  })

  it('should handle SET_NICKNAME', () => {
    const nickname = 'nickname'
    expect(playerStateReducer(initialState, {
      type: types.SET_NICKNAME,
      nickname
    })).toEqual({
      nickname: nickname,
      cards: [],
      cardslot: null,
      team: '',
      alive: true
    })
  })

  it('should handle UPDATE_CARDS', () => {
    const cards = ['card1', 'card2']
    expect(playerStateReducer(initialState, {
      type: types.UPDATE_CARDS,
      cards
    }))
    expect({
      nickname: '',
      cards: cards,
      cardslot: null,
      team: '',
      alive: true
      })
  })

  it('should handle SET_TEAM', () => {
    const team = 'team'
    expect(playerStateReducer(initialState, {
      type: types.SET_TEAM,
      team
    })).toEqual({
      nickname: '',
      cards: [],
      cardslot: null,
      team: team,
      alive: true
    })
  })

  it('should handle KILL_PLAYER', () => {
    expect(playerStateReducer(initialState, {
      type: types.KILL_PLAYER
    })).toEqual({
      nickname: '',
      cards: [],
      cardslot: null,
      team: '',
      alive: false
    })
  })

  it('should handle RESURECT_PLAYER', () => {
    expect(playerStateReducer({
      nickname: '',
      cards: [],
      cardslot: null,
      team: '',
      alive: false
    }, {
      type: types.RESURECT_PLAYER
    })).toEqual(initialState)
  })

  it('should handle CLEAR_PLAYER_STATE', () => {
    expect(playerStateReducer({
      nickname: '',
      cards: ['card'],
      cardslot: 'card',
      team: 'team',
      alive: false
    }, {
      type: types.CLEAR_PLAYER_STATE
    })).toEqual(initialState)
  })

  it('should handle SET_CARD_SLOT', () => {
    const card = 'card'
    expect(playerStateReducer(initialState, {
      type: types.SET_CARD_SLOT,
      card
    })).toEqual({
      nickname: '',
      cards: [],
      cardslot: card,
      team: '',
      alive: true
    })
  })
})