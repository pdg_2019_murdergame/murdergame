import * as actions from '../../../src/redux/playerState/playerStateAction'
import * as types from '../../../src/redux/playerState/playerStateTypes'

describe('playerState ACTION', () => {

  it('should create an action on nickname set', () => {
    const nickname = 'nickname'
    const expectedAction = {
      type: types.SET_NICKNAME,
      nickname
    }
    expect(actions.setNickname(nickname)).toEqual(expectedAction)
  })

  it('should create an action on card update', () => {
    const cards = ['card1', 'card2']
    const expectedAction = {
      type: types.UPDATE_CARDS,
      cards
    }
    expect(actions.updateCards(cards)).toEqual(expectedAction)
  })

  it('should create an action on team set', () => {
    const team = 'team'
    const expectedAction = {
      type: types.SET_TEAM,
      team
    }
    expect(actions.setTeam(team)).toEqual(expectedAction)
  })

  it('should create an action on player kill', () => {
    const expectedAction = {
      type: types.KILL_PLAYER
    }
    expect(actions.killPlayer()).toEqual(expectedAction)
  })

  it('should create an action on player resurect', () => {
    const expectedAction = {
      type: types.RESURECT_PLAYER
    }
    expect(actions.resurectPlayer()).toEqual(expectedAction)
  })

  it('should create an action on player state clear', () => {
    const expectedAction = {
      type: types.CLEAR_PLAYER_STATE
    }
    expect(actions.clearPlayerState()).toEqual(expectedAction)
  })

  it('should create an action on card slot set', () => {
    const card = 'slot'
    const expectedAction = {
      type: types.SET_CARD_SLOT,
      card
    }
    expect(actions.setCardslot(card)).toEqual(expectedAction)
  })
})