import * as actions from '../../../src/redux/connectionState/connectionStateAction'
import * as types from '../../../src/redux/connectionState/connectionStateTypes'

describe('connectionState ACTION', () => {

    it('should create an action to fetch a jwt token', () => {
        const expectedAction = {
            type: types.FETCH_TOKEN_REQUEST
        }
        expect(actions.fetchTokenRequest()).toEqual(expectedAction)
    })

    it('should create an action on jwt token fetch success', () => {
        const token = 'some token'
        const expectedAction = {
            type: types.FETCH_TOKEN_SUCCESS,
            token
        }
        expect(actions.fetchTokenSuccess(token)).toEqual(expectedAction)
    })

    it('should create an action on jwt token fetch failure', () => {
        const err = 'some error'
        const expectedAction = {
            type: types.FETCH_TOKEN_FAILURE,
            error: err
        }
        expect(actions.fetchTokenFailure(err)).toEqual(expectedAction)
    })

    it('should create an action on socket connection request', () => {
        const expectedAction = {
            type: types.CONNECT_SOCKET_REQUEST
        }
        expect(actions.connectSocketRequest()).toEqual(expectedAction)
    })

    it('should create an action on socket connection success', () => {
        const socket = 'socket object'
        const role = 'cabal'
        const expectedAction = {
            type: types.CONNECT_SOCKET_SUCCESS,
            socket,
            role
        }
        expect(actions.connectSocketSuccess(socket, role)).toEqual(expectedAction)
    })

    it('should create an action on socket connection failure', () => {
        const error = 'some error'
        const expectedAction = {
            type: types.CONNECT_SOCKET_FAILURE,
            error
        }
        expect(actions.connectSocketFailure(error)).toEqual(expectedAction)
    })

    it('should create an action on socket disconnection', () => {
        const expectedAction = {
            type: types.DISCONNECT_SOCKET
        }
        expect(actions.disconnectSocket()).toEqual(expectedAction)
    })

    it('should create an action on socket message', () => {
        const serverMessage = 'some message'
        const expectedAction = {
            type: types.CONNECT_SOCKET_MSG,
            serverMessage
        }
        expect(actions.connectSocketMsg(serverMessage)).toEqual(expectedAction)
    })
})