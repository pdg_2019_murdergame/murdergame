import connectionStateReducer from '../../../src/redux/connectionState/connectionStateReducer'
import * as types from '../../../src/redux/connectionState/connectionStateTypes'

describe('connectionState REDUCER', () => {

    const initialState = {
        loading: false,
        socket: null,
        serverMessage: '',
        token: '',
        role: '',
        error: ''
    }

    it('should return the initial state', () => {
        expect(connectionStateReducer(undefined, {})).toEqual(initialState)
    })

    it('should handle CONNECT_SOCKET_REQUEST', () => {
        expect(connectionStateReducer(initialState, {
            type: types.CONNECT_SOCKET_REQUEST
        })).toEqual({
            loading: true,
            socket: null,
            serverMessage: '',
            token: '',
            role: '',
            error: ''
        })
    })

    it('should handle CONNECT_SOCKET_SUCCESS', () => {
        expect(connectionStateReducer(initialState, {
            type: types.CONNECT_SOCKET_SUCCESS,
            socket: 'socket',
            role: 'role'
        })).toEqual({
            loading: false,
            socket: 'socket',
            serverMessage: '',
            token: '',
            role: 'role',
            error: ''
        })
    })

    it('should handle CONNECT_SOCKET_FAILURE', () => {
        expect(connectionStateReducer(initialState, {
            type: types.FETCH_TOKEN_FAILURE,
            error: 'error'
        })).toEqual({
            loading: false,
            socket: null,
            serverMessage: '',
            token: '',
            role: '',
            error: 'error'
        })
    })

    it('should handle DISCONNECT_SOCKET', () => {
        expect(connectionStateReducer(initialState, {
            type: types.DISCONNECT_SOCKET
        })).toEqual({
            loading: false,
            socket: null,
            serverMessage: '',
            token: '',
            role: '',
            error: ''
        })
    })

    it('should handle CONNECT_SOCKET_MSG', () => {
        expect(connectionStateReducer(initialState, {
            type: types.CONNECT_SOCKET_MSG,
            serverMessage: 'message'
        })).toEqual({
            loading: false,
            socket: null,
            serverMessage: 'message',
            token: '',
            role: '',
            error: ''
        })
    })

    it('should handle FETCH_TOKEN_REQUEST', () => {
        expect(connectionStateReducer(initialState, {
            type: types.FETCH_TOKEN_REQUEST,
        })).toEqual({
            loading: true,
            socket: null,
            serverMessage: '',
            token: '',
            role: '',
            error: ''
        })
    })

    it('should handle FETCH_TOKEN_SUCCESS', () => {
        expect(connectionStateReducer(initialState, {
            type: types.FETCH_TOKEN_SUCCESS,
            token: 'token'
        })).toEqual({
            loading: false,
            socket: null,
            serverMessage: '',
            token: 'token',
            role: '',
            error: ''
        })
    })

    it('should handle FETCH_TOKEN_FAILURE', () => {
        expect(connectionStateReducer(initialState, {
            type: types.FETCH_TOKEN_FAILURE,
            error: 'error'
        })).toEqual({
            loading: false,
            socket: null,
            serverMessage: '',
            token: '',
            role: '',
            error: 'error'
        })
    })
})