import * as actions from '../../../src/redux/serverState/serverStateAction'
import * as types from '../../../src/redux/serverState/serverStateTypes'
import thunk from 'redux-thunk'
import configureStore from 'redux-mock-store'
import moxios from 'moxios'

const mockStore = configureStore([thunk])

describe('serverState ACTION', () => {

  it('should create an action on server fetch request', () => {
    const expectedAction = {
      type: types.FETCH_SERVER_STATE_REQUEST
    }
    expect(actions.fetchServerStateRequest()).toEqual(expectedAction)
  })

  it('should create an action on server fetch success', () => {
    const state = 'gut'
    const expectedAction = {
      type: types.FETCH_SERVER_STATE_SUCCESS,
      state
    }
    expect(actions.fetchServerStateSuccess(state)).toEqual(expectedAction)
  })

  it('should crate an action on server fetch failure', () => {
    const error = 'error'
    const expectedAction = {
      type: types.FETCH_SERVER_STATE_FAILURE,
      error
    }
    expect(actions.fetchServerStateFailure(error)).toEqual(expectedAction)
  })
})

describe('serverState ACTION ASYNC Success', () => {
  beforeEach(() => moxios.install())
  afterEach(() => moxios.uninstall())

  it('should call dispatch status on successfully fetching server', () => {
      moxios.wait(() => {
        const request = moxios.requests.mostRecent()
        request.respondWith({
          status: 200
        })
      })

      const expectedActions = [
        actions.fetchServerStateRequest(),
        actions.fetchServerStateSuccess(true)
      ]

      const store = mockStore({})

      return store.dispatch(actions.fetchServerState()).then(() => {
        expect(store.getActions()).toEqual(expectedActions)
      })
  })
})