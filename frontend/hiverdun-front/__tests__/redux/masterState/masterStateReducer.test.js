import masterStateReducer from '../../../src/redux/masterState/masterStateReducer'
import * as types from '../../../src/redux/masterState/masterStateTypes'

describe('masterState REDUCER', () => {

  const initialState = {
    votes: [],
    guilty: ''
  }

  it('should return the initial state', () => {
    expect(masterStateReducer(undefined, {})).toEqual(initialState)
  })

  it('should handle UPDATE_VOTES', () => {
    const votes = ['vote1', 'vote2']
    expect(masterStateReducer(initialState, {
      type: types.UPDATE_VOTES,
      votes
    })).toEqual({
      votes: ['vote1', 'vote2'],
      guilty: ''
    })
  })

  it('should handle UPDATE_GUILTY', () => {
    const name = 'guilty'
    expect(masterStateReducer(initialState, {
      type: types.UPDATE_GUILTY,
      name
    })).toEqual({
      votes: [],
      guilty: 'guilty'
    })
  })

  it('should handle CLEAR_MASTER_STATE', () => {
    expect(masterStateReducer({
      votes: ['123'],
      guilty: 'guilty'
    }, {
      type: types.CLEAR_MASTER_STATE
    })).toEqual(initialState)
  })
})