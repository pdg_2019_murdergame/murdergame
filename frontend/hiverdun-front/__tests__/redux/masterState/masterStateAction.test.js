import * as actions from '../../../src/redux/masterState/masterStateAction'
import * as types from '../../../src/redux/masterState/masterStateTypes'

describe('masterState ACTION', () => {

  it('should create an action on votes update', () => {
    const votes = ['vote1', 'vote2']
    const expectedAction = {
      type: types.UPDATE_VOTES,
      votes
    }
    expect(actions.updateVotes(votes)).toEqual(expectedAction)
  })

  it('should create an action on guilty update', () => {
    const name = 'guilty'
    const expectedAction = {
      type: types.UPDATE_GUILTY,
      name
    }
    expect(actions.updateGuilty(name)).toEqual(expectedAction)
  })

  it('should create an action on master state clear', () => {
    const expectedAction = {
      type: types.CLEAR_MASTER_STATE
    }
    expect(actions.clearMasterState()).toEqual(expectedAction)
  })
})