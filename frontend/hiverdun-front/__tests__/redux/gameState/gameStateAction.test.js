import * as actions from '../../../src/redux/gameState/gameStateAction'
import * as types from '../../../src/redux/gameState/gameStateTypes'

describe('gameState ACTION', () => {

  it('should create an action on player number update', () => {
    const numberOfPlayers = 4
    const expectedAction = {
      type: types.UPDATE_PLAYER_NUMBER,
      numberOfPlayers
    }
    expect(actions.updatePlayerNumber(numberOfPlayers)).toEqual(expectedAction)
  })

  it('should create an action on party code set', () => {
    const partyCode = 'code'
    const expectedAction = {
      type: types.SET_PARTY_CODE,
      partyCode
    }
    expect(actions.setPartyCode(partyCode)).toEqual(expectedAction)
  })

  it('should create an action on clear game state', () => {
    const expectedAction = {
      type: types.CLEAR_GAME_STATE
    }
    expect(actions.clearGameState()).toEqual(expectedAction)
  })

  it('should create an action to update the players', () => {
    const expectedAction = {
      type: types.UPDATE_PLAYERS,
      players: []
    }
    expect(actions.updatePlayers([])).toEqual(expectedAction)
  })

  it('should create an action on state update', () => {
    const newState = 'state'
    const expectedAction = {
      type: types.UPDATE_STATE,
      newState
    }
    expect(actions.updateState(newState)).toEqual(expectedAction)
  })

  it('should create an action on cabal votes update', () => {
    const votes = []
    const expectedAction = {
      type: types.UPDATE_CABAL_VOTES,
      votes
    }
    expect(actions.updateCabalVotes(votes)).toEqual(expectedAction)
  })

  it('should create an action on last deads set', () => {
    const deads = []
    const expectedAction = {
      type: types.SET_LAST_DEADS,
      deads
    }
    expect(actions.setLastDeads(deads)).toEqual(expectedAction)
  })
})