import gameStateReducer from '../../../src/redux/gameState/gameStateReducer'
import * as types from '../../../src/redux/gameState/gameStateTypes'

describe('gameState REDUCER', () => {

  const initialState = {
    partyCode: '',
    numberOfPlayers: 0,
    gameState: 'LobbyState',
    players: [],
    cabalVotes: [],
    lastDeads: [],
    winner: ''
  }

  it('should return the initial state', () => {
    expect(gameStateReducer(undefined, {})).toEqual(initialState)
  })

  it('should handle UPDATE_PLAYER_NUMBER', () => {
    expect(gameStateReducer(initialState, {
      type: types.UPDATE_PLAYER_NUMBER,
      numberOfPlayers: 4
    })).toEqual({
      partyCode: '',
      numberOfPlayers: 4,
      gameState: 'LobbyState',
      players: [],
      cabalVotes: [],
      lastDeads: [],
      winner: ''
    })
  })

  it('should handle SET_PARTY_CODE', () => {
    expect(gameStateReducer(initialState, {
      type: types.SET_PARTY_CODE,
      partyCode: 'CODE12'
    })).toEqual({
      partyCode: 'CODE12',
      numberOfPlayers: 0,
      gameState: 'LobbyState',
      players: [],
      cabalVotes: [],
      lastDeads: [],
      winner: ''
    })
  })

  it('should handle CLEAR_GAME_STATE', () => {
    expect(gameStateReducer(initialState, {
      type: types.CLEAR_GAME_STATE
    })).toEqual({
      partyCode: '',
      numberOfPlayers: 0,
      gameState: 'LobbyState',
      players: [],
      cabalVotes: [],
      lastDeads: [],
      winner: ''
    })
  })

  it('should handle UPDATE_PLAYERS', () => {
    expect(gameStateReducer(initialState, {
      type: types.UPDATE_PLAYERS,
      players: ['player1', 'player2']
    })).toEqual({
      partyCode: '',
      numberOfPlayers: 0,
      gameState: 'LobbyState',
      players: ['player1', 'player2'],
      cabalVotes: [],
      lastDeads: [],
      winner: ''
    })
  })

  it('should handle UPDATE_STATE', () => {
    expect(gameStateReducer(initialState, {
      type: types.UPDATE_STATE,
      newState: 'newstate'
    })).toEqual({
      partyCode: '',
      numberOfPlayers: 0,
      gameState: 'newstate',
      players: [],
      cabalVotes: [],
      lastDeads: [],
      winner: ''
    })
  })

  it('should handle UPDATE_CABAL_VOTES', () => {
    expect(gameStateReducer(initialState, {
      type: types.UPDATE_CABAL_VOTES,
      votes: ['vote1', 'vote2']
    })).toEqual({
      partyCode: '',
      numberOfPlayers: 0,
      gameState: 'LobbyState',
      players: [],
      cabalVotes: ['vote1', 'vote2'],
      lastDeads: [],
      winner: ''
    })
  })

  it('should handle SET_LAST_DEADS', () => {
    expect(gameStateReducer(initialState, {
      type: types.SET_LAST_DEADS,
      deads: ['dead1', 'dead2']
    })).toEqual({
      partyCode: '',
      numberOfPlayers: 0,
      gameState: 'LobbyState',
      players: [],
      cabalVotes: [],
      lastDeads: ['dead1', 'dead2'],
      winner: ''
    })
  })
})