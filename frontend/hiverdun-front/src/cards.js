import BrokenCard from './models/BrokenCard'
import ProphecyCard from './models/ProphecyCard'
import AnkhCard from './models/AnkhCard'
import SacrificeCard from './models/SacrificeCard'
import HardSkinCard from './models/HardSkinCard'
import DeathPoisonCard from './models/DeathPoisonCard'
import PrayerCard from './models/PrayerCard'
import HintCard from './models/HintCard'
import HeirCard from './models/HeirCard'
import BurgleCard from './models/BurgleCard'


/**
 * Create a new Card with given id
 * @param {number} id the id of the Card to create
 */
export const createCard = id => {
    switch (id) {
        case 0:
            return new BrokenCard(id)
        case 1:
            return new ProphecyCard(id)
        case 2:
            return new AnkhCard(id)
        case 3:
            return new DeathPoisonCard(id)
        case 4:
            return new HeirCard(id)
        case 5:
            return new PrayerCard(id)
        case 6:
            return new SacrificeCard(id)
        case 8:
            return new BurgleCard(id)
        case 9:
            return new HardSkinCard(id)
        case 14:
            return new HintCard(id)
        default:
            return null
    }
}

/**
 * Create a new Card Array with given array of ids
 * @param {Array[number]} cardIds the array of card ids
 */
export const createCardArray = cardIds => {
    const cardArray = []

    for (const id of cardIds) {
        const card = createCard(id)
        if (card !== null) {
            cardArray.push(card)
        }
    }

    return cardArray
}