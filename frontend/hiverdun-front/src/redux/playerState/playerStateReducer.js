import {
    SET_NICKNAME,
    UPDATE_CARDS,
    SET_TEAM,
    KILL_PLAYER,
    RESURECT_PLAYER,
    CLEAR_PLAYER_STATE,
    SET_CARD_SLOT
} from './playerStateTypes'

const initialState = {
    nickname: '',
    cards: [],
    cardslot: null,
    team: '',
    alive: true
}

const playerStateReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_NICKNAME:
            return {
                ...state,
                nickname: action.nickname
            }
        case UPDATE_CARDS:
            return {
                ...state,
                cards: action.cards
            }
        case SET_TEAM:
            return {
                ...state,
                team: action.team
            }
        case KILL_PLAYER:
            return {
                ...state,
                alive: false
            }
        case RESURECT_PLAYER:
            return {
                ...state,
                alive: true
            }
        case CLEAR_PLAYER_STATE:
            return {
                ...initialState
            }
        case SET_CARD_SLOT:
            return {
                ...state,
                cardslot: action.card
            }
        default:
            return state
    }
}

export default playerStateReducer