import {
    SET_NICKNAME,
    UPDATE_CARDS,
    SET_TEAM,
    KILL_PLAYER,
    RESURECT_PLAYER,
    CLEAR_PLAYER_STATE,
    SET_CARD_SLOT
} from './playerStateTypes'

export const setNickname = nickname => {
    return {
        type: SET_NICKNAME,
        nickname
    }
}

export const updateCards = cards => {
    return {
        type: UPDATE_CARDS,
        cards
    }
}

export const setTeam = team => {
    return {
        type: SET_TEAM,
        team
    }
}

export const killPlayer = () => {
    return {
        type: KILL_PLAYER
    }
}

export const resurectPlayer = () => {
    return {
        type: RESURECT_PLAYER
    }
}

export const clearPlayerState = () => {
    return {
        type: CLEAR_PLAYER_STATE
    }
}

export const setCardslot = card => {
    return {
        type: SET_CARD_SLOT,
        card
    }
}