import { combineReducers } from 'redux'
import { reducer as burgerMenu } from 'redux-burger-menu'

import serverStateReducer from './serverState/serverStateReducer'
import connectionStateReducer from './connectionState/connectionStateReducer'
import gameStateReducer from './gameState/gameStateReducer'
import masterStateReducer from './masterState/masterStateReducer'
import playerStateReducer from './playerState/playerStateReducer'

const rootReducer = combineReducers({
    serverState: serverStateReducer,
    connection: connectionStateReducer,
    gameState: gameStateReducer,
    masterState: masterStateReducer,
    playerState: playerStateReducer,
    burgerMenu
})

export default rootReducer