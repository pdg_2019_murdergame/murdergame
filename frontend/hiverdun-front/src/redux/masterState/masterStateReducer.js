import {
    UPDATE_VOTES,
    UPDATE_GUILTY,
    CLEAR_MASTER_STATE
} from './masterStateTypes'

const initialState = {
    votes: [],
    guilty: ''
}

const masterStateReducer = (state = initialState, action) => {
    switch (action.type) {
        case UPDATE_VOTES:
            return {
                ...state,
                votes: action.votes
            }
        case UPDATE_GUILTY:
            return {
                ...state,
                guilty: action.name
            }
        case CLEAR_MASTER_STATE:
            return {
                ...initialState
            }
        default:
            return state
    }
}

export default masterStateReducer