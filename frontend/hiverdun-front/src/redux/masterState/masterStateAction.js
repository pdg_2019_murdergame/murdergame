import {
    UPDATE_VOTES,
    UPDATE_GUILTY,
    CLEAR_MASTER_STATE
} from './masterStateTypes'

export const updateVotes = votes => {
    return {
        type: UPDATE_VOTES,
        votes
    }
}

export const updateGuilty = name => {
    return {
        type: UPDATE_GUILTY,
        name
    }
}

export const clearMasterState = () => {
    return {
        type: CLEAR_MASTER_STATE
    }
}