import { createStore, applyMiddleware } from 'redux'
// middleware
import thunk from 'redux-thunk'
import logger from 'redux-logger'
// reducer
import rootReducer from './rootReducer'

const store = createStore(rootReducer, applyMiddleware(thunk, logger))

export default store