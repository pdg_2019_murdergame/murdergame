import {
    FETCH_SERVER_STATE_REQUEST,
    FETCH_SERVER_STATE_SUCCESS,
    FETCH_SERVER_STATE_FAILURE
} from './serverStateTypes'

import axios from 'axios'

// express server url
const serverAddress = process.env.REACT_APP_API_URL

export const fetchServerStateRequest = () => {
    return {
        type: FETCH_SERVER_STATE_REQUEST
    }
}

export const fetchServerStateSuccess = serverState => {
    return {
        type: FETCH_SERVER_STATE_SUCCESS,
        state: serverState
    }
}

export const fetchServerStateFailure = error => {
    return {
        type: FETCH_SERVER_STATE_FAILURE,
        error: error
    }
}

/**
 * Async server state fetch
 */
export const fetchServerState = () => {
    return function(dispatch) {
        dispatch(fetchServerStateRequest())
        return axios.get(serverAddress + '/rest/alive')
            .then(response => {
                const state = (response.status === 200) || (response.status === 304)
                dispatch(fetchServerStateSuccess(state))
            })
            .catch(error => {
                dispatch(fetchServerStateFailure(error.message))
            })
    }
}