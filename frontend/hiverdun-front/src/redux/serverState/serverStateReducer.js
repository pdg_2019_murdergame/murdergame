import {
    FETCH_SERVER_STATE_REQUEST,
    FETCH_SERVER_STATE_SUCCESS,
    FETCH_SERVER_STATE_FAILURE
} from './serverStateTypes'

const initialState = {
    loading: false,
    alive: false,
    error: ''
}

const serverStateReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_SERVER_STATE_REQUEST:
            return {
                ...state,
                loading: true
            }
        case FETCH_SERVER_STATE_SUCCESS:
            return {
                loading: false,
                alive: action.state
            }
        case FETCH_SERVER_STATE_FAILURE:
            return {
                loading: false,
                alive: false,
                error: action.error
            }
        default:
            return state
    }
}

export default serverStateReducer