import {
    UPDATE_PLAYER_NUMBER,
    SET_PARTY_CODE,
    CLEAR_GAME_STATE,
    UPDATE_PLAYERS,
    UPDATE_STATE,
    UPDATE_CABAL_VOTES,
    SET_LAST_DEADS,
    SET_WINNER
} from './gameStateTypes'

export const updatePlayerNumber = numberOfPlayers => {
    return {
        type: UPDATE_PLAYER_NUMBER,
        numberOfPlayers
    }
}

export const setPartyCode = partyCode => {
    return {
        type: SET_PARTY_CODE,
        partyCode
    }
}

export const clearGameState = () => {
    return {
        type: CLEAR_GAME_STATE
    }
}

export const updatePlayers = players => {
    return {
        type: UPDATE_PLAYERS,
        players
    }
}

export const updateState = newState => {
    return {
        type: UPDATE_STATE,
        newState
    }
}

export const updateCabalVotes = votes => {
    return {
        type: UPDATE_CABAL_VOTES,
        votes
    }
}

export const setLastDeads = deads => {
    return {
        type: SET_LAST_DEADS,
        deads
    }
}

export const setWinner = winner => {
    return {
        type: SET_WINNER,
        winner
    }
}