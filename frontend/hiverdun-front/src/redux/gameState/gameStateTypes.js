export const UPDATE_PLAYER_NUMBER = 'UPDATE_PLAYER_NUMBER'
export const SET_PARTY_CODE = 'SET_PARTY_CODE'
export const CLEAR_GAME_STATE = 'CLEAR_GAME_STATE'
export const UPDATE_PLAYERS = 'UPDATE_PLAYERS'
export const UPDATE_STATE = 'UPDATE_STATE'
export const UPDATE_CABAL_VOTES = 'UPDATE_CABAL_VOTES'
export const SET_LAST_DEADS = 'SET_LAST_DEADS'
export const SET_WINNER = 'SET_WINNER'