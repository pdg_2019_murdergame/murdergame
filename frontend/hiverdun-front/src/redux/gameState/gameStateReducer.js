import {
    UPDATE_PLAYER_NUMBER,
    SET_PARTY_CODE,
    CLEAR_GAME_STATE,
    UPDATE_PLAYERS,
    UPDATE_STATE,
    UPDATE_CABAL_VOTES,
    SET_LAST_DEADS,
    SET_WINNER
} from './gameStateTypes'

const initialState = {
    partyCode: '',
    numberOfPlayers: 0,
    gameState: 'LobbyState',
    players: [],
    cabalVotes: [],
    lastDeads: [],
    winner: ''
}

const gameStateReducer = (state = initialState, action) => {
    switch (action.type) {
        case UPDATE_PLAYER_NUMBER:
            return {
                ...state,
                numberOfPlayers: action.numberOfPlayers
            }
        case SET_PARTY_CODE:
            return {
                ...state,
                partyCode: action.partyCode
            }
        case CLEAR_GAME_STATE:
            return {
                ...initialState
            }
        case UPDATE_PLAYERS:
            return {
                ...state,
                players: action.players
            }
        case UPDATE_STATE:
            return {
                ...state,
                gameState: action.newState
            }
        case UPDATE_CABAL_VOTES:
            return {
                ...state,
                cabalVotes: action.votes
            }
        case SET_LAST_DEADS:
            return {
                ...state,
                lastDeads: action.deads
            }
        case SET_WINNER:
            return {
                ...state,
                winner: action.winner
            }
        default:
            return state
    }
}

export default gameStateReducer