import {
    CONNECT_SOCKET_REQUEST,
    CONNECT_SOCKET_SUCCESS,
    CONNECT_SOCKET_FAILURE,
    DISCONNECT_SOCKET,
    FETCH_TOKEN_REQUEST,
    FETCH_TOKEN_SUCCESS,
    FETCH_TOKEN_FAILURE,
    CONNECT_SOCKET_MSG
} from './connectionStateTypes'

const initialState = {
    loading: false,
    socket: null,
    serverMessage: '',
    token: '',
    role: '',
    error: ''
}

const connectionStateReducer = (state = initialState, action) => {
    switch (action.type) {
        case CONNECT_SOCKET_REQUEST:
            return {
                ...state,
                error: '',
                loading: true
            }
        case CONNECT_SOCKET_SUCCESS:
            return {
                ...state,
                loading: false,
                socket: action.socket,
                role: action.role
            }
        case CONNECT_SOCKET_FAILURE:
            return {
                ...state,
                loading: false,
                socket: null,
                error: action.error
            }
        case DISCONNECT_SOCKET:
            return {
                ...state,
                loading: false,
                token: '',
                socket: null,
                role: ''
            }
        case CONNECT_SOCKET_MSG:
            return {
                ...state,
                serverMessage: action.serverMessage
            }
        case FETCH_TOKEN_REQUEST:
            return {
                ...state,
                error: '',
                loading: true
            }
        case FETCH_TOKEN_SUCCESS:
            return {
                ...state,
                loading: false,
                token: action.token
            }
        case FETCH_TOKEN_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        default:
            return state
    }
}

export default connectionStateReducer