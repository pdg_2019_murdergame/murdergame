import {
    CONNECT_SOCKET_REQUEST,
    CONNECT_SOCKET_SUCCESS,
    CONNECT_SOCKET_FAILURE,
    DISCONNECT_SOCKET,
    CONNECT_SOCKET_MSG,
    FETCH_TOKEN_REQUEST,
    FETCH_TOKEN_SUCCESS,
    FETCH_TOKEN_FAILURE
} from './connectionStateTypes'

import axios from 'axios'
import socketIOClient from 'socket.io-client'
import { setPartyCode } from '../gameState/gameStateAction'
import { setNickname } from '../playerState/playerStateAction'
import io from '../../net/socketAPI'

// express server url
const serverAddress = process.env.REACT_APP_API_URL

export const connectSocketRequest = () => {
    return {
        type: CONNECT_SOCKET_REQUEST
    }
}

export const connectSocketSuccess = (socket, role) => {
    return {
        type: CONNECT_SOCKET_SUCCESS,
        socket: socket,
        role
    }
}

export const connectSocketFailure = error => {
    return {
        type: CONNECT_SOCKET_FAILURE,
        error: error
    }
}

export const disconnectSocket = () => {
    return {
        type: DISCONNECT_SOCKET
    }
}

export const connectSocketMsg = msg => {
    return {
        type: CONNECT_SOCKET_MSG,
        serverMessage: msg
    }
}

export const fetchTokenRequest = () => {
    return {
        type: FETCH_TOKEN_REQUEST
    }
}

export const fetchTokenSuccess = token => {
    return {
        type: FETCH_TOKEN_SUCCESS,
        token: token
    }
}

export const fetchTokenFailure = err => {
    return {
        type: FETCH_TOKEN_FAILURE,
        error: err
    }
}

/**
 * Async socket connection
 */
const connectSocket = (token, role = 'player') => {
    return function(dispatch) {
        dispatch(connectSocketRequest())

        const socket = socketIOClient(serverAddress, {
            query: {
                token
            }
        })

        io.initListeners(socket, dispatch, role)
    }
}

/**
 * Aync party token fetching and connection on the socket (player)
 */
export const joinParty = (nickname, partyCode) => {
    return function(dispatch) {
        dispatch(fetchTokenRequest())
        axios.post(serverAddress + '/rest/players', {
                nickname
            }, {
                params: {
                    code: partyCode
                }
            })
            .then(response => {
                if (response.status === 200) {
                    const token = response.data.token
                    dispatch(fetchTokenSuccess(token))

                    // set party code
                    dispatch(setPartyCode(partyCode))

                    // set nickname
                    dispatch(setNickname(nickname))

                    // connect to socket
                    dispatch(connectSocket(token))
                } else {
                    dispatch(fetchTokenFailure(response.data))
                }
            })
            .catch(err => {
                dispatch(fetchTokenFailure(err.message))
            })
    }
}

/**
 * Aync party token fetching and connection on the socket (master)
 */
export const createParty = () => {
    return function(dispatch) {
        dispatch(fetchTokenRequest())
        axios.post(serverAddress + '/rest/parties')
            .then(response => {
                if (response.status === 200) {
                    const token = response.data.token
                    dispatch(fetchTokenSuccess(token))

                    // set party code
                    dispatch(setPartyCode(response.data.party.code))

                    // connect to socket
                    dispatch(connectSocket(token, 'master'))
                } else {
                    dispatch(fetchTokenFailure(response.data))
                }
            })
            .catch(err => {
                dispatch(fetchTokenFailure(err.message))
            })
    }
}