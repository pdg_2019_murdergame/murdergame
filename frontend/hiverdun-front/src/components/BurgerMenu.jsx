import { elastic as Menu } from 'react-burger-menu'
import { decorator as BurgerMenu } from 'redux-burger-menu'
import './BurgerMenu.scss'

// Wrap Burger-Menu in a Redux-Burger-Menu
export default BurgerMenu(Menu)