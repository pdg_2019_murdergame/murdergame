// React
import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
// CSS / icons
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import './LangButton.scss'

/**
 * Component to display the button to set the language
 * @param {string} param0 id to set the style from the parent component 
 */
function LangButton({ itemId }) {

  // translations (i18n)
  const [t, i18n] = useTranslation('translation')

  const changeLanguage = lng => {
    i18n.changeLanguage(lng)
  }

  // local states
  const [dropdownState, setDropdownState] = useState(false)

  return (
    <div id={itemId} className={dropdownState ? "dropdown bm-item is-active" : "dropdown bm-item"}
      onClick={() => { setDropdownState(prev => !prev) }}>
      <div className="dropdown-trigger">
        <button className="button is-dark is-large is-fullwidth belisa" aria-haspopup="true" aria-controls="dropdown-menu">
          <span>{t('Language')} - {i18n.language}</span>
          <span id="icon" className="icon is-small">
            <FontAwesomeIcon icon="angle-down"/>
          </span>
        </button>
      </div>
      <div className="dropdown-menu" id="dropdown-menu" role="menu">
        <div className="dropdown-content" id="dropdown-content">
          <div className="dropdown-item" onClick={() => changeLanguage('en')}>English</div>
          <div className="dropdown-item" onClick={() => changeLanguage('fr')}>Français</div>
        </div>
      </div>
    </div>
  )
}

export default LangButton
