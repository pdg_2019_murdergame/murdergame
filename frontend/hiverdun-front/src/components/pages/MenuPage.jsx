// React
import React from 'react'
import { Link } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
// Components
import ServerStatus from '../ServerStatus'
import LangButton from '../LangButton'
import BurgerMenu from '../BurgerMenu'
import About from '../About'
// CSS / icons
import burgerIcon from '../../assets/burger-menu/menu.svg'
import burgerCloseIcon from '../../assets/burger-menu/closing_eye.svg'
import houseImg from '../../assets/house_main.svg'
import './MenuPage.scss'

/**
 * Component (page) to rener the Menu interface
 */
function MenuPage() {
  
  // translations (i18n)
  const [t] = useTranslation('translation')

  return (
    <div id="outer-container">

      <BurgerMenu
        className="is-mobile"
        width={280}
        pageWrapId={"page-wrap"}
        outerContainerId={"outer-container"}
        customBurgerIcon={<img src={burgerIcon} alt="burger icon" />}
        customCrossIcon={<img src={burgerCloseIcon} alt="burger close icon" />}
        disableAutoFocus
      >
        <ServerStatus itemId="server-status" />
        <LangButton itemId="menu-lang" />
        <About itemId="menu-about" />
      </BurgerMenu>

      <main id="page-wrap">
        <section className="section" />
        <section id="buttons-section" className="section">
          <div className="columns is-centered is-vcentered is-mobile">
            <div className="column is-narrow has-text-centered">
              <div className="field">
                <div className="control">
                  <Link to='/player/join-lobby'>
                    <button className="button is-dark is-large is-fullwidth is-size-2 belisa">
                      {t('Join')}
                    </button>
                  </Link>
                </div>
              </div>
              <div className="field">
                <div className="control">
                  <Link to='/master/create-lobby'>
                    <button className="button is-dark is-large is-fullwidth is-size-2 belisa">
                      {t('Create')}
                    </button>
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </section>
      </main>
      <figure className="image">
        <div id="barrier"></div>
      </figure>
      <div className="content has-text-centered">
        <figure className="image">
          <img id="house" src={houseImg} alt="house" />
        </figure>
      </div>
    </div>
  )
}

export default MenuPage
