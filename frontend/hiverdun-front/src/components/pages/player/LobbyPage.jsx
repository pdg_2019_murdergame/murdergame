// React
import React, { useState, useEffect } from 'react'
import { Redirect } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'
import { connectSocketMsg, disconnectSocket, clearGameState, clearPlayerState } from '../../../redux'
import { useTranslation } from 'react-i18next'
// Network
import io from '../../../net/socketAPI'
// Components
import { useToasts } from 'react-toast-notifications'
import BurgerMenu from '../../BurgerMenu'
import ServerStatus from '../../ServerStatus'
import LangButton from '../../LangButton'
import About from '../../About'
// CSS / icons
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import burgerIcon from '../../../assets/burger-menu/menu.svg'
import burgerCloseIcon from '../../../assets/burger-menu/closing_eye.svg'
import ribbonIcon from '../../../assets/ribbon.svg'
import './LobbyPage.scss'

/**
 * Component (page) to display the Lobby interface to the user (player)
 */
function LobbyPage() {

  // translations (i18n)
  const [t] = useTranslation('translation')

  // redux states
  const dispatch = useDispatch()
  const gameState = useSelector(state => state.gameState)
  const connState = useSelector(state => state.connection)

  // local states
  const [playersList, setPlayersList] = useState([])
  const minPlayers = 4 // min will be 8 in production
  const maxPlayers = 18
  const {addToast} = useToasts()

  // update player icons display
  useEffect(() => {
    updatePlayerList(gameState.numberOfPlayers, maxPlayers)
  }, [gameState.numberOfPlayers])

  // show connections/disconnections toasts
  useEffect(() => {
    if(connState.serverMessage !== '') {
      addToast(connState.serverMessage, {
        appearance: 'info'
      })
    }
    dispatch(connectSocketMsg(''))
  }, [connState.serverMessage, addToast, dispatch])

  /**
   * Disconnect socket, clear stats
   */
  const leaveLobby = () => {
    io.disconnect()
    dispatch(clearGameState())
    dispatch(clearPlayerState())
    dispatch(disconnectSocket())
  }

  /**
   * Renders the player icons
   * @param {number} numberOfPlayers
   * @param {number} maxPlayers 
   */
  const updatePlayerList = (numberOfPlayers, maxPlayers) => {
    const players = []
    // connected players
    for(let i = 0; i < numberOfPlayers; i++) {
      players.push(
        <div key={i} className="column has-text-centered ">
          <FontAwesomeIcon icon="user-circle" className="circle has-text-primary" />
        </div>
      )
    }

    // empty player slots
    for(let i = numberOfPlayers; i < maxPlayers; i++) {
      players.push(
        <div key={i} className="column has-text-centered ">
          <FontAwesomeIcon icon="user-circle" className="circle has-text-white" />
        </div>
      )
    }

    setPlayersList(players)
  }

  // redirection in case the player is not connected to a game
  if(connState.socket == null) {
    return <Redirect to='/player/join-lobby' />
  }

  // redirection in case the user is connected as master
  if(connState.role === 'master') {
    return <Redirect to='/master' />
  }

  // redirect to game page
  if(gameState.gameState === 'StoryState') {
    return <Redirect to='/player/hiverdun' />
  }

  return (
    <div id="outer-container">
      <BurgerMenu
        className="is-mobile"
        width={280}
        pageWrapId={"page-wrap"}
        outerContainerId={"outer-container"}
        customBurgerIcon={<img src={burgerIcon} alt="burger icon" />}
        customCrossIcon={<img src={burgerCloseIcon} alt="burger close icon" />}
        disableAutoFocus
      >
        <ServerStatus itemId="server-status" />
        <LangButton itemId="menu-lang" />
        <button className='button is-danger' style={{width: '5em'}} onClick={leaveLobby} >
          {t('Leave')}
        </button>
        <About itemId="menu-about" />
      </BurgerMenu>
      <main id="page-wrap">
        <section className="section" />
        <section className="section is-medium has-text-centered">
          <h1 className="belisa is-size-1">{t('PartyCode')}</h1>
          <figure className="image">
            <img id="ribbon" src={ribbonIcon} alt="ribbon" />
            <h2 id="ribbon-text" className="has-text-danger is-size-3">{gameState.partyCode}</h2>
          </figure>
        </section>

        <section className="section">
          <div className="columns">
            <div className="column is-one-third is-offset-one-third">
              <div className="columns is-centered is-multiline is-mobile">
                {playersList}
              </div>
            </div>
          </div>
          <div className="container has-text-centered">
            <span className={
              gameState.numberOfPlayers < minPlayers
              ? "has-text-danger"
              : "has-text-primary"
            }>
              {gameState.numberOfPlayers}
            </span>
            <span>
              /{`${maxPlayers} ` + t('PlayersInLobby')}
            </span>
            <button
              id='leave-button'
              className='button is-danger is-medium'
              onClick={leaveLobby}
            >
              {t('Leave')}
            </button>
          </div>
        </section>
      </main>
    </div>
  )
}

export default LobbyPage