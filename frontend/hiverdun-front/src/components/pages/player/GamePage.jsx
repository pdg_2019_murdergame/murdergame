// React
import React, { useState, useEffect } from 'react'
import { Redirect } from 'react-router-dom'
import { useSelector } from 'react-redux'
import { useTranslation } from 'react-i18next'
// Network
import io from '../../../net/socketAPI'
// Components
import ReactLoading from 'react-loading'
import BurgerMenu from '../../BurgerMenu'
import CardSelection from './gameComponents/CardSelection'
import CardPlay from './gameComponents/CardPlay'
import VotingDisplay from './gameComponents/VotingDisplay'
import burgerIcon from '../../../assets/burger-menu/menu.svg'
import burgerCloseIcon from '../../../assets/burger-menu/closing_eye.svg'
import ServerStatus from '../../ServerStatus'
import LangButton from '../../LangButton'
import About from '../../About'
import WaitingDisplay from './gameComponents/WaitingDisplay'
// CSS / icons
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import './GamePage.scss'

/**
 * Main Game Component wrapping the whole player's game content
 */
function GamePage() {

  // translations (i18n)
  const [t] = useTranslation('translation')

  // local states
  const [cardPick, setCardPick] = useState(true)

  // redux states
  const playerState = useSelector(state => state.playerState)
  const gameState = useSelector(state => state.gameState)
  const connection = useSelector(state => state.connection)

  // update player's personnal info at every game state transition
  useEffect(() => {
      io.emit.me()


      // set winner on endgame state
      if(gameState.gameState.includes('End')) {
        io.emit.askWinner()
      }

      // reset pick card for next night
      if(gameState.gameState.includes('Dawn')) {
        setCardPick(true)
      }
      
  }, [gameState.gameState])

  const renderBurgerMenu = () => {
    return (
      <BurgerMenu
        className="is-mobile"
        width={280}
        pageWrapId={"page-wrap"}
        outerContainerId={"outer-container"}
        customBurgerIcon={<img src={burgerIcon} alt="burger icon" />}
        customCrossIcon={<img src={burgerCloseIcon} alt="burger close icon" />}
        disableAutoFocus
      >
        <ServerStatus itemId="server-status" />
        <LangButton itemId="menu-lang" />
        <About itemId="menu-about" />
      </BurgerMenu>
    )
  }

  // socket is not initialized, redirect
  if(connection.socket === null) {
    return <Redirect to='/menu' />
  }

  // player is ALIVE
  if(playerState.alive) {

    // STATE : Story
    if(gameState.gameState === 'StoryState') {
      return (
        <div>
          <h1 id='info' className='belisa' >
            {t('LookMainScreen')}
          </h1>
          <ReactLoading
            type='spinningBubbles'
            color='#ed4c67'
            className='loading-spinner'
            width='10em'
            height='0em'
          />
        </div>
      )
    }
  
    // STATE : Night
    if(gameState.gameState.includes('Night')) {
      return (
        <div id="outer-container">
          {renderBurgerMenu()}
          <main id="page-wrap" className=' columns is-centered'>
            <div className='column'>
              <section className='vertical-space'/>
              {
                cardPick ?
                  <CardSelection setCardPick={setCardPick} />
                :
                  <CardPlay isDawn={false} />
              }
            </div>
          </main>
        </div>
      )
    }
  
    /**
     * Render CardPlay if the player has cards to be played in the Dawn state
     * else WaitingDisplay.
     */
    const renderDawn = () => {

      // check if the player has a card playable in the dawn state
      if(playerState.cards.reduce((acc, c) => acc || c.dawn, false)) {
        return <CardPlay isDawn={true} />
      } else {
        // player is ready for next state (emit 'pass')
        return <WaitingDisplay />
      }
    }

    // STATE : Dawn
    if(gameState.gameState.includes('Dawn')) {
      return (
        <div id="outer-container">
          {renderBurgerMenu()}
          <main id="page-wrap" className=' columns is-centered'>
          <div className='column'>
            <section className='vertical-space'/>
            {renderDawn()}
            </div>
          </main>
        </div>
      )
    }
  
    // STATE : Day
    if(gameState.gameState.includes('Day')) {
      return (
        <div id="outer-container">
          {renderBurgerMenu()}
          <main id="page-wrap">
            <VotingDisplay />
          </main>
        </div>
      )
    }
  
    // STATE : End (Game Over)
    if(gameState.gameState === 'EndState') {
      return <Redirect to='/player/end' />
    }
  }

  // STATE : End (Game Over and player dead)
  if(gameState.gameState === 'EndState') {
    return <Redirect to='/player/end' />
  }

  // player is DEAD
  return (
    <div>
      <section className='section has-text-centered'>
        <div className='columns is-centered'>
          <div className='column is-two-thirds'>
            <h1 id='death-title' className='title is-1 has-text-danger belisa'>
              {t('YouDied')}
            </h1>
          </div>
        </div>
      </section>
      <section className='section has-text-centered'>
        <FontAwesomeIcon icon='skull' className='fa fa-10x' />
      </section>
    </div>
  )
}

export default GamePage
