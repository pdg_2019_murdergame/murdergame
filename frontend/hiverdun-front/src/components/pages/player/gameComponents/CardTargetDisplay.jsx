// React
import React, { useState } from 'react'
import { useSelector } from 'react-redux'
import { useTranslation } from 'react-i18next'
// Network
import io from '../../../../net/socketAPI'
// CSS / icons
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import './CardTargetDisplay.scss'

function CardTargetDisplay({setPlayMode, cardId}) {

  // redux states
  const gameState = useSelector(state => state.gameState)
  const playerState = useSelector(state => state.playerState)

  // translations (i18n)
  const [t] = useTranslation('translation')

  // local states
  const [targets, setTargets] = useState([])

  /**
   * Set or unset a target in the target array
   * @param {string} nickname the nickname of the targeted player
   */
  const selectTarget = nickname => {

    let newTargets

    if(!targets.includes(nickname)) {

      newTargets = [...targets]

      if(targets.length < getTargetsNumber(cardId)) {
        newTargets.push(nickname)
      } else {
        newTargets.shift()
        newTargets.push(nickname)
      }
    } else {
      newTargets = targets.filter(n => n !== nickname)
    }
    setTargets(newTargets)
  }

  /**
   * Render a target line
   * @param {player} p the player
   * @param {number} i the key
   */
  const targetLine = (p, i) => {
    return (
      <div key={i} className='panel-block'
      onClick={() => selectTarget(p.nickname)}
      >
        <span className={p.team ? 'team-color-' + p.team: 'team-color-unknown'}>
          <FontAwesomeIcon 
            icon={targets.includes(p.nickname) ? 'crosshairs' : 'circle'}
            className={'icon is-large'} 
          />
        </span>
        <span className={'player-name-slider player-name-color name-hoverable'}>
          {p.nickname}
        </span>
        <span className={p.online ? 'circle-online player-online-status' : 'circle-offline player-online-status'}>
          <FontAwesomeIcon 
            icon='circle'
            className={'icon is-right'}
          />
        </span>
      </div>
    )
  }

  /**
   * Returns the number of target for this card id
   * @param {number} id the card id
   */
  const getTargetsNumber = id => {
    switch(id) {
      case 1:
        return 1
      case 2:
        return 1
      case 3:
        return 1
      case 4:
        return 1
      case 5:
        return 1
      case 8:
        return 1
      case 14:
        return 1
      default:
        return 0
    }
  }

  /**
   * Render the player lines accoring to the card played
   * @param {Array[player]} players the players
   */
  const displayList = players => {
    switch(cardId) {
      case 2:
        // return only last deads
        return players.filter(p => gameState.lastDeads.map(d => d.nickname).includes(p.nickname)).map((p, i) => targetLine(p, i))
      case 4:
        // return every none Cabal player that is alive
        return players.filter(p => (p.team !== 'cabal' && p.alive)).map((p, i) => targetLine(p, i))
      case 8:
        // return all players that are alive exept self
        return players.filter(p => (p.alive && p.nickname !== playerState.nickname)).map((p, i) => targetLine(p, i))
      default:
        // return every player except self
        return players.filter(p => (p.alive && p.nickname !== playerState.nickname)).map((p, i) => targetLine(p, i))
    }
  }

  // In that case there shouldn't be targets for this card
  // go back to normal mode (shoud not happen)
  if(getTargetsNumber(cardId) < 1) {
    setPlayMode('')
  }

  return (
    <div>
      <section className='section has-text-centered'>
        <button
          className='button is-medium is-primary'
          disabled={targets.length !== getTargetsNumber(cardId)}
          onClick={
            () => {
              io.emit.playcard(cardId, targets)
              setPlayMode('waiting')
            }
          }
        >
          {t('Confirm')}
        </button>
      </section>
      <div className='columns is-centered'>
        <div className='column is-half'>
          <div className='has-text-centered'>
            {t('Select')} <span className='has-text-primary'>{getTargetsNumber(cardId) + ' '}</span>
            {t('Target') + (getTargetsNumber(cardId) > 1 ? 's' : '')}
          </div>
          <nav className='panel'>
            {displayList(gameState.players)}
          </nav>
        </div>
      </div>
    </div>
  )
}

export default CardTargetDisplay
