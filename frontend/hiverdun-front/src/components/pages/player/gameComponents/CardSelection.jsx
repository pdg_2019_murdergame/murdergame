// React
import React, { useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { updateCards, setCardslot } from '../../../../redux'
import { useTranslation } from 'react-i18next'
// Network
import io from '../../../../net/socketAPI'
// Component
import CardComp from '../../../CardComp'
import CardHand from '../gameComponents/CardHand'
import { DropTarget } from 'react-drag-drop-container'
import PlayerSlider from './PlayerSlider'
// CSS / icons
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import './CardSelection.scss'

/**
 * Component displaying the Card selection interface
 * @param {Function} param0 the callback to go in play state 
 */
function CardSelection({setCardPick}) {

  // translation
  const [t] = useTranslation('translation')

  // redux states
  const playerState = useSelector(state => state.playerState)
  const dispatch = useDispatch()

  // local states
  const [silderState, setSliderState] = useState(false)

  /**
   * Remove card in slot and emit to server
   */
  const rejectCard = () => {
    io.emit.reject(playerState.cardslot.id)
    dispatch(setCardslot(null))
    setCardPick(false)
  }

  /**
   * Nothing to reject, go out of pick card state
   */
  const doneCard = () => {
    io.emit.reject(-1)
    setCardPick(false)
  }

  /**
   * Helper function to generate the new hand
   * by removing a specific card
   * @param {Card} card the card to remove
   * @param {Array[Card]} hand the current hand
   */
  const removeFromHand = (card, hand) => {
    const newHand = []

    for(const c of hand) {
      if(c !== card) {
        newHand.push(c)
      }
    }

    return newHand
  }

  /**
   * Puts card in the slot (cardslot).
   * If the slot is not empty, it exchanges the
   * cards.
   * @param {Card} card the card to put in the slot
   */
  const putCardInSlot = card => {
    const newHand = removeFromHand(card, playerState.cards)
    const slotCard = playerState.cardslot

    // slot card is empty
    if(slotCard == null) {
      dispatch(setCardslot(card))
      dispatch(updateCards(newHand))
    } else {
      // if the slot is not empty
      newHand.push(slotCard)
      dispatch(setCardslot(card))
      dispatch(updateCards(newHand))
    }
  }

  /**
   * Handle drop of card in the target
   */
  const dropped = event => {
    const card = event.dragData
    // if the card is from the hand and is not persistant
    if(card !== playerState.cardslot && !card.persistant) {
      putCardInSlot(card)
    }
  }

  return (
    !silderState ?
    <div>
        <DropTarget
          targetKey='dropzone'
          onHit={dropped}
        >
          <div className={'cardslot has-text-centered ' + (playerState.cardslot == null ? 'empty-slot' : '')}>
              {playerState.cardslot != null ? <CardComp cardModel={playerState.cardslot} isMovable={true} /> : '+'}
          </div>
        </DropTarget>
        <section className='section'>
        <div className='has-text-centered card-selection-info'>
          {playerState.cardslot != null ? t('RejectInfo') : t('ContinueInfo')}
        </div>

        <div className='has-text-centered'>
          {
            playerState.cardslot != null ?
            <button 
              className='button is-danger is-medium'
              onClick={rejectCard}
            >
              {t('Reject')}
            </button>
            :
              <button 
              className='button is-primary is-medium'
              onClick={doneCard}
            >
              {t('Continue')}
            </button>
          }
          <button className='button is-info slider-button' onClick={() => setSliderState(true)}>
            <FontAwesomeIcon icon='users' />
          </button>
        </div>
      </section>
      <CardHand phase='selection' />
    </div>
    :
    <PlayerSlider setSliderState={setSliderState} />
  )
}

export default CardSelection
