// React
import React, {useEffect} from 'react'
import { useTranslation } from 'react-i18next'
// Network
import io from '../../../../net/socketAPI'
// Components
import ReactLoading from 'react-loading'
// CSS / icons
import './WaitingDisplay.scss'

/**
 * Component to display a waiting state to the user
 */
function WaitingDisplay() {

  useEffect(() => {
    io.emit.pass()
  }, [])

  // translations (i18n)
  const [t] = useTranslation('translation')

  return (
    <div>
      <section className='section' />
      <section className='section has-text-centered'>
        <div className='columns is-centered'>
          <div className='column is-two-thirds'>
            <h1 className='title is-1 has-text-light belisa'>
              {t('WaitingPlayers')}
            </h1>
          </div>
        </div>
      </section>
      <ReactLoading
        type='spinningBubbles'
        color='white'
        className='waiting-loading'
        width='10em'
        height='0em'
      />
    </div>
  )
}

export default WaitingDisplay
