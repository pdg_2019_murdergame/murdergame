// React
import React, { useState, useEffect } from 'react'
import { DropTarget } from 'react-drag-drop-container'
import { useSelector, useDispatch } from 'react-redux'
import { updateCards } from '../../../../redux'
import { useTranslation } from 'react-i18next'
// Network
import io from '../../../../net/socketAPI'
// Components
import { useToasts } from 'react-toast-notifications'
import CardHand from './CardHand'
import CabalVotingDisplay from './CabalVotingDisplay'
import WaitingDisplay from './WaitingDisplay'
import CardTargetDisplay from './CardTargetDisplay'
import PlayerSlider from './PlayerSlider'
// CSS / icons
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import './CardPlay.scss'

/**
 * Component displaying the interface to play the Cards
 * @param {bool} param0 true if the context is 'dawn' 
 */
function CardPlay({isDawn}) {

  // translations (i18n)
  const [t] = useTranslation('translation')

  // redux store
  const dispatch = useDispatch()
  const playerState = useSelector(state => state.playerState)
  const gameState = useSelector(state => state.gameState)

  // local states
  const [isActive, setIsActive] = useState(false)
  const [playMode, setPlayMode] = useState('') // 'vote', 'target', 'waiting', '' (default)
  const [playedCardId, setPlayedCardId] = useState(-1)
  const [sliderState, setSliderState] = useState(false)
  const {addToast} = useToasts()


  // reset play mode
  useEffect(() => {
    setPlayMode('')
  }, [gameState.gameState, setPlayMode])

  /**
   * Pass the card playing phase
   */
  const onPassHandler = () => {
     setPlayMode('waiting')
  }

  /**
   * Handle drop of card in the target
   */
  const dropped = event => {
    setIsActive(false)
    const card = event.dragData
    activateCard(card)
  }

  /**
   * Remove the card from the hand (2 copies if paired)
   * @param {Card} card the card to remove
   */
  const removeCardFromHand = card => {
    if(card.paired) {
      const newHand = []
      let pairRemoved = false
      for(const c of playerState.cards.filter(ca => ca !== card)) {
        if(c.id === card.id) {
          if(pairRemoved) {
            newHand.push(c)
          } else {
            pairRemoved = true
          }
        } else {
          newHand.push(c)
        }
      }
      dispatch(updateCards(newHand))
    } else {
      const newHand = playerState.cards.filter(c => c !== card)
      dispatch(updateCards(newHand))
    }
  }

  /**
   * Return the number of cards in the hand with the given id
   */
  const countCopies = cardId => {
    let count = 0
    for(const c of playerState.cards) {
      if(c.id === cardId) {
        count++
      }
    }
    return count
  }

  /**
   * Take the appropriate actions for the given used card
   * @param {Card} card the card to be used
   */
  const activateCard = card => {
    switch(card.id) {
      case 0:
        // broken card
        addToast(t('CardNoEffect'), {
          appearance: 'info'
        })
        io.emit.playcard(card.id)
        removeCardFromHand(card)
        break
      case 1:
        // prohecy card
        setPlayedCardId(card.id)
        setPlayMode('target')
        break
      case 2:
        // ankh card
        if(gameState.lastDeads.length === 0) {
          addToast(t('NoOneToSave'), {
            appearance: 'info'
          })
        } else {
          setPlayedCardId(card.id)
          setPlayMode('target')
        }
        break
      case 3:
        // death poison card
        setPlayedCardId(card.id)
        setPlayMode('target')
        break
      case 4:
        // heir card
        setPlayedCardId(card.id)
        setPlayMode('target')
        break
      case 5:
        // prayer card
        setPlayedCardId(card.id)
        setPlayMode('target')
        break
      case 6:
        setPlayMode('vote')
        break
      case 8:
        // burgle card
        setPlayedCardId(card.id)
        setPlayMode('target')
        break
      case 14:
        // hint Card
        if(countCopies(card.id) > 1) {
          setPlayedCardId(card.id)
          setPlayMode('target')
        } else {
          addToast(t('NeedTwoCopies'), {
            appearance: 'warning'
          })
        }
        break
      default:
        console.log('card unkown')
    }
  }

  switch(playMode) {
    case 'vote':
      return (
        <CabalVotingDisplay setPlayMode={setPlayMode} />
      )
    case 'target':
      return (
        <CardTargetDisplay setPlayMode={setPlayMode} cardId={playedCardId} />
      )
    case 'waiting':
      return (
        <WaitingDisplay />
      )
    default:
      return (
        !sliderState ?
        <div>
            <DropTarget
              targetKey='dropzone'
              onHit={dropped}
              onDragEnter={() => setIsActive(true)}
              onDragLeave={() => setIsActive(false)}
            >
            <div className={'playslot has-text-centered ' + (isActive ? 'playslot-active' : '')}>
              {t('PlaySlotInfo')}
            </div>
          </DropTarget>
          <section className='section has-text-centered'>
            <button 
              className='button is-danger is-medium'
              onClick={() => onPassHandler()}
            >
              {t('Pass')}
            </button>
            <button className='button is-info slider-button' onClick={() => setSliderState(true)}>
            <FontAwesomeIcon icon='users' />
          </button>
          </section>
          <CardHand phase={isDawn ? 'dawn' : 'night'} />
        </div>
        : <PlayerSlider setSliderState={setSliderState} />
      )
  }
}

export default CardPlay
