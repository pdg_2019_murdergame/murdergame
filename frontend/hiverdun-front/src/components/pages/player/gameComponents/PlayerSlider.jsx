// React
import React from 'react'
import { useSelector } from 'react-redux'
// CSS / icons
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import './PlayerSlider.scss'

function PlayerSlider({setSliderState}) {
  
  // redux states
  const gameState = useSelector(state => state.gameState)

  /**
   * Render a line with player info
   * @param {player} p the player 
   * @param {number} i index
   */
  const infoLine = (p, i) => {
    return (
      <div key={i} className='panel-block'>
        <span className={p.team ? 'team-color-' + p.team: 'team-color-unknown'}>
          <FontAwesomeIcon 
            icon={p.alive ? 'circle' : 'skull'}
            className={'icon is-large'} 
          />
        </span>
        <span className={'player-name-color player-name-slider'}>
          {p.nickname}
        </span>
        <span className={p.online ? 'circle-online player-online-status' : 'circle-offline player-online-status'}>
          <FontAwesomeIcon 
            icon='circle'
            className={'icon is-right'}
          />
        </span>
      </div>
    )
  }

  return (
    <div>
      <section className='section has-text-centered'>
        <button
          className='button is-medium is-danger fa-rotate-180 close-slider-button'
          onClick={() => setSliderState(false)}
        >
          <FontAwesomeIcon icon='sign-out-alt' />
        </button>
      </section>
      <section className='section'>
        <div className='columns is-centered'>
          <div className='column is-half'>
            <nav className='panel'>
              {gameState.players.map((p, i) => infoLine(p, i))}
            </nav>
          </div>
        </div>
      </section>
    </div>
  )
}

export default PlayerSlider
