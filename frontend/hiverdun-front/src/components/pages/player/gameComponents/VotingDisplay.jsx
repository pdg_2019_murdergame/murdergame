// React
import React, { useState } from 'react'
import { useSelector } from 'react-redux'
import { useTranslation } from 'react-i18next'
// Network
import io from '../../../../net/socketAPI'
// CSS / icons
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

/**
 * Voting Component displaying the list of players (Village Vote)
 */
function VotingDisplay() {

  // translations (i18n)
  const [t] = useTranslation('translation')

  // redux states
  const gameState = useSelector(state => state.gameState)
  const playerState = useSelector(state => state.playerState)

  // local states
  const [target, setTarget] = useState('')

  /**
   * Emit a vote
   * @param {string} target the nickname of the targeted player
   */
  const vote = target => {
    setTarget(target)
    io.emit.vote(target)
  }

  /**
   * Render a voting line
   * @param {player} p the player
   * @param {number} i index 
   */
  const playerLine = (p, i) => {
    return (
      <div key={i} className='panel-block'
      onClick={p.alive ? () => vote(p.nickname) : () => {}}
      >
        <span className={p.team ? 'team-color-' + p.team: 'team-color-unknown'}>
          <FontAwesomeIcon 
            icon={target === p.nickname ? 'crosshairs' : 'circle'}
            className={'icon is-large'} 
          />
        </span>
        <span className={'player-name-slider player-name-color ' + (p.alive ? 'name-hoverable' : '')}>
          {p.nickname}
        </span>
        <span className={p.online ? 'circle-online player-online-status' : 'circle-offline player-online-status'}>
          <FontAwesomeIcon 
            icon='circle'
            className={'icon is-right'}
          />
        </span>
      </div>
    )
  }

  return (
    <div>
      <section className='section has-text-centered'>
      </section>
      <div className='columns is-centered'>
        <div className='column is-half'>
          <h2 className='title is-3 has-text-centered has-text-white'>
            {t('VoteInfo')}
          </h2>
          <nav className='panel'>
            <div
              className='panel-block has-text-white'
              onClick={() => vote('')}
            >
              <span className='team-color-unknown'>
              <FontAwesomeIcon 
                icon={target === '' ? 'crosshairs' : 'peace'}
                className={'icon is-large'} 
              />
              </span>
              <span className={'player-name-slider name-hoverable'}>
                {t('BlankVote')}
              </span>
            </div>
            {
              // list of players that are alive (excluding self)
              gameState.players
                .filter(p => (p.nickname !== playerState.nickname) && p.alive)
                .map((p, i) => playerLine(p, i))
            }
          </nav>
          <div className='has-text-centered'>
            <span className='title is-3 has-text-danger'>{t('DeadPlayers')} </span>
            <FontAwesomeIcon icon='skull' className='has-text-danger fa-2x' />
          </div>
          <nav className='panel'>
            {
              // list of dead players
              gameState.players
                .filter(p => (p.nickname !== playerState.nickname) && !p.alive)
                .map((p, i) => playerLine(p, i))
            }
          </nav>
        </div>
      </div>
    </div>
  )
}

export default VotingDisplay
