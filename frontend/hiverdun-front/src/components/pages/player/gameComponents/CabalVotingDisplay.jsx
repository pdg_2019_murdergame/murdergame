// React
import React, { useState } from 'react'
import { useSelector } from 'react-redux'
import { useTranslation } from 'react-i18next'
// Network
import io from '../../../../net/socketAPI'
// CSS / icons
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import './CabalVotingDisplay.scss'

/**
 * Voting Component displaying the list of players (Cabal Vote)
 * @param {Function} param0 callback to set the playMode in parent component
 */
function CabalVotingDisplay({setPlayMode}) {

  // translations (i18n)
  const [t] = useTranslation('translation')

  // redux states
  const gameState = useSelector(state => state.gameState)
  const playerState = useSelector(state => state.playerState)

  // local states
  const [cabalTarget, setCabalTarget] = useState('') // set current target

  /**
   * Emit Vote to server
   * @param {string} nickname 
   */
  const vote = nickname => {
    setCabalTarget(nickname)
    io.emit.votecabal(nickname)
  }

  /**
   * Render a citizen line (targetable player)
   * @param {player} p
   * @param {number} i index
   * @param {number} count the current number of votes on this target
   */
  const voteLine = (p, i, count) => {
    return (
      <div key={i} className='panel-block' onClick={() => {vote(p.nickname)}}>
        <span className={p.team ? 'team-color-' + p.team: 'team-color-unknown'}>
          <FontAwesomeIcon 
            icon={'circle'}
            className={'icon is-large'} 
          />
        </span>
        <span className={'player-name-slider name-hoverable ' + (cabalTarget === p.nickname ? 'has-text-danger' : 'player-name-color')}>
          {p.nickname}
        </span>
          {voteCount(count)}
        <span className={p.online ? 'circle-online player-online-status' : 'circle-offline player-online-status'}>
          <FontAwesomeIcon 
            icon='circle'
            className={'icon is-right'}
          />
        </span>
      </div>
    )
  }

  /**
   * Render a cabal line (non-targetable player)
   * @param {*} p 
   * @param {*} i 
   * @param {string} target the nickname of the player targeted by this player
   */
  const cabalLine = (p, i, target) => {
    return (
      <div key={i} className='panel-block'>
        <span className='team-color-cabal'>
          <FontAwesomeIcon 
            icon='circle'
            className={'icon is-large'} 
          />
        </span>
        <span className={'player-name-color cabal-name-voteline'}>
          {p.nickname}
        </span>
        {
          target ?
            <span className='has-text-danger'>
            <FontAwesomeIcon 
              icon='crosshairs'
            />
            </span>
          : ''
        }
        <span className={'player-name-color cabal-target-voteline'}>
          {target}
        </span>
        <span className={'circle-online player-online-status'}>
          <FontAwesomeIcon 
            icon='circle'
            className={'icon is-right'}
          />
        </span>
      </div>
    )
  }

  /**
   * Renders and array of 'count' icons (skull)
   * @param {number} count the number of ions to render 
   */
  const voteCount = count => {
    const voteIcons = []
    for(let i = 0; i < count; i++) {
      voteIcons.push(
        <FontAwesomeIcon
          key={i}
          icon='skull'
          className={'has-text-danger target-icon'}
        />
      )
    }
    return voteIcons
  }

  /**
   * Returns the target's nickname for a specific (cabal) player
   * @param {player} p the player (cabal) to get the target from
   */
  const getPlayerTarget = p => {
    const l = gameState.cabalVotes.filter(v => v.player === p.nickname)
    if(l.length === 1) {
      return l[0].target
    } else {
      return ''
    }
  }

  return (
    <div>
      <section className='section has-text-centered'>
        <button
          className='button is-medium is-primary'
          onClick={
            () => {
              // player is ready for next state (emit 'pass')
              io.emit.pass()
              setPlayMode('waiting')
            }
          }
        >
        {
          cabalTarget !== '' ? t('ConfirmVote') : t('Pass')
        }
        </button>
      </section>
      <div className='columns is-centered'>
        <div className='column is-half'>
         <nav className='panel'>
            {
              // array of the other cabal members (alive and online) with there current target
              gameState.players
                .filter(p => p.team === 'cabal' && p.nickname !== playerState.nickname && p.alive && p.online)
                .map((p, i) => cabalLine(p, i, getPlayerTarget(p)))
            }
          </nav>
          <nav className='panel'>
            {
              // array of citizens (non-cabal members) with number of votes on him
              gameState.players
                .filter(p => p.team !== 'cabal' && p.alive)
                .map((p, i) => voteLine(p, i, gameState.cabalVotes.filter(v => v.target === p.nickname).length))
            }
          </nav>
        </div>
      </div>
    </div>
  )
}

export default CabalVotingDisplay