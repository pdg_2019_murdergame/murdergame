// React
import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { setCardslot, updateCards } from '../../../../redux'
import { useTranslation } from 'react-i18next'
// Components
import CardComp from '../../../CardComp'
import { DropTarget } from 'react-drag-drop-container'
// CSS / icons
import '../../../../_sass/card.scss'

/**
 * Component displaying the player Cards
 * @param {string} param0 phase : usage context ('seletion', 'night', 'dawn') 
 */
function CardHand({phase}) {

  // translation
  const [t] = useTranslation('translation')

  // redux states
  const playerState = useSelector(state => state.playerState)
  const dispatch = useDispatch()

  /**
   * Puts the dropped card in the player's hand (set of cards).
   * If the card is from the cardslot and the hand is full, replace the first
   * card of the hand that is not persistant. If there is none, does nothing.
   * @param {Card} card the card dropped in the hand
   */
  const putCardInHand = card => {
    const newHand = []
    let newSlotCard

    for(const c of playerState.cards) {
      newHand.push(c)
    }
    
    if(newHand.length < 3) {
      newHand.push(card)
      dispatch(setCardslot(null))
      dispatch(updateCards(newHand))
    } else {
      for(let i = 0; i < newHand.length; i++) {
        if(!newHand[i].persistant) {
          newSlotCard = newHand[i]
          newHand[i] = card
          dispatch(setCardslot(newSlotCard))
          dispatch(updateCards(newHand))
          return
        }
      }
    }
  }

  /**
   * Handle drop of card in the target
   */
  const dropped = event => {
    const card = event.dragData
    if(!playerState.cards.includes(card)) {
      putCardInHand(card)
    }
  }

  /**
   * Determine if the card is movable
   * @param {Card} card 
   */
  const getMovability = card => {
    if(card.passive) {
      return phase === 'selection'
    } else {
      switch(phase) {
        case 'selection':
          return !card.persistant
        case 'night':
          return !card.dawn
        case 'dawn':
          return card.dawn
        default:
          return false
      }
    }
  }

  return (
    <DropTarget 
      targetKey='dropzone'
      onHit={dropped}
    >
      <div id="game-card-hand" className={playerState.cards.length === 0 ? 'empty-card-hand' : ''}>
          {
            playerState.cards.length > 0 ?
            playerState.cards.map((c, i) => <CardComp key={i} cardModel={c} isMovable={getMovability(c)} />)
            : t('EmptyHandInfo')
          }
      </div>
    </DropTarget>
  )
}

export default CardHand
