export { default as JoinLobbyPage } from './JoinLobbyPage'
export { default as LobbyPage } from './LobbyPage'
export { default as GamePage } from './GamePage'
export { default as PlayerEndGamePage } from './PlayerEndGamePage'