// React
import React from 'react'
import { Redirect } from 'react-router-dom'
import { useSelector } from 'react-redux'
import { useTranslation } from 'react-i18next'
// CSS / icons
import './PlayerEndGamePage.scss'

/**
 * Component (page) rendered when the game is over (and player is alive)
 */
function PlayerEndGamePage() {

  // translations (i18n)
  const [t] = useTranslation('translation')

  // redux states
  const connection = useSelector(state => state.connection)
  const playerState = useSelector(state => state.playerState)
  const gameState = useSelector(state => state.gameState)

  // socket is not initialized, redirect
  if(connection.socket === null) {
    return <Redirect to='/menu' />
  }

  const renderTitle = () => {

    if(gameState.winner === 'equality' || gameState.winner === '') {
      return (
        <h1 className='title-gameover has-text-primary belisa'>
          {t('DrawTitle')}
        </h1>
      )
    }

    if(gameState.winner === playerState.team) {
      return (
        <h1 className='title-gameover has-text-primary belisa'>
          {t('YouWon')}
        </h1>
      )
    } else {
      return (
        <h1 className='title-gameover has-text-danger belisa'>
          {t('YouLost')}
        </h1>
      )
    }
  }

  const renderText = () => {
    if(gameState.winner === 'equality' || gameState.winner === '') {
      return (
        <h2 className='has-text-primary'>
          {t('DrawText')}
        </h2>
      )
    }

    if(gameState.winner === playerState.team) {
      if(playerState.team === 'cabal') {
        return (
          <h2 className='has-text-danger'>
            {t('CabalWonText')}
          </h2>
        )
      } else {
        return (
          <h2 className='has-text-success'>
            {t('CitizenWonText')}
          </h2>
        )
      }
    } else {
      if(playerState.team === 'cabal') {
        return (
          <h2 className='has-text-danger'>
            {t('CabalLostText')}
          </h2>
        )
      } else {
        return (
          <h2 className='has-text-danger'>
            {t('CitizenLostText')}
          </h2>
        )
      }
    }
  }

  return (
    <div>
      <section className='section' style={{padding: 0, marginTop: '2em', marginBottom: '1em'}}>
        <div className='columns is-centered'>
          <div className='column is-half has-text-centered'>
              {renderTitle()}
            <div className='text-gameover belisa'>
              {renderText()}
            </div>
          </div>
        </div>
      </section>
    </div>
  )
}

export default PlayerEndGamePage
