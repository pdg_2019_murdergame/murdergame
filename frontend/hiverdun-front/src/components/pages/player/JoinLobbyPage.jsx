// React
import React, { useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { joinParty } from '../../../redux'
import { Redirect } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
// Components
import BurgerMenu from '../../BurgerMenu'
import ServerStatus from '../../ServerStatus'
import LangButton from '../../LangButton'
import About from '../../About'
// CSS / icons
import burgerIcon from '../../../assets/burger-menu/menu.svg'
import burgerCloseIcon from '../../../assets/burger-menu/closing_eye.svg'
import './JoinLobbyPage.scss'

/**
 * Component (page) that renders the interface to join a game (as player)
 */
function JoinLobbyPage() {

  // translation (i18n)
  const [t] = useTranslation('translation') // i18n hook

  // redux states
  const connState = useSelector(state => state.connection)
  const dispatch = useDispatch()

  // local states
  const [nickname, setNickname] = useState('')   // user name input
  const [partyCode, setPartyCode] = useState('') // user pary code input
  const [errorInfo, setErrorInfo] = useState('')

  /**
   * Check user inputs
   */
  const lobbyEntryHandler = () => {
    
    // input check
    // clear error info
    setErrorInfo('')

    if(nickname === '' || partyCode === '') {
      setErrorInfo(t('ErrorEmptyInputs'))
      return
    }

    if(nickname.length < 3 || nickname.length > 16) {
      setErrorInfo(t('ErrorNickNameLength'))
      return
    }

    if(partyCode.length !== 6) {
      setErrorInfo(t('ErrorInvalidCode'))
      return
    }

    // fetch jwt token and connect to socket (join party)
    dispatch(joinParty(nickname, partyCode))
  }
  
  // redirection in case their is already a connection enabled
  if(connState.socket !== null) {
    return <Redirect to='/player/lobby' />
  }

  return (
    <div id="outer-container">
      <BurgerMenu
        className="is-mobile"
        width={280}
        pageWrapId={"page-wrap"}
        outerContainerId={"outer-container"}
        customBurgerIcon={<img src={burgerIcon} alt="burger icon" />}
        customCrossIcon={<img src={burgerCloseIcon} alt="burger close icon" />}
        disableAutoFocus
      >
        <ServerStatus itemId="server-status" />
        <LangButton itemId="menu-lang" />
        <About itemId="menu-about" />
      </BurgerMenu>

      <main id="page-wrap">
        <section className="section" />
        <section className="section">
          <div className="columns is-centered is-vcentered is-mobile">
            <div className="column is-narrow">
              <div className="field">
                <label className="label has-text-light">{t('Nickname')}</label>
                <div className="control">
                  <input
                    className="input is-large is-focused"
                    type="text"
                    placeholder={t('NicknameHint')}
                    value={nickname}
                    maxLength='16'
                    onChange={e => setNickname(e.target.value)}
                  />
                  <p className="help has-text-light">{t('NicknameInfo')}</p>
                </div>
              </div>

              <div className="field">
                <label className="label has-text-light">{t('PartyCode')}</label>
                <div className="control">
                  <input
                    className="input is-large"
                    type="text"
                    placeholder={t('PartyCodeHint')}
                    value={partyCode}
                    maxLength='6'
                    onChange={e => setPartyCode(e.target.value.toUpperCase())}
                  />
                  <p className="help has-text-light">
                    {t('PartyCodeInfo')}
                  </p>
                </div>
              </div>

              <div className="field">
                <div className="control has-text-centered">
                  <button 
                    className={"button is-primary is-medium " + (connState.loading ? 'is-loading' : '')}
                    onClick={lobbyEntryHandler}>
                    {t('EnterLobby')}
                  </button>
                </div>
              </div>

              <div className="has-text-centered has-text-danger">
                {errorInfo}
              </div>

              <div className="has-text-centered has-text-danger">
                {connState.error}
              </div>
            </div>
          </div>
        </section>
      </main>
    </div>
  )
}

export default JoinLobbyPage
