// React
import React, { useEffect } from 'react'
import {useSelector} from 'react-redux'
import { useTranslation } from 'react-i18next'
import { Redirect } from 'react-router-dom'
// Network
import io from '../../../net/socketAPI'
// Components
import VoteResultDisplay from './gameComponents/VoteResultDisplay'
// CSS / icons
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import houseImg from '../../../assets/house_main.svg'
import './VillagePage.scss'

/**
 * Main Game Component wrapping the master's content
 */
function VillagePage() {
  
  // translations (i18n)
  const [t] = useTranslation('translation')

  // redux states
  const gameState = useSelector(state => state.gameState)
  const masterState = useSelector(state => state.masterState)
  const connection = useSelector(state => state.connection)

  // update master's information at every game state change
  useEffect(() => {

    // set winner on endgame state
    if(gameState.gameState.includes('End')) {
      io.emit.askWinner()
    }

    io.emit.me()
  }, [gameState.gameState])

  // socket not initialized, redirect
  if(connection.socket === null) {
    return <Redirect to='/menu' />
  }

  // STATE : Night
  if(gameState.gameState.includes('Night')) {
    return (
      <div className='night-background'>
        <section className='section has-text-centered'>
          <div className='columns is-centered'>
            <div className='column is-two-thirds'>
              <h1 id='night-title' className='title is-1 belisa'>
                {t('TitleNight') + ' Hiverdun'}
              </h1>
              <section className='fa-layers'>
                <FontAwesomeIcon icon='moon' id='night-moon' />
                <FontAwesomeIcon icon='cloud' id='night-cloud'/> 
              </section>
              <section className='section night-text'>
                <h2 className='title is-1 belisa has-text-danger'>
                  {t('TextNight')}
                </h2>
              </section>
            </div>
          </div>
        </section>
      </div>
    )
  }

  // STATE : Dawn
  if(gameState.gameState.includes('Dawn')) {
    return (
      <div className='dawn-background'>
        <section className='section has-text-centered'>
          <div className='columns is-centered'>
            <div className='column is-two-thirds'>
              <h1 id='dawn-title' className='title is-1 belisa'>
                {t('TitleDawn')}
              </h1>
              <section className='fa-layers'>
                <FontAwesomeIcon icon='circle' id='dawn-sun' />
              </section>
            </div>
          </div>
        </section>
      </div>
    )
  }

  // STATE : Day
  if(gameState.gameState.includes('Day')) {
    return (
      <div className='day-background'>
        <section className='section'>
          <FontAwesomeIcon icon='circle' id='day-sun' className=''
          />
        </section>
        <section className='section'>
          <div className='columns is-centered'>
            <div className='column is-one-third has-text-centered'>
              {
                masterState.guilty === '' ?
                  <h1 className='title is-1 has-text-white'>
                    {t('VoteUndecided')}
                  </h1>
                :
                  <h1 className='title is-1 has-text-white'>
                    <span className='has-text-danger'>{masterState.guilty}</span> {t('IsGuilty')}
                  </h1>
              }
              <button 
                className={'button ' + (masterState.guilty === '' ? 'is-primary' : 'is-danger')}
                onClick={() => io.emit.verdict()}
              >
                {masterState.guilty === '' ? t('PassDay') : t('Execute')}
              </button>
              {
                gameState.lastDeads.length !== 0 ?
                <div>
                  <h2 id='death-anounce-title' className='title is-3 has-text-danger'>
                    {t('NotWakeUp')}
                  </h2>
                  {
                    gameState.lastDeads.map((p, i) => <h2 key={i} className={'title is-4 ' + (p.team === 'cabal' ? 'has-text-danger' : 'has-text-success')}>{p.nickname}</h2>)
                  }
                </div>
                : ''
              }
            </div>
            <div className='column is-one-third'>
              <VoteResultDisplay />
            </div>
          </div>
        </section>
        <figure className="image">
          <div id="barrier-day"></div>
        </figure>
          <div className="content has-text-centered">
          <figure className="image">
            <img id="house-day" src={houseImg} alt="house-day" />
          </figure>
        </div>
      </div>
    )
  }

  // STATE : End
  if(gameState.gameState === 'EndState') {
    return <Redirect to='/master/end' />
  }

  return (
    <div>
      Are you lost my child ?
    </div>
  )
}

export default VillagePage
