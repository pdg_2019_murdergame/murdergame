// React
import React, { useEffect, useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { updateGuilty } from '../../../../redux'
// CSS / icons
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import './VoteResultDisplay.scss'

/**
 * Renders the visual display of the day votation
 */
function VoteResultDisplay() {

  // redux states
  const gameState = useSelector(state => state.gameState)
  const masterState = useSelector(state => state.masterState)
  const dispatch = useDispatch()

  // local states
  const [displayedList, setDisplayedList] = useState([])

  /**
   * Renders a player line with the number of votes against him
   * @param {player} p the player
   * @param {index} i 
   * @param {number} votes the number of votes
   * @param {string} target the player's target
   */
  const playerLine = (p, i, votes, target) => {
    return (
      <div key={i} className='panel-block'>
        <span className={votes > 0 ? 'has-text-danger' : ''}>
          {votes}
        </span>
        <span className={'player-name-color name-voteline'}>
          {p.nickname}
        </span>
        {
          target ?
            <span className='has-text-danger'>
            <FontAwesomeIcon 
              icon='crosshairs'
            />
            </span>
          : ''
        }
        <span className={'player-name-color vote-target'}>
          {target}
        </span>
        <span className={p.online ? 'circle-online player-online-status' : 'circle-offline player-online-status'}>
          <FontAwesomeIcon 
            icon='circle'
            className={'icon is-right'}
          />
        </span>
      </div>
    )
  }

  useEffect(() => {

  /**
   * Returns the number of votes against the specified player
   * @param {player} player 
   */
  const getVoteCount = player => {
    return masterState.votes.filter(vote => vote.target === player.nickname).length
  }

  /**
   * Returns the target of the player
   * @param {player} player 
   */
  const getTarget = player => {
    const l = masterState.votes.filter(v => v.player === player.nickname)
    if(l.length === 1) {
      return l[0].target
    } else {
      return ''
    }
  }

  /**
   * Returns the new Guild player or an empty string if there is none
   * @param {Array[{player, number}]} playersWithVotes 
   */
  const getGuilty = playersWithVotes => {
    if(playersWithVotes[0].count > playersWithVotes[1].count) {
      return playersWithVotes[0].p.nickname
    } else {
      return ''
    }
  }

  const l = gameState.players
              .filter(p => p.alive)
              .map(p => {return {p, count: getVoteCount(p)}})
              .sort((e1, e2) => e2.count - e1.count)

    // player lines array (jsx) with vote count on them and target, sorted by vote count 
    setDisplayedList(l.map((e, i) => playerLine(e.p, i, e.count, getTarget(e.p))))
    dispatch(updateGuilty(getGuilty(l)))
  }, [masterState.votes, gameState.players, dispatch])

  return (
    <div>
      <nav className='panel'>
          {displayedList}
        </nav>
    </div>
  )
}

export default VoteResultDisplay
