// React
import React from 'react'
import { useSelector } from 'react-redux'
import { useTranslation } from 'react-i18next'
import { Redirect } from 'react-router-dom'
// Network
import io from '../../../net/socketAPI'
import './IntroPage.scss'

/**
 * Component (page) displayed at for the game intro
 */
function IntroPage() {

  // translations (i18n)
  const [t] = useTranslation('translation') // i18n hook

  // redux states
  const gameState = useSelector(state => state.gameState)
  const connection = useSelector(state => state.connection)

  // send socket event to get in the first night state
  const skipStory = () => {
    io.emit.skip()
  }

  // socket is not initialized, redirect
  if(connection.socket === null) {
    return <Redirect to='/menu' />
  }

  // redirect on state change
  if(gameState.gameState === 'FirstNightState') {
    return <Redirect to='/master/village' />
  }

  return (
    <div>
      <section className='section'>
        <div className='columns is-centered'>
          <div className='column is-two-thirds belisa'>
            <div className='intro-text'>
              {t('InHiverdun')} <span id='intro-hiverdun-text'>Hiverdun</span>,<br/>
              {t('IntroText')} <span className='has-text-danger'>{t('Cabal')}</span> ?
            </div>
          </div>
        </div>
      </section>
      <section className='section footer'>
        <button
          id='skip-intro-button'
          className='button is-primary is-medium'
          onClick={skipStory}
        >
          {t('Skip')}
        </button>
      </section>
    </div>
  )
}

export default IntroPage
