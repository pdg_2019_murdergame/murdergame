// React
import React from 'react'
import { Redirect } from 'react-router-dom'
import {useSelector} from 'react-redux'
import { useTranslation } from 'react-i18next'
// CSS / icons
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import './EndGamePage.scss'

/**
 * Component (page) displayed when the game is over
 */
function EndGamePage() {
  const [t] = useTranslation('translation')

  // redux states
  const connection = useSelector(state => state.connection)
  const gameState = useSelector(state => state.gameState)

  // socket no initialized, redirect
  if(connection.socket === null) {
    return <Redirect to='/menu' />
  }

   /**
   * Render a line with player info
   * @param {player} p the player 
   * @param {number} i index
   */
 const infoLine = (p, i) => {
  return (
    <div key={i} className='panel-block'>
      <span className={p.team ? 'team-color-' + p.team: 'team-color-unknown'}>
        <FontAwesomeIcon 
          icon={p.alive ? 'circle' : 'skull'}
          className={'icon is-large'} 
        />
      </span>
      <span className={'player-name-color player-name-slider'}>
        {p.nickname}
      </span>
      <span className={p.online ? 'circle-online player-online-status' : 'circle-offline player-online-status'}>
        <FontAwesomeIcon 
          icon='circle'
          className={'icon is-right'}
        />
      </span>
    </div>
  )
}

const renderTitle = () => {

  if(gameState.winner === 'equality' || gameState.winner === '') {
    return t('DrawTitle')
  }

  if(gameState.winner === 'cabal') {
    return t('CabalWonTitle')
  }

  return t('CitizenWonTitle')
}
  
  return (
    <div>
      <section className='section' style={{padding: 0, marginTop: '2em', marginBottom: '1em'}}>
        <div className='columns is-centered'>
          <div className='column is-half has-text-centered'>
            <h1 className={'title-winner belisa ' + (gameState.winner === 'citizen' ? 'has-text-success' : 'has-text-danger')} >
              {renderTitle()}
            </h1>
            <nav className='panel'>
              {
                gameState.players
                  .sort((p1, p2) => p2.alive - p1.alive)
                  .map((p, i) => infoLine(p, i))
              }
            </nav>
          </div>
        </div>
      </section>
    </div>
  )
}

export default EndGamePage
