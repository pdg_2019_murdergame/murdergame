// React
import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Redirect } from 'react-router-dom'
import { createParty, connectSocketMsg, disconnectSocket, clearGameState } from '../../../redux'
import { action as toggleMenu } from 'redux-burger-menu'
import { useTranslation } from 'react-i18next'
import ReactLoading from 'react-loading'
import { useToasts } from 'react-toast-notifications'
// Network
import io from '../../../net/socketAPI'
// Components
import BurgerMenu from '../../BurgerMenu'
import ServerStatus from '../../ServerStatus'
import LangButton from '../../LangButton'
import About from '../../About'
// CSS / icons
import burgerIcon from '../../../assets/burger-menu/menu.svg'
import burgerCloseIcon from '../../../assets/burger-menu/closing_eye.svg'
import ribbonIcon from '../../../assets/ribbon.svg'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import './LobbyCreationPage.scss'

/**
 * Component (page) rendered where creating a new game
 */
function LobbyCreationPage() {

  // translation (i18n)
  const [t] = useTranslation('translation')

  // redux states
  const dispatch = useDispatch()
  const gameState = useSelector(state => state.gameState)
  const connState = useSelector(state => state.connection)
  
  // local states
  const [playersList, setPlayersList] = useState([])
  const [cancel, setCancel] = useState(false)
  const { addToast } = useToasts()
  const minPlayers = 4 // min will be 8 in production
  const maxPlayers = 18

  // on component load, create game
  useEffect(() => {
    if(connState.socket == null && !cancel) {
      // create new party
      dispatch(createParty())
    }
  }, [connState.socket, dispatch, cancel])

  // update player icons display
  useEffect(() => {
    updatePlayerList(gameState.numberOfPlayers, maxPlayers)
  }, [gameState.numberOfPlayers])

  // show connections/disconnections toasts
  useEffect(() => {
    if(connState.serverMessage !== '') {
      addToast(connState.serverMessage, {
        appearance: 'info'
      })
    }
    dispatch(connectSocketMsg(''))
  }, [connState.serverMessage, addToast, dispatch])

  /**
   * Generate the new list of player icons
   * @param {number} numberOfPlayers 
   * @param {number} maxPlayers 
   */
  const updatePlayerList = (numberOfPlayers, maxPlayers) => {
    const players = []
    // connected players
    for(let i = 0; i < numberOfPlayers; i++) {
      players.push(
        <div key={i} className="column has-text-centered ">
          <FontAwesomeIcon icon="user-circle" className="circle has-text-primary" />
        </div>
      )
    }

    // empty player slots
    for(let i = numberOfPlayers; i < maxPlayers; i++) {
      players.push(
        <div key={i} className="column has-text-centered ">
          <FontAwesomeIcon icon="user-circle" className="circle has-text-white" />
        </div>
      )
    }

    setPlayersList(players)
  }

  /**
   * notify server to start the game
   */
  const startGame = () => {
    io.emit.start()
  }

  /**
   * notify server to cancel the game creation
   */
  const cancelGame = () => {
    setCancel(true)
    io.disconnect()
    dispatch(clearGameState())
    dispatch(disconnectSocket())
  }

  // redirection in case of game creation error
  if(connState.error !== '') {
    dispatch(toggleMenu(true))
    return <Redirect to='/menu' />
  }

  // redirection in case of game creation canceled
  if(cancel) {
    return <Redirect to='/menu' />
  }

  // redirection if the user is a player
  if(connState.role === 'player') {
    return <Redirect to='/player' />
  }

  // redirection to intro page
  if(gameState.gameState === 'StoryState') {
    return <Redirect to='/master/intro' />
  }

  return (
    <div id="outer-container">
      <BurgerMenu
        className="is-mobile"
        width={280}
        pageWrapId={"page-wrap"}
        outerContainerId={"outer-container"}
        customBurgerIcon={<img src={burgerIcon} alt="burger icon" />}
        customCrossIcon={<img src={burgerCloseIcon} alt="burger close icon" />}
        disableAutoFocus
      >
        <ServerStatus itemId="server-status" />
        <LangButton itemId="menu-lang" />
        <About itemId="menu-about" />
      </BurgerMenu>
      <main id="page-wrap">
        <section className="section" />
        <section className="section is-medium has-text-centered">
          <h1 className="belisa is-size-1">{t('PartyCode')}</h1>
          <figure className="image">
            <img id="ribbon" src={ribbonIcon} alt="ribbon" />
            {
              connState.loading
              ? <ReactLoading type='bubbles' color='#ed4c67' className="loading" width='7em' height='0em'/>
              : <h2 id="ribbon-text" className="has-text-danger is-size-3">{gameState.partyCode}</h2>
            }
          </figure>
        </section>

        <section className="section">
          <div className="columns">
            <div className="column is-one-third is-offset-one-third">
              <div className="columns is-centered is-multiline is-mobile">
                {playersList}
              </div>
            </div>
          </div>
          <div className="container has-text-centered">
            <span className={
              gameState.numberOfPlayers < minPlayers
              ? "has-text-danger"
              : "has-text-primary"
            }>
              {gameState.numberOfPlayers}
            </span>
            /{`${maxPlayers} ` + t('PlayersInLobby')}
          </div>
        </section>
        <div className="container">
          <div className="field">
            <div className="control has-text-centered">
              <button 
                className={gameState.numberOfPlayers < minPlayers ? "button is-danger is-medium" : "button is-primary is-medium"}
                disabled={gameState.numberOfPlayers < minPlayers}
                onClick={startGame}
              >
                {
                  gameState.numberOfPlayers < minPlayers
                  ? t('NeedMorePlayers')
                  : t('Start') 
                }
              </button>
              <button
                id='cancel-button'
                className={"button is-danger is-medium"}
                onClick={cancelGame}
              >
                {t('Cancel')}
              </button>
            </div>
          </div>
        </div>
      </main>
    </div>
  )
}

export default LobbyCreationPage
