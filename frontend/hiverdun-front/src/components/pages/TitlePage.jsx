// React
import React from 'react'
import { Link } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
// Components
import LangButton from '../LangButton'
import ServerStatus from '../ServerStatus'
// CSS / icons
import './TitlePage.scss'

/**
 * Component (page) to render the Title page (index page)
 */
function TitlePage() {

  // translations (i18n)
  const [t] = useTranslation('translation')

  return (
    <div>
      <section className='section' >
        <div className='container has-text-centered'>
          <h1 id='title-shadow' className='belisa'>Shadows of</h1>
          <h2 id='title-hiverdun' className='title belisa'>Hiverdun</h2>
          <Link to='/menu'>
            <button className='button is-primary is-size-2 belisa'>
              {t('Play')}
            </button>
          </Link>
          <ServerStatus itemId='title-server-status' />
          <LangButton itemId='title-lang-button' />
        </div>
      </section>
    </div>
  )
}

export default TitlePage