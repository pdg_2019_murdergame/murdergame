// React
import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { fetchServerState } from '../redux'
import { useTranslation } from 'react-i18next'

/**
 * Component to display the server status
 */
function ServerStatus({itemId}) {

  // translations (i18n)
  const [t] = useTranslation('translation')

  // redux states
  const serverState = useSelector(state => state.serverState)
  const dispatch = useDispatch()

  // get server status
  useEffect(() => {
    dispatch(fetchServerState())
  }, [dispatch])

  return (
    <div id={itemId} className="belisa bm-item">
      <h2>{t('ServerStatus')}
      {
        serverState.loading ?
          <span style={{ color: '#330066', paddingLeft: '0.2em'}}>
            {t('Checking') + "..."}
          </span>
        : (serverState.alive ?
          <span className='has-text-primary' style={{paddingLeft: '0.2em'}}>
            {t('Alive')}
          </span>
        : <span className='has-text-danger' style={{paddingLeft: '0.2em'}}>
            {t('Dead')}
          </span>
      )}
      </h2>
    </div>
  )
}

export default ServerStatus
