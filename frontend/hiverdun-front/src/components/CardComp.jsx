// React
import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
// Components
import { DragDropContainer } from 'react-drag-drop-container'
// CSS / icons
import '../_sass/card.scss'

/**
 * Component wraping a Card model
 * @param {{Card, bool}} param0 the Card model and a boolean to set if the Card is draggable
 */
function CardComp({cardModel, isMovable}) {

  // translations (i18n)
  const [t] = useTranslation('translation')

  // local states
  const [isDragged, setIsDragged] = useState(false)

  return (
    <DragDropContainer
      targetKey='dropzone'
      dragData={cardModel}
      noDragging={!isMovable}
      onDrop={() => {
        setIsDragged(false)
      }}
      onDragEnd={() => {
        setIsDragged(false)
      }}
      onDrag={() => {
        setIsDragged(true)
      }}
    >
    <div className={isDragged ? 'dragged-card' : ''}>
      <div className={
          'game-card has-text-centered ' +
          (isMovable ? '': (cardModel.passive ? '' : 'game-card-static ')) +
          (cardModel.passive ? 'game-card-passive' : '')
        }
      >
        <div className="game-card-top">
            <h1 className="belisa is-size-1">{t('CardStrings.Names.' + cardModel.textKey)}</h1>
        </div>
        <div className="game-card-content has-text-black">
            {t('CardStrings.Descriptions.' + cardModel.textKey)}
        </div>
      </div>
    </div>
    </DragDropContainer>
  )
}

export default CardComp
