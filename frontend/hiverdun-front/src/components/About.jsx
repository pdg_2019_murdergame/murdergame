// React
import React from 'react'
import { useTranslation } from 'react-i18next'
// CSS / icons
import './About.scss'

/**
 * Component to display the About text
 * @param {string} param0 id to modify styles from the parent component
 */
function About({itemId}) {
  
  // translations (i18n)
  const [t] = useTranslation('translation')

  return (
    <div id={itemId} className="bm-item">
      <h2 className="belisa">{t('About')}</h2>
      <p id="about-text">
        Hiverdun {t('AboutText')} HEIG-VD
      </p>
    </div>
  )
}

export default About
