import React from 'react'
// redux
import { Provider } from 'react-redux'
import store from '../redux/store'
// responsive
import { BreakpointProvider } from 'react-socks'
// i18n
import '../i18n'
// toasts
import {ToastProvider} from 'react-toast-notifications'

/**
 * Component to wrap the content with the context providers
 * @param {Components} props the child components to wrap
 */
function ProviderWrapper(props) {
  return (
    <Provider store={store}>
      <BreakpointProvider>
        <ToastProvider
          autoDismiss={true}
          autoDismissTimeout={2000}
          placement="bottom-center"
        >
          {props.children}
        </ToastProvider>
      </BreakpointProvider>
    </Provider>
  )
}

export default ProviderWrapper
