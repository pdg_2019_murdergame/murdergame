import store from '../redux/store'
import { createCard, createCardArray } from '../cards'
import {
    updatePlayerNumber,
    updateState,
    connectSocketSuccess,
    connectSocketFailure,
    connectSocketMsg,
    disconnectSocket,
    setNickname,
    resurectPlayer,
    killPlayer,
    setTeam,
    updateCards,
    setCardslot,
    updatePlayers,
    updateVotes,
    updateCabalVotes,
    setLastDeads,
    setWinner
} from '../redux'
import i18n from '../i18n'

/**
 * Get the socket from the store
 */
const getSocket = () => {
    return store.getState().connection.socket
}

/***** socket emits API *****/

const emit = {

    /**
     * Start game
     */
    start: () => {
        if (getSocket() !== null) {
            getSocket().emit('gamestart')
        }
    },

    /**
     * Ask for player infos
     */
    me: () => {
        if (getSocket() !== null) {
            getSocket().emit('me')
        }
    },

    /**
     * Reject a card
     * @param {number} cardId 
     */
    reject: cardId => {
        if (getSocket() !== null) {
            getSocket().emit('reject', cardId)
        }
    },

    /**
     * Skip the Story State
     */
    skip: () => {
        if (getSocket() !== null) {
            getSocket().emit('skip')
        }
    },

    /**
     * Vote against a player
     * @param {string} target 
     */
    vote: target => {
        if (getSocket() !== null) {
            getSocket().emit('vote', target)
        }
    },

    /**
     * Vote against a player (cabal)
     * @param {string} target
     */
    votecabal: target => {
        if (getSocket() !== null) {
            getSocket().emit('cabal-vote', target)
        }
    },

    /**
     * Play a card
     * @param {number} cardId
     * @param {Array[string]} targets
     */
    playcard: (cardId, targets = []) => {
        if (getSocket() !== null) {
            if (targets.length === 0) {
                getSocket().emit('playcard', { cardid: cardId })
            } else if (targets.length === 1) {
                getSocket().emit('playcard', { cardid: cardId, target: targets[0] })
            } else {
                getSocket().emit('playcard', { cardid: cardId, targets: targets })
            }
        }
    },

    /**
     * Tell the server that the player is ready to go to the next state (dawn/day)
     */
    pass: () => {
        if (getSocket() !== null) {
            getSocket().emit('pass')
        }
    },

    /**
     * Tell the server that the players have voted
     */
    verdict: () => {
        if (getSocket() !== null) {
            getSocket().emit('verdict', 'guidoux')
        }
    },

    /**
     * Ask the server for the winning team
     */
    askWinner: () => {
        if(getSocket() !== null) {
            getSocket().emit('askwinner')
        }
    }
}

/***** socket listeners API *****/

/**
 * 
 * @param {SocketIO} socket 
 */
const initListeners = (socket, dispatch, role) => {
    socket.on('connect_failed', () => {
        dispatch(connectSocketFailure('socket connection failed'))
    })

    socket.on('connect', () => {
        dispatch(connectSocketSuccess(socket, role))
    })

    socket.on('roomjoin', name => {
        if (name !== 'Anon') {
            dispatch(connectSocketMsg(`${name} ${i18n.t('MsgJoined')}`))
        }
    })

    socket.on('roomleave', name => {
        if (name !== 'Anon') {
            dispatch(connectSocketMsg(`${name} ${i18n.t('MsgLeft')}`))
        }
    })

    socket.on('disconnect', reason => {
        dispatch(disconnectSocket())
    })

    socket.on('gamesize', numberOfPlayers => {
        dispatch(updatePlayerNumber(numberOfPlayers))
    })

    socket.on('gamestate', state => {
        dispatch(updateState(state))
    })

    socket.on('me', player => {
        // update player infos
        dispatch(setNickname(player.nickname))
        player.alive ? dispatch(resurectPlayer()) : dispatch(killPlayer())
        dispatch(setTeam(player.team))

        // update cards
        const receivedHand = createCardArray(player.cards)
        const cardslotId = player.newcard

        if (cardslotId !== null) {
            let receivedSlotCard = createCard(cardslotId)

            if (receivedHand.length < 3) {
                receivedHand.push(receivedSlotCard)

                dispatch(setCardslot(null))
                dispatch(updateCards(receivedHand))
            } else {
                if (receivedSlotCard.persistant) {
                    let newSlotCard = receivedSlotCard
                    for (let i = 0; i < receivedHand.length; i++) {
                        if (!receivedHand[i].persistant) {
                            newSlotCard = receivedHand[i]
                            receivedHand[i] = receivedSlotCard
                            break
                        }
                    }
                    dispatch(setCardslot(newSlotCard))
                    dispatch(updateCards(receivedHand))
                } else {
                    dispatch(setCardslot(receivedSlotCard))
                    dispatch(updateCards(receivedHand))
                }
            }
        } else {
            dispatch(setCardslot(null))
            dispatch(updateCards(receivedHand))
        }
    })

    socket.on('cardplayed', cardId => {
        // Not Used
    })

    socket.on('playerslist', players => {
        // update the players list by keeping the team infos
        const currentPlayers = store.getState().gameState.players
        const playerMap = new Map()
        currentPlayers.forEach(p => playerMap.set(p.nickname, p.team))
        const newPlayers = players.map(p => {
            const team = playerMap.has(p.nickname) ? playerMap.get(p.nickname) : ''
            return {...p, team: team }
        })

        dispatch(updatePlayers(newPlayers))
    })

    socket.on('info', playerInfos => {
        // update the team info of a player
        const currentPlayers = store.getState().gameState.players
        const newPlayers = []
        currentPlayers.forEach(p => {
            if (p.nickname === playerInfos.nickname) {
                const updatedPlayer = {...p, team: playerInfos.team }
                newPlayers.push(updatedPlayer)
            } else {
                newPlayers.push(p)
            }
        })
        dispatch(updatePlayers(newPlayers))
    })

    socket.on('cabal-info', cabalPlayers => {
        // set the cabal players as team 'cabal'
        const cabalNames = cabalPlayers.map(p => p.nickname)
        const newPlayers = []
        for (let p of store.getState().gameState.players) {
            if (cabalNames.includes(p.nickname)) {
                newPlayers.push({...p, team: 'cabal' })
            } else {
                newPlayers.push(p)
            }
        }

        dispatch(updatePlayers(newPlayers))
    })

    socket.on('voteslist', votes => {
        dispatch(updateVotes(votes))
    })

    socket.on('cabal-voteslist', votes => {
        dispatch(updateCabalVotes(votes))
    })

    socket.on('lastdeads', deads => {
        dispatch(setLastDeads(deads))
    })

    socket.on('winner', winner => {
        dispatch(setWinner(winner))
    })
}

/**
 * Disconnect socket
 */
const disconnect = () => {
    if (getSocket() != null) {
        getSocket().disconnect()
    }
}

/**
 * socket connection object
 */
const io = {
    emit,
    initListeners,
    disconnect
}

export default io