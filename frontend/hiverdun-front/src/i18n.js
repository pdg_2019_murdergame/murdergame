import i18n from 'i18next'
import LanguageDetector from 'i18next-browser-languagedetector'
import XHR from 'i18next-xhr-backend'
import { initReactI18next } from 'react-i18next'

i18n
    .use(XHR) // load translation using xhr
    .use(LanguageDetector) // detect user language
    .use(initReactI18next) // init i18n-next
    .init({
        debug: true, // debug mode, false for production
        lng: 'en', // default language
        fallbackLng: 'en', // if detected language is not available

        interpolation: {
            escapeValue: false // react is sage from xss
        },

        // ressource by default in public/locales

        // translation namespace, files in locales needs to be called
        // <ns>.json to be used in the namespace
        ns: ['translation'],
        defaultNS: 'translation'
    })

export default i18n