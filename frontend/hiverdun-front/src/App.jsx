import React, { Suspense} from 'react' // Supsense -> fallback when text is loading
// Routing
import { BrowserRouter, Route, Switch } from 'react-router-dom'
// Pages
import TitlePage from './components/pages/TitlePage'
import MenuPage from './components/pages/MenuPage'
// Master Pages
import {LobbyCreationPage,
        IntroPage,
        VillagePage,
        EndGamePage} from './components/pages/master'
// Player Pages
import {JoinLobbyPage,
        GamePage,
        LobbyPage,
        PlayerEndGamePage} from './components/pages/player'
// CSS
import './App.scss'
import { library } from '@fortawesome/fontawesome-svg-core'
import {
  faHome,
  faAngleDown,
  faUserCircle,
  faCircle,
  faSkull,
  faCrosshairs,
  faCloud,
  faMoon,
  faPeace,
  faUsers,
  faChevronRight,
  faListUl,
  faSignOutAlt
} from '@fortawesome/free-solid-svg-icons'

library.add(
  faHome,
  faAngleDown,
  faUserCircle,
  faCircle,
  faSkull,
  faCrosshairs,
  faCloud,
  faMoon,
  faPeace,
  faUsers,
  faChevronRight,
  faListUl,
  faSignOutAlt)

function App() {
  
  return (
    <div className="App">
      <Suspense fallback='loading languages ...'>
        <BrowserRouter>
          <Switch>
            {/* Player pages*/}
            <Route path='/player/end' component={PlayerEndGamePage} />
            <Route path='/player/hiverdun' component={GamePage} />
            <Route path='/player/lobby' component={LobbyPage} />
            <Route path={['/player/join-lobby', '/player']} component={JoinLobbyPage} />
            {/* Master Pages*/}
            <Route path='/master/end' component={EndGamePage} />
            <Route path='/master/village' component={VillagePage} />
            <Route path='/master/intro' component={IntroPage} />
            <Route path={['/master/create-lobby', '/master']} component={LobbyCreationPage} />
            {/* Common Pages */}
            <Route path={['/menu', '/home']} component={MenuPage} />
            <Route path={['/title', '/']} component={TitlePage} />
          </Switch>
        </BrowserRouter>
      </Suspense>
    </div>
  );
}


export default App; 