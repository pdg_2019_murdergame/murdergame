import Card from "./Card";

/**
 * Class representing the Prophecy card (useage wip)
 */
class ProphecyCard extends Card {
    constructor(id) {
        super(id, 'ProphecyCard', true, false)
    }
}

export default ProphecyCard