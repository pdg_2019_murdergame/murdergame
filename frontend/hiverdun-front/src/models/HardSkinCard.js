import Card from "./Card";

/**
 * Class representing the Hard Skin Card
 */
class HardSkinCard extends Card {
    constructor(id) {
        super(id, 'HardSkinCard', false, false, false, true)
    }
}

export default HardSkinCard