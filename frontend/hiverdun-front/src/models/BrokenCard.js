import Card from "./Card";

/**
 * Class representing a Broken Card (no use)
 */
class BrokenCard extends Card {
    constructor(id) {
        super(id, 'BrokenCard', false, false)
    }
}

export default BrokenCard