import Card from "./Card";

/**
 * Class representing a Death Poison Card
 */
class DeathPoisonCard extends Card {
    constructor(id) {
        super(id, 'DeathPoisonCard', false, false, false, false)
    }
}

export default DeathPoisonCard