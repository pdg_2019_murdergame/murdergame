import Card from "./Card";

/**
 * Class representing a Cabal sacrifice card
 */
class SacrificeCard extends Card {
    constructor(id) {
        super(id, 'SacrificeCard', true, false)
    }
}

export default SacrificeCard