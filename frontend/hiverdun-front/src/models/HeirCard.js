import Card from "./Card";

/**
 * Class representing a Heir Card
 */
class HeirCard extends Card {
    constructor(id) {
        super(id, 'HeirCard', false, false, false, false)
    }
}

export default HeirCard