/**
 * Class representing a Card (abstract)
 */
class Card {

    /**
     * Create a Card
     * @param {number} id the id of the card
     * @param {string} textKey the key to get the texts of the card in the text resources
     * @param {bool} persistant true if the card is persistant
     * @param {bool} paired true if the card needs to be paired to be played
     * @param {bool} dawn true if the card is played at dawn
     * @param {bool} passive true if the card is passive
     */
    constructor(id, textKey, persistant, paired, dawn = false, passive = false) {
        this._id = id
        this._textKey = textKey
        this._persistant = persistant
        this._paired = paired
        this._dawn = dawn
        this._passive = passive
    }

    get id() {
        return this._id
    }

    get textKey() {
        return this._textKey
    }

    get persistant() {
        return this._persistant
    }

    get paired() {
        return this._paired
    }

    get dawn() {
        return this._dawn
    }

    get passive() {
        return this._passive
    }
}

export default Card