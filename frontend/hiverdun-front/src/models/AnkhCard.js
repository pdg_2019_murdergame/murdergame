import Card from "./Card";

/**
 * Class representing an Ankh Card (played at Dawn)
 */
class AnkhCard extends Card {
    constructor(id) {
        super(id, 'AnkhCard', false, false, true, false)
    }
}

export default AnkhCard