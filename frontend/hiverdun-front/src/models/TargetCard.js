import Card from "./Card";

/**
 * Class representing a Card to target someone
 */
class TargetCard extends Card {
    constructor(id) {
        super(id, 'TargetCard', true, false)
    }
}

export default TargetCard