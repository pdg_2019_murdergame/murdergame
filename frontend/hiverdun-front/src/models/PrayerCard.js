import Card from "./Card";

/**
 * Class representing a Prayer Card
 */
class PrayerCard extends Card {
    constructor(id) {
        super(id, 'PrayerCard', true, false, false, false)
    }
}

export default PrayerCard