import Card from "./Card";

/**
 * Class representing a Burgle Card
 */
class BurgleCard extends Card {
    constructor(id) {
        super(id, 'BurgleCard', false, false, false, false)
    }
}

export default BurgleCard