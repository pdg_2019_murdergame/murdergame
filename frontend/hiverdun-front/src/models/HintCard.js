import Card from "./Card";

/**
 * Class representing a Hint Card
 */
class HintCard extends Card {
    constructor(id) {
        super(id, 'HintCard', false, true, false, false)
    }
}

export default HintCard