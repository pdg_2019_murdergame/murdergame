import React from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';
// components
import ProviderWrapper from './components/ProviderWrapper'
import App from './App';
// css
import './index.scss';

ReactDOM.render(
  <ProviderWrapper>
    <App />
  </ProviderWrapper>
  , document.getElementById('root'));
serviceWorker.unregister();