# production

```shell
# Using the production Dockerfile, build and tag the Docker image:
docker build -f Dockerfile-prod -t sample:prod .
# Spin up the container:
 docker run -it -p 80:80 --rm sample:prod
```

The ip is your `docker machine/toolbox/desktop ip` or localhost depending of your oprating system.

You do not need the port because we have exposed the port 80 in the container.

# dev 

  ```shell
# build the images
 docker build -t sample:dev .
# run it 
docker run -v ${PWD}:/app -v /app/node_modules -p 3001:3000 --rm sample:dev
  ```

On Windows the `${PWD}` can be replaced by `\ PWD -W`. 

Use the port `3001` to access the container.

(You can use `%cd%` to get your current relative path on Windows)

# error : no space left

It is possible that you get some errors about having to few memory left for your containers if you are not using the `--rm` command to clean your images and your volumes. You can use the following commands to clean them:

```shell
# remove images 
docker images -aq -f 'dangling=true' | xargs docker rmi
# remove volumes 
docker volume ls -q -f 'dangling=true' | xargs docker volume rm
```

--force works, but it is to use with precaution